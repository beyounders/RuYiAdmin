﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessEntity.BusinessModule
{
    /// <summary>
    /// 模块API访问账号表
    /// </summary>
    [SugarTable("biz_account")]
    public class BizAccount : BizUser
    {
        /// <summary>
        /// 模块编号
        /// </summary>
        [Required]
        public Guid ModuleId { get; set; }
    }
}
