﻿using RuYiAdmin.Net.Entity.Base;
using RuYiAdmin.Net.Entity.BusinessEnum;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement
{
    /// <summary>
    /// 计划任务实体模型
    /// </summary>
    [SugarTable("sys_schedule_job")]
    public class SysScheduleJob : RuYiAdminBaseEntity
    {
        /// <summary>
        /// 任务名称
        /// </summary>
        [Required, MaxLength(128)]
        public String JobName { get; set; }

        /// <summary>
        /// 任务描述
        /// </summary>
        [MaxLength(512)]
        public String JobDescription { get; set; }

        /// <summary>
        /// 命名空间
        /// </summary>
        [Required, MaxLength(512)]
        public String NameSpace { get; set; }

        /// <summary>
        /// 实现类
        /// </summary>
        [Required, MaxLength(128)]
        public String JobImplement { get; set; }

        /// <summary>
        /// Cron表达式
        /// </summary>
        [Required, MaxLength(128)]
        public String CronExpression { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public Nullable<DateTime> StartTime { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public Nullable<DateTime> EndTime { get; set; }

        /// <summary>
        /// 任务状态
        /// </summary>
        [Required]
        public JobStatus JobStatus { get; set; }

        /// <summary>
        /// 集群编号
        /// </summary>
        public Nullable<int> GroupId { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public Nullable<int> SerialNumber { get; set; }
    }
}
