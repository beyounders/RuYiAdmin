﻿using RuYiAdmin.Net.Entity.Base;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement
{
    /// <summary>
    /// 角色用户关系模型
    /// </summary>
    [SugarTable("sys_role_user")]
    public class SysRoleUser : RuYiAdminBaseEntity
    {
        /// <summary>
        /// 角色编号
        /// </summary>
        [Required]
        public Guid RoleId { get; set; }

        /// <summary>
        /// 用户编号
        /// </summary>
        [Required]
        public Guid UserId { get; set; }
    }
}
