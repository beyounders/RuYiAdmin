﻿using RuYiAdmin.Net.Entity.Base;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement
{
    /// <summary>
    /// 机构模型
    /// </summary>
    [SugarTable("sys_organization")]
    public class SysOrganization : RuYiAdminBaseEntity
    {
        /// <summary>
        /// 机构名称
        /// </summary>
        [Required, MaxLength(128)]
        public String OrgName { get; set; }

        /// <summary>
        /// 父键
        /// </summary>
        public Nullable<Guid> ParentId { get; set; }

        /// <summary>
        /// 主管人
        /// </summary>
        public Nullable<Guid> Leader { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public int SerialNumber { get; set; }
    }
}
