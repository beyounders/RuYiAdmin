﻿using Masuit.Tools.Core.Validator;
using RuYiAdmin.Net.Entity.Base;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement
{
    /// <summary>
    /// 用户模型
    /// </summary>
    [SugarTable("sys_user")]
    public class SysUser : RuYiAdminBaseEntity
    {
        /// <summary>
        /// 登录名称
        /// </summary>
        [Required, MaxLength(128)]
        public String LogonName { get; set; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        [Required, MaxLength(128)]
        public String DisplayName { get; set; }

        /// <summary>
        /// 登录密码
        /// </summary>
        [Required, MaxLength(512)]
        public String Password { get; set; }

        /// <summary>
        /// 座机号码
        /// </summary>
        [MaxLength(45)]
        public String Telephone { get; set; }

        /// <summary>
        /// 手机号码
        /// </summary>
        [MaxLength(45)]//, IsPhone
        public String MobilePhone { get; set; }

        /// <summary>
        /// 邮箱地址
        /// </summary>
        [MaxLength(45)]//, IsEmail
        public String Email { get; set; }

        /// <summary>
        /// 是否超级管理员
        /// </summary>
        public int IsSupperAdmin { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public int SerialNumber { get; set; }

        /// <summary>
        /// 是否启用
        /// 0：禁用；
        /// 1：启用
        /// </summary>
        public int IsEnabled { get; set; }
    }
}
