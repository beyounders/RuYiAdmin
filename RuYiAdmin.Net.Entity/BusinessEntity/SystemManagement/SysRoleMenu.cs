﻿using RuYiAdmin.Net.Entity.Base;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement
{
    /// <summary>
    /// 角色菜单关系模型
    /// </summary>
    [SugarTable("sys_role_menu")]
    public class SysRoleMenu : RuYiAdminBaseEntity
    {
        /// <summary>
        /// 角色编号
        /// </summary>
        [Required]
        public Guid RoleId { get; set; }

        /// <summary>
        /// 菜单编号
        /// </summary>
        [Required]
        public Guid MenuId { get; set; }
    }
}
