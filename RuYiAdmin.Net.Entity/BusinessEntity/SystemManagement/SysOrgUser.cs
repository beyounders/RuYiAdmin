﻿using RuYiAdmin.Net.Entity.Base;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement
{
    /// <summary>
    /// 机构用户关系模型
    /// </summary>
    [SugarTable("sys_org_user")]
    public class SysOrgUser : RuYiAdminBaseEntity
    {
        /// <summary>
        /// 机构编号
        /// </summary>
        [Required]
        public Guid OrgId { get; set; }

        /// <summary>
        /// 用户编号
        /// </summary>
        [Required]
        public Guid UserId { get; set; }
    }
}
