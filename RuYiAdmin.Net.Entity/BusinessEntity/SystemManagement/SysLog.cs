﻿using Masuit.Tools.Core.Validator;
using RuYiAdmin.Net.Entity.Base;
using RuYiAdmin.Net.Entity.BusinessEnum;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement
{
    /// <summary>
    /// 审计日志模型
    /// </summary>
    [SugarTable("sys_log")]
    public class SysLog : RuYiAdminBaseEntity
    {
        /// <summary>
        /// 用户编号
        /// </summary>
        [Required]
        public Guid UserId { get; set; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        [Required, MaxLength(128)]
        public String UserName { get; set; }

        /// <summary>
        /// 机构编号
        /// </summary>
        [Required]
        public Guid OrgId { get; set; }

        /// <summary>
        /// 机构名称
        /// </summary>
        [Required, MaxLength(128)]
        public String OrgName { get; set; }

        /// <summary>
        /// 使用系统
        /// </summary>
        public String System { get; set; }

        /// <summary>
        /// 使用浏览器
        /// </summary>
        public String Browser { get; set; }

        /// <summary>
        /// IP地址
        /// </summary>
        [IsIPAddress] 
        public String IP { get; set; }

        /// <summary>
        /// 操作类型
        /// </summary>
        [Required]
        public OperationType OperationType { get; set; }

        /// <summary>
        /// 请求路径
        /// </summary>
        [Required]
        public String RequestUrl { get; set; }

        /// <summary>
        /// 请求参数
        /// </summary>
        public String Params { get; set; }

        /// <summary>
        /// 返回值
        /// </summary>
        public String Result { get; set; }

        /// <summary>
        /// 记录旧值
        /// </summary>
        public String OldVaue { get; set; }

        /// <summary>
        /// 记录新值
        /// </summary>
        public String NewValue { get; set; }
    }
}
