﻿using RuYiAdmin.Net.Entity.Base;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement
{
    /// <summary>
    /// 多语模型
    /// </summary>
    [SugarTable("sys_language")]
    public class SysLanguage : RuYiAdminBaseEntity
    {
        /// <summary>
        /// 语言名称
        /// </summary>
        [Required, MaxLength(45)]
        public String LanguageName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public Nullable<int> OrderNumber { get; set; }
    }
}
