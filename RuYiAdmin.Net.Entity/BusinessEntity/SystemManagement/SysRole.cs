﻿using RuYiAdmin.Net.Common.CommonClass.Excel;
using RuYiAdmin.Net.Entity.Base;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement
{
    /// <summary>
    /// 角色模型
    /// </summary>
    [SugarTable("sys_role")]
    public class SysRole : RuYiAdminBaseEntity
    {
        /// <summary>
        /// 角色名称
        /// </summary>
        [Required, MaxLength(128)]
        [ExcelExport("角色名称")]
        public String RoleName { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        [ExcelExport("排序")]
        public Nullable<int> SerialNumber { get; set; }
    }
}
