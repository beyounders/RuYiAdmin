﻿using RuYiAdmin.Net.Entity.BusinessEnum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessEntity.System
{
    /// <summary>
    /// 系统消息
    /// </summary>
    public class SystemMessage
    {
        /// <summary>
        /// 消息
        /// </summary>
        public String Message { get; set; }

        /// <summary>
        /// 消息类型
        /// </summary>
        public MessageType MessageType { get; set; }

        /// <summary>
        /// 信息载体
        /// </summary>
        public Object Object { get; set; }
    }
}
