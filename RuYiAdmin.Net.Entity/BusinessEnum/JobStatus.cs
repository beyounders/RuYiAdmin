﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessEnum
{
    /// <summary>
    /// 任务状态
    /// </summary>
    public enum JobStatus
    {
        [Description("已启用")]
        Started,

        [Description("运行中")]
        Running,

        [Description("执行中")]
        Executing,

        [Description("执行完成")]
        Completed,

        [Description("任务计划中")]
        Planning,

        [Description("已停止")]
        Stopped
    }
}
