﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessEnum
{
    /// <summary>
    /// 操作类型
    /// </summary>
    public enum OperationType
    {
        /// <summary>
        /// 查询列表
        /// </summary>
        QueryList,

        /// <summary>
        /// 查询实体
        /// </summary>
        QueryEntity,

        /// <summary>
        /// 新增实体
        /// </summary>
        AddEntity,

        /// <summary>
        /// 编辑实体
        /// </summary>
        EditEntity,

        /// <summary>
        /// 逻辑删除实体
        /// </summary>
        DeleteEntity,

        /// <summary>
        /// 物理删除实体
        /// </summary>
        RemoveEntity,

        /// <summary>
        /// 上传文件
        /// </summary>
        UploadFile,

        /// <summary>
        /// 下载文件
        /// </summary>
        DownloadFile,

        /// <summary>
        /// 导入数据
        /// </summary>
        ImportData,

        /// <summary>
        /// 导出数据
        /// </summary>
        ExportData,

        /// <summary>
        /// 菜单授权
        /// </summary>
        MenuAuthorization,

        /// <summary>
        /// 用户授权
        /// </summary>
        PermissionAuthorization,

        /// <summary>
        /// 打印
        /// </summary>
        Print,

        /// <summary>
        /// 登录
        /// </summary>
        Logon,

        /// <summary>
        /// 登出
        /// </summary>
        Logout,

        /// <summary>
        /// 强制登出
        /// </summary>
        ForceLogout,

        /// <summary>
        /// 更新密码
        /// </summary>
        UpdatePassword,

        /// <summary>
        /// 启动计划任务
        /// </summary>
        StartScheduleJob,

        /// <summary>
        /// 暂停计划任务
        /// </summary>
        PauseScheduleJob,

        /// <summary>
        /// 恢复计划任务
        /// </summary>
        ResumeScheduleJob,

        /// <summary>
        /// 权限下放
        /// </summary>
        DelegatePermission
    }
}
