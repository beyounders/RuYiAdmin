﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using RuYiAdmin.Net.Entity.CoreEntity;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.Base
{
    public class RuYiAdminBaseEntity
    {
        #region 通用属性

        /// <summary>
        /// 编号
        /// </summary>
        [Required]
        [SugarColumn(IsPrimaryKey = true)]
        public Guid Id { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [MaxLength(512)]
        public String Remark { get; set; }

        /// <summary>
        /// 逻辑标志位
        /// </summary>
        [Required]
        public int IsDel { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        [Required]
        public Guid Creator { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Required]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 编辑人员
        /// </summary>
        [Required]
        public Guid Modifier { get; set; }

        /// <summary>
        /// 编辑时间
        /// </summary>
        [Required]
        public DateTime ModifyTime { get; set; }

        #endregion

        #region 通用方法 

        /// <summary>
        /// 转化为json
        /// </summary>
        /// <returns>json字符串</returns>
        public String ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        /// <summary>
        /// 创建赋值
        /// </summary>
        /// <param name="context">HttpContext</param>
        public void Create(IHttpContextAccessor context)
        {
            this.Id = Guid.NewGuid();

            this.IsDel = 0;

            var user = SessionContext.GetCurrentUserInfo(context);

            this.Creator = user.Id;
            this.CreateTime = DateTime.Now;

            this.Modifier = user.Id;
            this.ModifyTime = DateTime.Now;
        }

        /// <summary>
        /// 编辑赋值
        /// </summary>
        /// <param name="context">HttpContext</param>
        public void Modify(IHttpContextAccessor context)
        {
            var user = SessionContext.GetCurrentUserInfo(context);

            this.Modifier = user.Id;
            this.ModifyTime = DateTime.Now;
        }

        /// <summary>
        /// 逻辑删除赋值
        /// </summary>
        /// <param name="context">HttpContext</param>
        public void Delete(IHttpContextAccessor context)
        {
            this.IsDel = 1;

            var user = SessionContext.GetCurrentUserInfo(context);

            this.Modifier = user.Id;
            this.ModifyTime = DateTime.Now;
        }

        #endregion
    }
}
