﻿using Microsoft.AspNetCore.Http;
using RuYiAdmin.Net.Common.Global;
using RuYiAdmin.Net.Common.Utility;
using RuYiAdmin.Net.Entity.BusinessDTO.SystemManagement;
using RuYiAdmin.Net.Entity.BusinessEnum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.CoreEntity
{
    /// <summary>
    /// Session上下文
    /// </summary>
    public class SessionContext
    {
        /// <summary>
        /// 当前用户
        /// </summary>
        /// <param name="context">HttpContext</param>
        /// <returns>用户信息</returns>
        public static SysUserDTO GetCurrentUserInfo(IHttpContextAccessor context)
        {
            var token = context.HttpContext.GetToken();

            var user = RedisUtil.Get<SysUserDTO>(token);

            return user;
        }

        /// <summary>
        /// 获取用户机构编号
        /// </summary>
        /// <param name="context">HttpContext</param>
        /// <returns>用户机构编号</returns>
        public static Guid GetUserOrgId(IHttpContextAccessor context)
        {
            var orgId = Guid.Empty;

            var user = SessionContext.GetCurrentUserInfo(context);
            if (user.IsSupperAdmin.Equals((int)YesNo.YES) && user.OrgId.Equals(Guid.Empty))
            {
                orgId = GlobalContext.SystemConfig.OrgRoot;
            }
            else
            {
                orgId = user.OrgId;
            }

            return orgId;
        }
    }
}
