﻿using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessDTO.SystemManagement
{
    /// <summary>
    /// 机构DTO
    /// </summary>
    public class SysOrganizationDTO : SysOrganization
    {
        /// <summary>
        /// 主管人姓名
        /// </summary>
        public String LeaderName { get; set; }

        /// <summary>
        /// 子集
        /// </summary>
        public List<SysOrganizationDTO> Children { get; set; }
    }
}
