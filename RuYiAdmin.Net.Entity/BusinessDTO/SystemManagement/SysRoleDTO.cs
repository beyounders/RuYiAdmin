﻿using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessDTO.SystemManagement
{
    /// <summary>
    /// 系统角色DTO
    /// </summary>
    public class SysRoleDTO : SysRole
    {
        /// <summary>
        /// 机构编号
        /// </summary>
        public Guid OrgId { get; set; }

        /// <summary>
        /// 机构名称
        /// </summary>
        public String OrgName { get; set; }
    }
}
