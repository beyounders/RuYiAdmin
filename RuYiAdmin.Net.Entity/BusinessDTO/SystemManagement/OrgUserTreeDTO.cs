﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessDTO.SystemManagement
{
    /// <summary>
    /// 机构、用户树
    /// </summary>
    public class OrgUserTreeDTO
    {
        /// <summary>
        /// 编号
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// 类型
        /// 1，机构
        /// 2，用户
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public Nullable<int> SerialNumber { get; set; }

        /// <summary>
        /// 子集
        /// </summary>
        public List<OrgUserTreeDTO> Children { get; set; }
    }
}
