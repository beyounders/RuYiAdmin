﻿using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessDTO.SystemManagement
{
    /// <summary>
    /// 数据字典DTO
    /// </summary>
    public class SysCodeTableDTO : SysCodeTable
    {
        /// <summary>
        /// 子集
        /// </summary>
        public List<SysCodeTableDTO> Children { get; set; }
    }
}
