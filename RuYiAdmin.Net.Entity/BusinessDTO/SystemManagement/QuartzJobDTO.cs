﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessDTO.SystemManagement
{
    public class QuartzJobDTO
    {
        /// <summary>
        /// 任务编号
        /// </summary>
        public Guid JobId { get; set; }

        /// <summary>
        /// 集群节点编号
        /// </summary>
        public int? GroupId { get; set; }

        /// <summary>
        /// 预执行动作
        /// </summary>
        public string Action { get; set; }

    }
}
