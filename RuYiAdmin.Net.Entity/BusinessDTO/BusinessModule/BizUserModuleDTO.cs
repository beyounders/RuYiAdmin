﻿using RuYiAdmin.Net.Entity.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.BusinessDTO.BusinessModule
{
    /// <summary>
    /// BizUserModuleDTO
    /// </summary>
    public class BizUserModuleDTO : RuYiAdminBaseEntity
    {
        /// <summary>
        /// 模块编号
        /// </summary>
        [Required]
        public Guid ModuleId { get; set; }

        /// <summary>
        /// 用户编号
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// 用户所在模块登录账号
        /// </summary>
        [Required]
        [MaxLength(128)]
        public String UserModuleLogonName { get; set; }

        /// <summary>
        /// 用户所在模块登录密码
        /// </summary>
        [Required]
        [MaxLength(512)]
        public String UserModulePassword { get; set; }

        /// <summary>
        /// 登录名
        /// </summary>
        public String UserLogonName { get; set; }

        /// <summary>
        /// 用户展示名
        /// </summary>
        public String UserDisplayName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public String UserPassword { get; set; }

        /// <summary>
        /// 座机
        /// </summary>
        [MaxLength(45)]
        public String Telephone { get; set; }

        /// <summary>
        /// 手机
        /// </summary>
        [MaxLength(45)]
        public String MobilePhone { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        [MaxLength(45)]
        public String Email { get; set; }

        /// <summary>
        /// 是否启用
        /// 0：禁用；
        /// 1：启用
        /// </summary>
        public int IsEnabled { get; set; }
    }
}
