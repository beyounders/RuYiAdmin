﻿using AutoMapper;
using RuYiAdmin.Net.Entity.BusinessDTO.SystemManagement;
using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Entity.AutoMapperConfig
{
    /// <summary>
    /// 映射描述文件
    /// </summary>
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            #region POCO TO DTO

            //机构
            CreateMap<SysOrganization, SysOrganizationDTO>();

            //用户
            CreateMap<SysUser, SysUserDTO>();

            //菜单
            CreateMap<SysMenu, SysMenuDTO>();

            //角色
            CreateMap<SysRole, SysRoleDTO>();

            //数据字典
            CreateMap<SysCodeTable, SysCodeTableDTO>();

            //导入配置
            CreateMap<SysImportConfig, ImportConfigDTO>();

            //导入配置明细
            CreateMap<SysImportConfigDetail, ImportConfigDetailDTO>();

            #endregion

            #region DTO TO POCO
            #endregion
        }
    }
}
