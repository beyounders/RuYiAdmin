﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesseract;

namespace RuYiAdmin.Net.Common.Utility
{
    /// <summary>
    /// OCR工具类
    /// </summary>
    public static class OCRUtil
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="imagePath"></param>
        /// <returns></returns>
        public static String Recognize(String imagePath)
        {
            String result = String.Empty;

            using (var ocr = new TesseractEngine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Library\\Tesseract"), "chi_sim", EngineMode.Default))
            {
                var pix = Pix.LoadFromFile(imagePath);
                using (var page = ocr.Process(pix))
                {
                    String text = page.GetText();
                    if (!String.IsNullOrEmpty(text))
                    {
                        result = text;
                    }
                }
            }

            return result;
        }
    }
}
