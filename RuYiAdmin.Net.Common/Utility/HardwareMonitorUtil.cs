﻿using Masuit.Tools.Hardware;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Common.Utility
{
    /// <summary>
    /// 硬件监测工具
    /// </summary>
    public static class HardwareMonitorUtil
    {
        /// <summary>
        /// 硬件监测
        /// </summary>
        /// <returns>硬件监测信息</returns>
        public static Object StartMonitoring()
        {
            int cpuCount = SystemInfo.GetCpuCount();// 获取CPU核心数
            float cpuLoad = SystemInfo.CpuLoad;// 获取CPU占用率
            double cpuTemperature = SystemInfo.GetCPUTemperature();// 获取CPU温度
            List<DiskInfo> diskInfo = SystemInfo.GetDiskInfo();// 获取磁盘每个分区可用空间
            string localUsedIp = SystemInfo.GetLocalUsedIP().ToString();// 获取本机当前正在使用的IP地址
            RamInfo ramInfo = SystemInfo.GetRamInfo();// 获取内存信息

            return new
            {
                CpuCount = cpuCount,
                CpuLoad = cpuLoad,
                CpuTemperature = cpuTemperature,
                CpuInfo = SystemInfo.GetCpuInfo(),
                LogicalDrives = Environment.GetLogicalDrives(),
                DiskInfo = diskInfo.DistinctBy(t => t.SerialNumber),
                LocalUsedIP = localUsedIp,
                RamInfo = ramInfo,
                OSArchitecture = Enum.GetName(typeof(Architecture), RuntimeInformation.OSArchitecture),
                OSDescription = RuntimeInformation.OSDescription,
                ProcessArchitecture = Enum.GetName(typeof(Architecture), RuntimeInformation.ProcessArchitecture),
                FrameworkDescription = RuntimeInformation.FrameworkDescription,
                Windows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows),
                Is64BitOperatingSystem = Environment.Is64BitOperatingSystem,
                Is64BitProcess = Environment.Is64BitProcess,
                OSVersion = Environment.OSVersion,
                CpuCore = Environment.ProcessorCount,
                HostName = Environment.MachineName
            };
        }
    }
}
