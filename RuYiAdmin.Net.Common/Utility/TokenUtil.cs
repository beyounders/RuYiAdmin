﻿using Microsoft.AspNetCore.Http;
using RuYiAdmin.Net.Common.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Common.Utility
{
    /// <summary>
    /// Token工具类
    /// </summary>
    public static class TokenUtil
    {
        /// <summary>
        /// 获取token
        /// </summary>
        /// <param name="context">Http会话</param>
        /// <returns>token</returns>
        public static String GetToken(this HttpContext context)
        {
            var token = String.Empty;

            token = context.Request.Headers["token"];
            var tokenSalt = context.Request.Headers["ts"];

            token = RsaUtil.PemDecrypt(token, GlobalContext.SystemConfig.RsaPrivateKey);
            token = token.ToString().Replace("^" + tokenSalt, "");

            return token;
        }
    }
}
