﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Common.Utility
{
    public class Logger
    {
        /// <summary>
        /// ILog实例
        /// </summary>
        private static ILog logger;

        /// <summary>
        /// 构造函数
        /// </summary>
        static Logger()
        {
            if (logger == null && Global.GlobalContext.LogConfig.IsEnabled)
            {
                var repository = LogManager.CreateRepository("NETCoreRepository");
                var path = Directory.GetCurrentDirectory() + "/Log4net/log4net.config";
                //读取配置信息
                XmlConfigurator.Configure(repository, new FileInfo(path));
                logger = LogManager.GetLogger(repository.Name, "InfoLogger");
            }
        }

        /// <summary>
        /// 普通日志
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public static void Info(string message, Exception exception = null)
        {
            if (logger != null && Global.GlobalContext.LogConfig.IsEnabled)
            {
                if (exception == null)
                {
                    logger.Info(message);
                }
                else
                {
                    logger.Info(message, exception);
                }
            }
        }

        /// <summary>
        /// 告警日志
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public static void Warn(string message, Exception exception = null)
        {
            if (logger != null && Global.GlobalContext.LogConfig.IsEnabled)
            {
                if (exception == null)
                {
                    logger.Warn(message);
                }
                else
                {
                    logger.Warn(message, exception);
                }
            }
        }

        /// <summary>
        /// 错误日志
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public static void Error(string message, Exception exception = null)
        {
            if (logger != null && Global.GlobalContext.LogConfig.IsEnabled)
            {
                if (exception == null)
                {
                    logger.Error(message);
                }
                else
                {
                    logger.Error(message, exception);
                }
            }
        }
    }
}
