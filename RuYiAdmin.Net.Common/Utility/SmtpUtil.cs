﻿using RuYiAdmin.Net.Common.Global;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Common.Utility
{
    /// <summary>
    /// Smtp工具类
    /// </summary>
    public class SmtpUtil
    {
        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="subject">主题</param>
        /// <param name="body">内容</param>
        /// <param name="to">收件人</param>
        /// <param name="files">附件</param>
        public static void SendMail(String subject, String body, String to, List<FileInfo> files = null)
        {
            var host = GlobalContext.MailConfig.Host;
            var port = GlobalContext.MailConfig.Port;
            var userName = GlobalContext.MailConfig.UserName;
            var password = GlobalContext.MailConfig.Password;
            var senderAddress = GlobalContext.MailConfig.SenderAddress;

            SmtpClient mailClient = null;

            try
            {
                if (port != null && port > 0)
                {
                    mailClient = new SmtpClient(host, (int)port);
                }
                else
                {
                    mailClient = new SmtpClient(host);
                }

                //SMTP服务器身份验证
                mailClient.Credentials = new NetworkCredential(userName, password);

                //发件人地址、收件人地址
                MailMessage message = new MailMessage(senderAddress, to);

                //邮件主题
                message.Subject = subject;

                //邮件内容
                message.Body = body;

                foreach (var file in files)
                {
                    //附件
                    Attachment att = new Attachment(file.FullName);

                    //添加附件
                    message.Attachments.Add(att);
                }

                //发送
                mailClient.Send(message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
