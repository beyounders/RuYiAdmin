﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Common.Global
{
    /// <summary>
    /// 系统配置
    /// </summary>
    public class SystemConfig
    {
        /// <summary>
        /// Pem格式Rsa公钥
        /// </summary>
        public String RsaPublicKey { get; set; }

        /// <summary>
        /// Pem格式Rsa私钥
        /// </summary>
        public String RsaPrivateKey { get; set; }

        /// <summary>
        /// 白名单
        /// </summary>
        public String WhiteList { get; set; }

        /// <summary>
        /// Header配置
        /// </summary>
        public String HeaderConfig { get; set; }

        /// <summary>
        /// 机构根目录编号
        /// </summary>
        public Guid OrgRoot { get; set; }

        /// <summary>
        /// 默认密码
        /// </summary>
        public String DefaultPassword { get; set; }

        /// <summary>
        /// AesKey
        /// </summary>
        public String AesKey { get; set; }

        /// <summary>
        /// 登录上限
        /// </summary>
        public int LogonCountLimit { get; set; }

        /// <summary>
        /// TokenKey
        /// </summary>
        public String TokenKey { get; set; }

        /// <summary>
        /// 检测Token开关
        /// </summary>
        public bool CheckToken { get; set; }

        /// <summary>
        /// 检测JwtToken开关
        /// </summary>
        public bool CheckJwtToken { get; set; }

        /// <summary>
        /// 生产环境是否支持SwaggerUI
        /// </summary>
        public bool SupportSwaggerOnProduction { get; set; }
    }
}
