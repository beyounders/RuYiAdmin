﻿using RuYiAdmin.Net.Common.CommonEnum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Common.Global
{
    /// <summary>
    /// 消息中间件配置
    /// </summary>
    public class MOMConfig
    {
        /// <summary>
        /// 消息中间件类型
        /// </summary>
        public MOMType MOMType { get; set; }
    }
}
