﻿using RuYiAdmin.Net.Entity.BusinessDTO.SystemManagement;
using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using RuYiAdmin.Net.Entity.CoreEntity;
using RuYiAdmin.Net.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Service.BusinessService.SystemManagement.Organization
{
    /// <summary>
    /// 机构业务层接口
    /// </summary>
    public interface IOrganizationService : IRuYiAdminBaseService<SysOrganization>
    {
        /// <summary>
        /// 获取机构树
        /// </summary>
        /// <returns>ActionResult</returns>
        Task<QueryResult<SysOrganizationDTO>> GetOrgTreeNodes();

        /// <summary>
        /// 获取机构、用户树
        /// </summary>
        /// <returns></returns>
        Task<QueryResult<OrgUserTreeDTO>> GetOrgUserTree();

        /// <summary>
        /// 加载系统机构缓存
        /// </summary>
        Task LoadSystemOrgCache();

        /// <summary>
        /// 清理系统机构缓存
        /// </summary>
        Task ClearSystemOrgCache();
    }
}
