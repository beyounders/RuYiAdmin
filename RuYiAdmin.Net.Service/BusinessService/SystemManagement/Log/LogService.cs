﻿using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using RuYiAdmin.Net.Repository.BusinessRepository.SystemManagement.Log;
using RuYiAdmin.Net.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Service.BusinessService.SystemManagement.Log
{
    /// <summary>
    /// 审计日志业务层实现
    /// </summary>
    public class LogService : RuYiAdminBaseService<SysLog>, ILogService
    {
        /// <summary>
        /// 审计日志仓储实例
        /// </summary>
        private readonly ILogRepository logRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="logRepository"></param>
        public LogService(ILogRepository logRepository) : base(logRepository)
        {
            this.logRepository = logRepository;
        }
    }
}
