﻿using RuYiAdmin.Net.Common.Global;
using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using RuYiAdmin.Net.Entity.CoreEntity;
using RuYiAdmin.Net.Repository.BusinessRepository.Redis;
using RuYiAdmin.Net.Repository.BusinessRepository.SystemManagement.RoleMenu;
using RuYiAdmin.Net.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Service.BusinessService.SystemManagement.RoleMenu
{
    /// <summary>
    /// 角色菜单业务层实现
    /// </summary>
    class RoleMenuService : RuYiAdminBaseService<SysRoleMenu>, IRoleMenuService
    {
        #region 属性及构造函数

        /// <summary>
        /// 角色与菜单仓储实例
        /// </summary>
        private readonly IRoleMenuRepository roleMenuRepository;

        /// <summary>
        /// Redis仓储实例
        /// </summary>
        private readonly IRedisRepository redisRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="roleMenuRepository"></param>
        public RoleMenuService(IRoleMenuRepository roleMenuRepository,
                               IRedisRepository redisRepository) : base(roleMenuRepository)
        {
            this.roleMenuRepository = roleMenuRepository;
            this.redisRepository = redisRepository;
        }

        #endregion

        #region 公有方法

        #region 加载角色与菜单缓存

        /// <summary>
        /// 加载角色与菜单缓存
        /// </summary>
        public async Task LoadSystemRoleMenuCache()
        {
            var sqlKey = "sqls:sql:query_role_menu_info";
            var strSQL = GlobalContext.Configuration.GetSection(sqlKey).Value;

            int totalCount = 0;
            var roleMenus = (await this.roleMenuRepository.SqlQueryAsync<SysRoleMenu>(new QueryCondition(), totalCount, strSQL)).ToList();

            await this.redisRepository.SetAsync(GlobalContext.SystemCacheConfig.RoleAndMenuCacheName, roleMenus, -1);
        }

        #endregion

        #region 清理角色与菜单缓存

        /// <summary>
        /// 清理角色与菜单缓存
        /// </summary>
        public async Task ClearSystemRoleMenuCache()
        {
            await this.redisRepository.DeleteAsync(new String[] { GlobalContext.SystemCacheConfig.RoleAndMenuCacheName });
        }

        #endregion

        #endregion
    }
}
