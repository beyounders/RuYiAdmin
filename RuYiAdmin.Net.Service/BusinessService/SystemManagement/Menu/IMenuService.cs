﻿using RuYiAdmin.Net.Entity.BusinessDTO.SystemManagement;
using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using RuYiAdmin.Net.Entity.CoreEntity;
using RuYiAdmin.Net.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Service.BusinessService.SystemManagement.Menu
{
    /// <summary>
    /// 菜单业务层接口
    /// </summary>
    public interface IMenuService : IRuYiAdminBaseService<SysMenu>
    {
        /// <summary>
        /// 获取菜单树
        /// </summary>
        /// <returns>ActionResult</returns>
        Task<QueryResult<SysMenuDTO>> GetMenuTreeNodes();

        /// <summary>
        /// 加载系统菜单缓存
        /// </summary>
        Task LoadSystemMenuCache();

        /// <summary>
        /// 清理系统菜单缓存
        /// </summary>
        Task ClearSystemMenuCache();
    }
}
