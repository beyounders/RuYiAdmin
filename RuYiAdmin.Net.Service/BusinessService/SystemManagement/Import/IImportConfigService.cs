﻿using RuYiAdmin.Net.Entity.BusinessDTO.SystemManagement;
using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using RuYiAdmin.Net.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Service.BusinessService.SystemManagement.Import
{
    /// <summary>
    /// 导入配置业务层接口
    /// </summary>
    public interface IImportConfigService : IRuYiAdminBaseService<SysImportConfig>
    {
        /// <summary>
        /// 获取导入配置
        /// </summary>
        /// <param name="configName">配置名称</param>
        /// <returns>配置信息</returns>
        ImportConfigDTO GetImportConfig(String configName);
    }
}
