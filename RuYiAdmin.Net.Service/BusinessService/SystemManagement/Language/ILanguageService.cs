﻿using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using RuYiAdmin.Net.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Service.BusinessService.SystemManagement.Language
{
    /// <summary>
    /// 多语业务层接口
    /// </summary>
    public interface ILanguageService : IRuYiAdminBaseService<SysLanguage>
    {
        /// <summary>
        /// 加载系统多语缓存
        /// </summary>
        Task LoadSystemLanguageCache();

        /// <summary>
        /// 清理系统多语缓存
        /// </summary>
        Task ClearSystemLanguageCache();
    }
}
