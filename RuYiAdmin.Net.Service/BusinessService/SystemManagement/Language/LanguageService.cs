﻿using RuYiAdmin.Net.Common.Global;
using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using RuYiAdmin.Net.Entity.CoreEntity;
using RuYiAdmin.Net.Repository.BusinessRepository.Redis;
using RuYiAdmin.Net.Repository.BusinessRepository.SystemManagement.Language;
using RuYiAdmin.Net.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Service.BusinessService.SystemManagement.Language
{
    /// <summary>
    /// 多语业务层实现
    /// </summary>
    public class LanguageService : RuYiAdminBaseService<SysLanguage>, ILanguageService
    {
        #region 属性及构造函数

        /// <summary>
        /// 多语仓储实例
        /// </summary>
        private readonly ILanguageRepository languageRepository;

        /// <summary>
        /// Redis仓储实例
        /// </summary>
        private readonly IRedisRepository redisRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="languageRepository"></param>
        /// <param name="redisRepository"></param>
        public LanguageService(ILanguageRepository languageRepository,
                               IRedisRepository redisRepository) : base(languageRepository)
        {
            this.languageRepository = languageRepository;
            this.redisRepository = redisRepository;
        }

        #endregion

        #region 加载系统多语缓存

        /// <summary>
        /// 加载系统多语缓存
        /// </summary>
        public async Task LoadSystemLanguageCache()
        {
            var sqlKey = "sqls:sql:query_syslanguage";
            var strSQL = GlobalContext.Configuration.GetSection(sqlKey).Value;

            int totalCount = 0;
            var languages = (await this.languageRepository.SqlQueryAsync<SysLanguage>(new QueryCondition(), totalCount, strSQL)).
                                                    OrderBy(t => t.OrderNumber).ToList();

            await this.redisRepository.SetAsync(GlobalContext.SystemCacheConfig.LanguageCacheName, languages, -1);
        }

        #endregion

        #region 清理系统多语缓存

        /// <summary>
        /// 清理系统多语缓存
        /// </summary>
        public async Task ClearSystemLanguageCache()
        {
            await this.redisRepository.DeleteAsync(new String[] { GlobalContext.SystemCacheConfig.LanguageCacheName });
        }

        #endregion
    }
}