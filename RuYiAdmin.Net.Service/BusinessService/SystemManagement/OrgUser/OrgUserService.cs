﻿using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using RuYiAdmin.Net.Repository.BusinessRepository.SystemManagement.OrgUser;
using RuYiAdmin.Net.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Service.BusinessService.SystemManagement.OrgUser
{
    /// <summary>
    /// 机构用户业务层实现
    /// </summary>
    public class OrgUserService : RuYiAdminBaseService<SysOrgUser>, IOrgUserService
    {
        /// <summary>
        /// 机构与用户仓储实例
        /// </summary>
        private readonly IOrgUserRepository orgUserRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="orgUserRepository"></param>
        public OrgUserService(IOrgUserRepository orgUserRepository) : base(orgUserRepository)
        {
            this.orgUserRepository = orgUserRepository;
        }
    }
}
