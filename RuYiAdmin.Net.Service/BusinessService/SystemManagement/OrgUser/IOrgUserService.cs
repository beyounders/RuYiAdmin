﻿using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using RuYiAdmin.Net.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Service.BusinessService.SystemManagement.OrgUser
{
    /// <summary>
    /// 机构用户业务层接口
    /// </summary>
    public interface IOrgUserService : IRuYiAdminBaseService<SysOrgUser>
    {
    }
}
