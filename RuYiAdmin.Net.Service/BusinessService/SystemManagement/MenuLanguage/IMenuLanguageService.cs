﻿using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using RuYiAdmin.Net.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Service.BusinessService.SystemManagement.MenuLanguage
{
    /// <summary>
    /// 菜单多语业务层接口
    /// </summary>
    public interface IMenuLanguageService : IRuYiAdminBaseService<SysMenuLanguage>
    {
        /// <summary>
        /// 加载菜单与多语缓存
        /// </summary>
        Task LoadSystemMenuLanguageCache();

        /// <summary>
        /// 清理菜单与多语缓存
        /// </summary>
        Task ClearSystemMenuLanguageCache();
    }
}
