﻿using RuYiAdmin.Net.Common.Global;
using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using RuYiAdmin.Net.Entity.CoreEntity;
using RuYiAdmin.Net.Repository.BusinessRepository.Redis;
using RuYiAdmin.Net.Repository.BusinessRepository.SystemManagement.MenuLanguage;
using RuYiAdmin.Net.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Service.BusinessService.SystemManagement.MenuLanguage
{
    /// <summary>
    /// 菜单多语业务层实现
    /// </summary>
    public class MenuLanguageService : RuYiAdminBaseService<SysMenuLanguage>, IMenuLanguageService
    {
        #region 属性及构造函数

        /// <summary>
        /// 菜单多语仓储实例
        /// </summary>
        private readonly IMenuLanguageRepository menuLanguageRepository;

        /// <summary>
        /// Redis仓储实例
        /// </summary>
        private readonly IRedisRepository redisRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="menuLanguageRepository"></param>
        /// <param name="redisRepository"></param>
        public MenuLanguageService(IMenuLanguageRepository menuLanguageRepository,
                                   IRedisRepository redisRepository) : base(menuLanguageRepository)
        {
            this.menuLanguageRepository = menuLanguageRepository;
            this.redisRepository = redisRepository;
        }

        #endregion

        #region 公有方法

        #region 加载菜单与多语缓存

        /// <summary>
        /// 加载菜单与多语缓存
        /// </summary>
        public async Task LoadSystemMenuLanguageCache()
        {
            var sqlKey = "sqls:sql:query_menu_language_info";
            var strSQL = GlobalContext.Configuration.GetSection(sqlKey).Value;

            int totalCount = 0;
            var menuLanguages = (await this.menuLanguageRepository.SqlQueryAsync<SysMenuLanguage>(new QueryCondition(), totalCount, strSQL)).ToList();

            await this.redisRepository.SetAsync(GlobalContext.SystemCacheConfig.MenuAndLanguageCacheName, menuLanguages, -1);
        }

        #endregion

        #region 清理菜单与多语缓存

        /// <summary>
        /// 清理菜单与多语缓存
        /// </summary>
        public async Task ClearSystemMenuLanguageCache()
        {
            await this.redisRepository.DeleteAsync(new String[] { GlobalContext.SystemCacheConfig.MenuAndLanguageCacheName });
        }

        #endregion

        #endregion
    }
}