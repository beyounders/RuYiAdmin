﻿using RuYiAdmin.Net.Common.Global;
using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using RuYiAdmin.Net.Entity.CoreEntity;
using RuYiAdmin.Net.Repository.BusinessRepository.Redis;
using RuYiAdmin.Net.Repository.BusinessRepository.SystemManagement.RoleUser;
using RuYiAdmin.Net.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Service.BusinessService.SystemManagement.RoleUser
{
    /// <summary>
    /// 角色用户业务层实现
    /// </summary>
    class RoleUserService : RuYiAdminBaseService<SysRoleUser>, IRoleUserService
    {
        #region 属性及构造函数

        /// <summary>
        /// 角色与用户仓储实例
        /// </summary>
        private readonly IRoleUserRepository roleUserRepository;

        /// <summary>
        /// Redis仓储实例
        /// </summary>
        private readonly IRedisRepository redisRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="roleUserRepository"></param>
        /// <param name="redisRepository"></param>
        public RoleUserService(IRoleUserRepository roleUserRepository,
                               IRedisRepository redisRepository) : base(roleUserRepository)
        {
            this.roleUserRepository = roleUserRepository;
            this.redisRepository = redisRepository;
        }

        #endregion

        #region 公有方法

        #region 加载角色与用户缓存

        /// <summary>
        /// 加载角色与用户缓存
        /// </summary>
        public async Task LoadSystemRoleUserCache()
        {
            var sqlKey = "sqls:sql:query_role_user_info";
            var strSQL = GlobalContext.Configuration.GetSection(sqlKey).Value;

            int totalCount = 0;
            var roleUsers = (await this.roleUserRepository.SqlQueryAsync<SysRoleUser>(new QueryCondition(), totalCount, strSQL)).ToList();

            await this.redisRepository.SetAsync(GlobalContext.SystemCacheConfig.RoleAndUserCacheName, roleUsers, -1);
        }

        #endregion

        #region 清理角色与用户缓存

        /// <summary>
        /// 清理角色与用户缓存
        /// </summary>
        public async Task ClearSystemRoleUserCache()
        {
            await this.redisRepository.DeleteAsync(new String[] { GlobalContext.SystemCacheConfig.RoleAndUserCacheName });
        }

        #endregion

        #endregion
    }
}
