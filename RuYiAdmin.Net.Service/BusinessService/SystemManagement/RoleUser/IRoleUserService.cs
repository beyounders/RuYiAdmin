﻿using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using RuYiAdmin.Net.Service.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Service.BusinessService.SystemManagement.RoleUser
{
    /// <summary>
    /// 角色用户业务层接口
    /// </summary>
    public interface IRoleUserService : IRuYiAdminBaseService<SysRoleUser>
    {
        /// <summary>
        /// 加载角色与用户缓存
        /// </summary>
        Task LoadSystemRoleUserCache();

        /// <summary>
        /// 清理角色与用户缓存
        /// </summary>
        Task ClearSystemRoleUserCache();
    }
}
