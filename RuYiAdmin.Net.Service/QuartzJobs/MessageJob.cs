﻿using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Service.QuartzJobs
{
    public class MessageJob : IJob
    {
        public MessageJob()
        {
        }

        public Task Execute(IJobExecutionContext context)
        {
            //执行定时发送短信作业
            return Task.CompletedTask;
        }
    }
}
