﻿using Microsoft.AspNetCore.Http;
using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using RuYiAdmin.Net.Repository.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Repository.BusinessRepository.SystemManagement.RoleUser
{
    /// <summary>
    /// 角色用户数据访问层实现
    /// </summary>
    public class RoleUserRepository : RuYiAdminBaseRepository<SysRoleUser>, IRoleUserRepository
    {
        /// <summary>
        /// HttpContext
        /// </summary>
        private readonly IHttpContextAccessor context;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="context"></param>
        public RoleUserRepository(IHttpContextAccessor context) : base(context)
        {
            this.context = context;
        }
    }
}
