﻿using Microsoft.AspNetCore.Http;
using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using RuYiAdmin.Net.Repository.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Repository.BusinessRepository.SystemManagement.Log
{
    /// <summary>
    /// 审计日志数据访问层实现
    /// </summary>
    public class LogRepository : RuYiAdminBaseRepository<SysLog>, ILogRepository
    {
        /// <summary>
        /// HttpContext
        /// </summary>
        private readonly IHttpContextAccessor context;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="context"></param>
        public LogRepository(IHttpContextAccessor context) : base(context)
        {
            this.context = context;
        }
    }
}
