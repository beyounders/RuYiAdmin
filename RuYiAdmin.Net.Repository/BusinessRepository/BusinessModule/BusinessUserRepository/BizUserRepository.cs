//-----------------------------------------------------------------------
// <Copyright file="BizUserRepository.cs" company="RuYiAdmin">
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// * Version : 4.0.30319.42000
// * Author  : auto generated by RuYiAdmin T4 Template
// * FileName: BizUserRepository.cs
// * History : Created by RuYiAdmin 01/21/2022 13:30:46
// </Copyright>
//-----------------------------------------------------------------------

using Microsoft.AspNetCore.Http;
using RuYiAdmin.Net.Entity.BusinessEntity.BusinessModule;
using RuYiAdmin.Net.Repository.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.Repository.BusinessRepository.BusinessModule.BusinessUserRepository
{
    /// <summary>
    /// BizUser数据访问层实现
    /// </summary>   
    public class BizUserRepository : RuYiAdminBaseRepository<BizUser>, IBizUserRepository
    {
        /// <summary>
        /// HttpContext
        /// </summary>
        private readonly IHttpContextAccessor context;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="context"></param>
        public BizUserRepository(IHttpContextAccessor context) : base(context)
        {
            this.context = context;
        }
    }
}
