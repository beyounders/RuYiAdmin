import { asyncRoutes, constantRoutes } from '@/router'
import { getToken } from '@/utils/auth'
import Layout from '@/layout'

/**
 * Use meta.role to determine if the current user has permission
 * @param roles
 * @param route
 */
function hasPermission(roles, route) {
  if (route.meta && route.meta.roles) {
    return roles.some(role => route.meta.roles.includes(role))
  } else {
    return true
  }
}

/**
 * Filter asynchronous routing tables by recursion
 * @param routes asyncRoutes
 * @param roles
 */
export function filterAsyncRoutes(routes, roles) {
  const res = []

  routes.forEach(route => {
    const tmp = { ...route }
    if (hasPermission(roles, tmp)) {
      if (tmp.children) {
        tmp.children = filterAsyncRoutes(tmp.children, roles)
      }
      res.push(tmp)
    }
  })

  return res
}

const state = {
  routes: [],
  addRoutes: []
}

const mutations = {
  SET_ROUTES: (state, routes) => {
    state.addRoutes = routes
    state.routes = constantRoutes.concat(routes)
  }
}

const actions = {
  generateRoutes({ commit }, roles) {
    return new Promise(resolve => {
      let accessedRoutes
      if (roles.includes('admin')) {
        accessedRoutes = asyncRoutes || []
      } else {
        accessedRoutes = filterAsyncRoutes(asyncRoutes, roles)
      }

      function getMenuName(item) {
        const language = localStorage.getItem('language')
        let msg = ''
        switch (language) {
          case 'zh-CN':
            msg = item.menuName
            break
          case 'en-US':
            msg = item.menuNameEn
            break
          case 'ru-RU':
            msg = item.menuNameRu
            break
          default:
            break
        }
        return msg
      }

      const menus = JSON.parse(sessionStorage.getItem(getToken() + '_permissions'))
      const codes = []
      asyncRoutes.length = 0
      menus.forEach((item, index) => {
        if (item.menuType === 0) {
          const root = {
            path: item.path,
            component: Layout,
            redirect: item.path,
            name: item.menuName,
            meta: { title: getMenuName(item), icon: item.icon }
          }
          if (item.children.length > 0) {
            root.children = []
            item.children.forEach(sub => {
              if (sub.menuType === 0) {
                root.children.push({
                  path: sub.menuUrl.replace('/', ''),
                  component: (resolve) => require(['@/views' + sub.path + sub.menuUrl], resolve),
                  name: sub.menuUrl.replace('/', ''),
                  meta: { title: getMenuName(sub), icon: sub.icon }
                })
              }
              if (sub.children.length > 0) {
                sub.children.forEach(bv => {
                  codes.push(bv.code)
                })
              }
            })
          }
          asyncRoutes.push(root)
        }
      })
      sessionStorage.setItem(getToken() + '_codes', codes.toString())
      commit('SET_ROUTES', accessedRoutes)
      resolve(accessedRoutes)
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
