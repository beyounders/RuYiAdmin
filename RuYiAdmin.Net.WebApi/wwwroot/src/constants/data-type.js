export const DataType =
  {
    // / <summary>
    // / 字符串类型
    // / </summary>
    String: 0,

    // / <summary>
    // / 日期
    // / </summary>
    Date: 1,

    // / <summary>
    // / 时间类型
    // / </summary>
    DateTime: 2,

    // / <summary>
    // / GUID
    // / </summary>
    Guid: 3,

    // / <summary>
    // / 整型
    // / </summary>
    Int: 4,

    // / <summary>
    // / 浮点型
    // / </summary>
    Double: 5
  }
