export const OperationType = {
  // / <summary>
  // / 查询列表
  // / </summary>
  QueryList: 0,

  // / <summary>
  // / 查询实体
  // / </summary>
  QueryEntity: 1,

  // / <summary>
  // / 新增实体
  // / </summary>
  AddEntity: 2,

  // / <summary>
  // / 编辑实体
  // / </summary>
  EditEntity: 3,

  // / <summary>
  // / 逻辑删除实体
  // / </summary>
  DeleteEntity: 4,

  // / <summary>
  // / 物理删除实体
  // / </summary>
  RemoveEntity: 5,

  // / <summary>
  // / 上传文件
  // / </summary>
  UploadFile: 6,

  // / <summary>
  // / 下载文件
  // / </summary>
  DownloadFile: 7,

  // / <summary>
  // / 导入数据
  // / </summary>
  ImportData: 8,

  // / <summary>
  // / 导出数据
  // / </summary>
  ExportData: 9,

  // / <summary>
  // / 菜单授权
  // / </summary>
  MenuAuthorization: 10,

  // / <summary>
  // / 用户授权
  // / </summary>
  PermissionAuthorization: 11,

  // / <summary>
  // / 打印
  // / </summary>
  Print: 12,

  // / <summary>
  // / 登录
  // / </summary>
  Logon: 13,

  // / <summary>
  // / 登出
  // / </summary>
  Logout: 14,

  // / <summary>
  // / 强制登出
  // / </summary>
  ForceLogout: 15,

  // / <summary>
  // / 修改密码
  // / </summary>
  UpdatePassword: 16,

  // / <summary>
  // / 启动计划任务
  // / </summary>
  StartScheduleJob: 17,

  // / <summary>
  // / 暂停计划任务
  // / </summary>
  PauseScheduleJob: 18,

  // / <summary>
  // / 恢复计划任务
  // / </summary>
  ResumeScheduleJob: 19,

  // / <summary>
  // / 下放权限
  // / </summary>
  DelegatePermission: 20,

  getOperationType: function(type) {
    let msg = ''
    switch (type) {
      case OperationType.QueryList:
        msg = '查询列表'
        break
      case OperationType.QueryEntity:
        msg = '查询实体'
        break
      case OperationType.AddEntity:
        msg = '新增实体'
        break
      case OperationType.EditEntity:
        msg = '编辑实体'
        break
      case OperationType.DeleteEntity:
        msg = '逻辑删除'
        break
      case OperationType.RemoveEntity:
        msg = '物理删除'
        break
      case OperationType.UploadFile:
        msg = '上传文件'
        break
      case OperationType.DownloadFile:
        msg = '下载文件'
        break
      case OperationType.ImportData:
        msg = '导入数据'
        break
      case OperationType.ExportData:
        msg = '导出数据'
        break
      case OperationType.MenuAuthorization:
        msg = '菜单授权'
        break
      case OperationType.PermissionAuthorization:
        msg = '用户授权'
        break
      case OperationType.Print:
        msg = '打印'
        break
      case OperationType.Logon:
        msg = '登录'
        break
      case OperationType.Logout:
        msg = '登出'
        break
      case OperationType.ForceLogout:
        msg = '强制登出'
        break
      case OperationType.UpdatePassword:
        msg = '更新密码'
        break
      case OperationType.StartScheduleJob:
        msg = '启动计划任务'
        break
      case OperationType.PauseScheduleJob:
        msg = '暂停计划任务'
        break
      case OperationType.ResumeScheduleJob:
        msg = '恢复计划任务'
        break
      case OperationType.DelegatePermission:
        msg = '权限下放'
        break
      default:
        break
    }
    return msg
  },
  getOpTyKyVa: function() {
    return [
      {
        value: OperationType.QueryList,
        label: '查询列表'
      },
      {
        value: OperationType.QueryEntity,
        label: '查询实体'
      },
      {
        value: OperationType.AddEntity,
        label: '新增实体'
      },
      {
        value: OperationType.EditEntity,
        label: '编辑实体'
      },
      {
        value: OperationType.DeleteEntity,
        label: '逻辑删除'
      },
      {
        value: OperationType.RemoveEntity,
        label: '物理删除'
      },
      {
        value: OperationType.UploadFile,
        label: '上传文件'
      },
      {
        value: OperationType.DownloadFile,
        label: '下载文件'
      },
      {
        value: OperationType.ImportData,
        label: '导入数据'
      },
      {
        value: OperationType.ExportData,
        label: '导出数据'
      },
      {
        value: OperationType.MenuAuthorization,
        label: '菜单授权'
      },
      {
        value: OperationType.PermissionAuthorization,
        label: '用户授权'
      },
      {
        value: OperationType.Print,
        label: '打印'
      },
      {
        value: OperationType.Logon,
        label: '登录'
      },
      {
        value: OperationType.Logout,
        label: '登出'
      },
      {
        value: OperationType.ForceLogout,
        label: '强制登出'
      },
      {
        value: OperationType.UpdatePassword,
        label: '更新密码'
      },
      {
        value: OperationType.StartScheduleJob,
        label: '启动计划任务'
      },
      {
        value: OperationType.PauseScheduleJob,
        label: '暂停计划任务'
      },
      {
        value: OperationType.ResumeScheduleJob,
        label: '恢复计划任务'
      },
      {
        value: OperationType.DelegatePermission,
        label: '权限下放'
      }
    ]
  }
}
