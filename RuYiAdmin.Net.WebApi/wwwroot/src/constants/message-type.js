export const MessageType = {
  // / <summary>
  // / 强制下线
  // / </summary>
  ForceLogout: 0,

  // / <summary>
  // / 通知
  // / </summary>
  Notice: 1,

  // / <summary>
  // / 公告
  // / </summary>
  Announcement: 2,

  // / <summary>
  // / 广播
  // / </summary>
  Broadcast: 3
}
