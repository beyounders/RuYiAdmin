alter table BIZ_ACCOUNT
   drop constraint FK_BIZ_ACCO_REFERENCE_BIZ_MODU;

alter table BIZ_USER_MODULE
   drop constraint FK_BIZ_USER_REFERENCE_BIZ_MODU;

alter table BIZ_USER_MODULE
   drop constraint FK_BIZ_USER_REFERENCE_BIZ_USER;

alter table SYS_IMPORT_CONFIG_DETAIL
   drop constraint FK_SYS_IMPO_REFERENCE_SYS_IMPO;

alter table SYS_MENU_LANGUAGE
   drop constraint FK_SYS_MENU_REFERENCE_SYS_LANG;

alter table SYS_MENU_LANGUAGE
   drop constraint FK_SYS_MENU_REFERENCE_SYS_MENU;

alter table SYS_ORG_USER
   drop constraint FK_SYS_ORG__REFERENCE_SYS_USER;

alter table SYS_ORG_USER
   drop constraint FK_SYS_ORG__REFERENCE_SYS_ORGA;

alter table SYS_ROLE_MENU
   drop constraint FK_SYS_ROLE_REFERENCE_SYS_ROLE;

alter table SYS_ROLE_MENU
   drop constraint FK_SYS_ROLE_REFERENCE_SYS_MENU;

alter table SYS_ROLE_ORG
   drop constraint FK_SYS_ROLE_REFERENCE_SYS_ORGA;

alter table SYS_ROLE_ORG
   drop constraint FK_SYS_ROLE_ORG_SYS_ROLE;

alter table SYS_ROLE_USER
   drop constraint FK_SYS_ROLE_USER_SYS_ROLE;

alter table SYS_ROLE_USER
   drop constraint FK_SYS_ROLE_REFERENCE_SYS_USER;

drop table BIZ_ACCOUNT cascade constraints;

drop table BIZ_MODULE cascade constraints;

drop index USERLOGONNAME_UNIQUE;

drop table BIZ_USER cascade constraints;

drop index FK_UM_UID_IDX;

drop index FK_UM_MID_IDX;

drop table BIZ_USER_MODULE cascade constraints;

drop table SYS_CODE_TABLE cascade constraints;

drop table SYS_IMPORT_CONFIG cascade constraints;

drop table SYS_IMPORT_CONFIG_DETAIL cascade constraints;

drop table SYS_LANGUAGE cascade constraints;

drop table SYS_LOG cascade constraints;

drop table SYS_MENU cascade constraints;

drop table SYS_MENU_LANGUAGE cascade constraints;

drop table SYS_ORGANIZATION cascade constraints;

drop table SYS_ORG_USER cascade constraints;

drop table SYS_ROLE cascade constraints;

drop table SYS_ROLE_MENU cascade constraints;

drop table SYS_ROLE_ORG cascade constraints;

drop table SYS_ROLE_USER cascade constraints;

drop table SYS_SCHEDULE_JOB cascade constraints;

drop table SYS_USER cascade constraints;

--drop user RUYIADMIN;

/*==============================================================*/
/* User: RUYIADMIN                                              */
/*==============================================================*/
--create user RUYIADMIN 
--  identified by "";

/*==============================================================*/
/* Table: BIZ_ACCOUNT                                           */
/*==============================================================*/
create table BIZ_ACCOUNT 
(
   ID                   NVARCHAR2(36)        not null,
   MODULEID             NVARCHAR2(36)        not null,
   USERLOGONNAME        NVARCHAR2(128)       not null,
   USERDISPLAYNAME      NVARCHAR2(128)       not null,
   USERPASSWORD         NVARCHAR2(512)       not null,
   TELEPHONE            NVARCHAR2(45),
   MOBILEPHONE          NVARCHAR2(45),
   EMAIL                NVARCHAR2(45),
   ISENABLED            NUMBER(11)           not null,
   REMARK               NVARCHAR2(512),
   ISDEL                NUMBER(11)           not null,
   CREATOR              NVARCHAR2(36)        not null,
   CREATETIME           TIMESTAMP(6)         not null,
   MODIFIER             NVARCHAR2(36)        not null,
   MODIFYTIME           TIMESTAMP(6)         not null,
   constraint PK_BIZ_ACCOUNT primary key (ID)
         using index 
     tablespace USERS
     pctfree 10
     initrans 2
     maxtrans 255
     storage
     (
       initial 64K
       next 1M
       minextents 1
       maxextents unlimited
     )
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

comment on table BIZ_ACCOUNT is
'模块API访问账号表';

comment on column BIZ_ACCOUNT.ID is
'用户编号';

comment on column BIZ_ACCOUNT.MODULEID is
'模块编号';

comment on column BIZ_ACCOUNT.USERLOGONNAME is
'用户登录账号';

comment on column BIZ_ACCOUNT.USERDISPLAYNAME is
'用户名称';

comment on column BIZ_ACCOUNT.USERPASSWORD is
'用户密码';

comment on column BIZ_ACCOUNT.TELEPHONE is
'座机';

comment on column BIZ_ACCOUNT.MOBILEPHONE is
'手机';

comment on column BIZ_ACCOUNT.EMAIL is
'邮箱';

comment on column BIZ_ACCOUNT.ISENABLED is
'是否启用，0：禁用，1：启用';

comment on column BIZ_ACCOUNT.REMARK is
'备注';

comment on column BIZ_ACCOUNT.ISDEL is
'标志位';

comment on column BIZ_ACCOUNT.CREATOR is
'创建人';

comment on column BIZ_ACCOUNT.CREATETIME is
'创建时间';

comment on column BIZ_ACCOUNT.MODIFIER is
'修改人';

comment on column BIZ_ACCOUNT.MODIFYTIME is
'修改时间';
/*==============================================================*/
/* Table: BIZ_MODULE                                            */
/*==============================================================*/
create table BIZ_MODULE 
(
   ID                   NVARCHAR2(36)        not null,
   MODULENAME           NVARCHAR2(512)       not null,
   MODULESHORTNAME      NVARCHAR2(256)       not null,
   MODULESHORTNAMEEN    NVARCHAR2(128)       not null,
   MODULEPROTOCOL       NVARCHAR2(15)        not null,
   MODULEADDRESS        NVARCHAR2(256)       not null,
   MODULEPORT           NUMBER(11),
   MODULELOGOADDRESS    NVARCHAR2(512),
   MODULESSOADDRESS     NVARCHAR2(512)       not null,
   MODULETODOADDRESS    NVARCHAR2(512),
   SERIALNUMBER         NUMBER(11),
   REMARK               NVARCHAR2(512),
   ISDEL                NUMBER(11)           not null,
   CREATOR              NVARCHAR2(36)        not null,
   CREATETIME           TIMESTAMP(6)         not null,
   MODIFIER             NVARCHAR2(36)        not null,
   MODIFYTIME           TIMESTAMP(6)         not null,
   constraint PK_BIZ_MODULE primary key (ID)
         using index 
     tablespace USERS
     pctfree 10
     initrans 2
     maxtrans 255
     storage
     (
       initial 64K
       next 1M
       minextents 1
       maxextents unlimited
     )
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

comment on table BIZ_MODULE is
'模块表';

comment on column BIZ_MODULE.ID is
'模块编号';

comment on column BIZ_MODULE.MODULENAME is
'模块名称';

comment on column BIZ_MODULE.MODULESHORTNAME is
'模块简称';

comment on column BIZ_MODULE.MODULESHORTNAMEEN is
'模块英文简称';

comment on column BIZ_MODULE.MODULEPROTOCOL is
'采用HTTP协议，HTTP或者HTTPS';

comment on column BIZ_MODULE.MODULEADDRESS is
'模块地址：ip或者域名';

comment on column BIZ_MODULE.MODULEPORT is
'模块端口';

comment on column BIZ_MODULE.MODULELOGOADDRESS is
'模块logo图片位置';

comment on column BIZ_MODULE.MODULESSOADDRESS is
'模块单点登录地址';

comment on column BIZ_MODULE.MODULETODOADDRESS is
'模块待办地址';

comment on column BIZ_MODULE.SERIALNUMBER is
'序号';

comment on column BIZ_MODULE.REMARK is
'备注';

comment on column BIZ_MODULE.ISDEL is
'标志位';

comment on column BIZ_MODULE.CREATOR is
'创建人';

comment on column BIZ_MODULE.CREATETIME is
'创建时间';

comment on column BIZ_MODULE.MODIFIER is
'修改人';

comment on column BIZ_MODULE.MODIFYTIME is
'修改时间';

/*==============================================================*/
/* Table: BIZ_USER                                              */
/*==============================================================*/
create table BIZ_USER 
(
   ID                   NVARCHAR2(36)        not null,
   USERLOGONNAME        NVARCHAR2(128)       not null,
   USERDISPLAYNAME      NVARCHAR2(128)       not null,
   USERPASSWORD         NVARCHAR2(512)       not null,
   TELEPHONE            NVARCHAR2(45),
   MOBILEPHONE          NVARCHAR2(45),
   EMAIL                NVARCHAR2(45),
   ISENABLED            NUMBER(11)           not null,
   REMARK               NVARCHAR2(512),
   ISDEL                NUMBER(11)           not null,
   CREATOR              NVARCHAR2(36)        not null,
   CREATETIME           TIMESTAMP(6)         not null,
   MODIFIER             NVARCHAR2(36)        not null,
   MODIFYTIME           TIMESTAMP(6)         not null,
   constraint PK_BIZ_USER primary key (ID)
         using index 
     tablespace USERS
     pctfree 10
     initrans 2
     maxtrans 255
     storage
     (
       initial 64K
       next 1M
       minextents 1
       maxextents unlimited
     )
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

comment on table BIZ_USER is
'模块用户表';

comment on column BIZ_USER.ID is
'用户编号';

comment on column BIZ_USER.USERLOGONNAME is
'用户登录账号';

comment on column BIZ_USER.USERDISPLAYNAME is
'用户名称';

comment on column BIZ_USER.USERPASSWORD is
'用户密码';

comment on column BIZ_USER.TELEPHONE is
'座机';

comment on column BIZ_USER.MOBILEPHONE is
'手机';

comment on column BIZ_USER.EMAIL is
'邮箱';

comment on column BIZ_USER.ISENABLED is
'是否启用，0：禁用，1：启用';

comment on column BIZ_USER.REMARK is
'备注';

comment on column BIZ_USER.ISDEL is
'标志位';

comment on column BIZ_USER.CREATOR is
'创建人';

comment on column BIZ_USER.CREATETIME is
'创建时间';

comment on column BIZ_USER.MODIFIER is
'修改人';

comment on column BIZ_USER.MODIFYTIME is
'修改时间';

/*==============================================================*/
/* Index: USERLOGONNAME_UNIQUE                                  */
/*==============================================================*/
create unique index USERLOGONNAME_UNIQUE on BIZ_USER (
   USERLOGONNAME ASC
)
tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

/*==============================================================*/
/* Table: BIZ_USER_MODULE                                       */
/*==============================================================*/
create table BIZ_USER_MODULE 
(
   ID                   NVARCHAR2(36)        not null,
   USERID               NVARCHAR2(36)        not null,
   MODULEID             NVARCHAR2(36)        not null,
   USERMODULELOGONNAME  NVARCHAR2(128)       not null,
   USERMODULEPASSWORD   NVARCHAR2(512)       not null,
   REMARK               NVARCHAR2(512),
   ISDEL                NUMBER(11)           not null,
   CREATOR              NVARCHAR2(36)        not null,
   CREATETIME           TIMESTAMP(6)         not null,
   MODIFIER             NVARCHAR2(36)        not null,
   MODIFYTIME           TIMESTAMP(6)         not null,
   constraint PK_BIZ_USER_MODULE primary key (ID)
         using index 
     tablespace USERS
     pctfree 10
     initrans 2
     maxtrans 255
     storage
     (
       initial 64K
       next 1M
       minextents 1
       maxextents unlimited
     )
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

comment on table BIZ_USER_MODULE is
'模块与用户关系表';

comment on column BIZ_USER_MODULE.ID is
'主键';

comment on column BIZ_USER_MODULE.USERID is
'用户编号';

comment on column BIZ_USER_MODULE.MODULEID is
'模块编号';

comment on column BIZ_USER_MODULE.USERMODULELOGONNAME is
'用户所在模块登录账号';

comment on column BIZ_USER_MODULE.USERMODULEPASSWORD is
'用户所在模块登录密码';

comment on column BIZ_USER_MODULE.REMARK is
'备注';

comment on column BIZ_USER_MODULE.ISDEL is
'标志位';

comment on column BIZ_USER_MODULE.CREATOR is
'创建人';

comment on column BIZ_USER_MODULE.CREATETIME is
'创建时间';

comment on column BIZ_USER_MODULE.MODIFIER is
'修改人';

comment on column BIZ_USER_MODULE.MODIFYTIME is
'修改时间';

/*==============================================================*/
/* Index: FK_UM_MID_IDX                                         */
/*==============================================================*/
create index FK_UM_MID_IDX on BIZ_USER_MODULE (
   MODULEID ASC
)
tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

/*==============================================================*/
/* Index: FK_UM_UID_IDX                                         */
/*==============================================================*/
create index FK_UM_UID_IDX on BIZ_USER_MODULE (
   USERID ASC
)
tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

/*==============================================================*/
/* Table: SYS_CODE_TABLE                                        */
/*==============================================================*/
create table SYS_CODE_TABLE 
(
   ID                   NVARCHAR2(36)        not null,
   CODENAME             NVARCHAR2(128)       not null,
   CODE                 NVARCHAR2(128)       not null,
   VALUE                NVARCHAR2(256),
   PARENTID             NVARCHAR2(36),
   REMARK               NVARCHAR2(512),
   SERIALNUMBER         NUMBER(11),
   ISDEL                NUMBER(11)           not null,
   CREATOR              NVARCHAR2(36)        not null,
   CREATETIME           TIMESTAMP(6)         not null,
   MODIFIER             NVARCHAR2(36)        not null,
   MODIFYTIME           TIMESTAMP(6)         not null,
   constraint PK_SYS_CODE_TABLE primary key (ID)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

comment on table SYS_CODE_TABLE is
'字典表';

comment on column SYS_CODE_TABLE.ID is
'主键';

comment on column SYS_CODE_TABLE.CODENAME is
'名称';

comment on column SYS_CODE_TABLE.CODE is
'编码';

comment on column SYS_CODE_TABLE.VALUE is
'值';

comment on column SYS_CODE_TABLE.PARENTID is
'父键';

comment on column SYS_CODE_TABLE.REMARK is
'备注';

comment on column SYS_CODE_TABLE.SERIALNUMBER is
'序号';

comment on column SYS_CODE_TABLE.ISDEL is
'标志位';

comment on column SYS_CODE_TABLE.CREATOR is
'创建人';

comment on column SYS_CODE_TABLE.CREATETIME is
'创建时间';

comment on column SYS_CODE_TABLE.MODIFIER is
'修改人';

comment on column SYS_CODE_TABLE.MODIFYTIME is
'修改时间';

/*==============================================================*/
/* Table: SYS_IMPORT_CONFIG                                     */
/*==============================================================*/
create table SYS_IMPORT_CONFIG 
(
   ID                   NVARCHAR2(36)        not null,
   CONFIGNAME           NVARCHAR2(128)       not null,
   STARTROW             NUMBER(11)           not null,
   STARTCOLUMN          NUMBER(11)           not null,
   WORKSHEETINDEXES     NVARCHAR2(512),
   SERIALNUMBER         NUMBER(11),
   REMARK               NVARCHAR2(512),
   ISDEL                NUMBER(11)           not null,
   CREATOR              NVARCHAR2(36)        not null,
   CREATETIME           TIMESTAMP(6)         not null,
   MODIFIER             NVARCHAR2(36)        not null,
   MODIFYTIME           TIMESTAMP(6)         not null,
   constraint PK_SYS_IMPORT_CONFIG primary key (ID)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

comment on table SYS_IMPORT_CONFIG is
'导入配置主表';

comment on column SYS_IMPORT_CONFIG.ID is
'主键';

comment on column SYS_IMPORT_CONFIG.CONFIGNAME is
'配置名称';

comment on column SYS_IMPORT_CONFIG.STARTROW is
'起始行';

comment on column SYS_IMPORT_CONFIG.STARTCOLUMN is
'起始列';

comment on column SYS_IMPORT_CONFIG.WORKSHEETINDEXES is
'工作簿索引列表';

comment on column SYS_IMPORT_CONFIG.REMARK is
'备注';

comment on column SYS_IMPORT_CONFIG.ISDEL is
'标志位';

comment on column SYS_IMPORT_CONFIG.CREATOR is
'创建人';

comment on column SYS_IMPORT_CONFIG.CREATETIME is
'创建时间';

comment on column SYS_IMPORT_CONFIG.MODIFIER is
'修改人';

comment on column SYS_IMPORT_CONFIG.MODIFYTIME is
'修改时间';

/*==============================================================*/
/* Table: SYS_IMPORT_CONFIG_DETAIL                              */
/*==============================================================*/
create table SYS_IMPORT_CONFIG_DETAIL 
(
   ID                   NVARCHAR2(36)        not null,
   PARENTID             NVARCHAR2(36)        not null,
   DATATYPE             NUMBER(11)           not null,
   CELLS                NVARCHAR2(512)       not null,
   REQUIRED             NUMBER(11),
   MAXVALUE             NUMBER,
   MINVALUE             NUMBER,
   DECIMALLIMIT         NUMBER(11),
   TEXTENUM             NVARCHAR2(1024),
   EXTEND1              NVARCHAR2(64),
   EXTEND2              NVARCHAR2(128),
   EXTEND3              NVARCHAR2(256),
   SERIALNUMBER         NUMBER(11),
   REMARK               NVARCHAR2(512),
   ISDEL                NUMBER(11)           not null,
   CREATOR              NVARCHAR2(36)        not null,
   CREATETIME           TIMESTAMP(6)         not null,
   MODIFIER             NVARCHAR2(36)        not null,
   MODIFYTIME           TIMESTAMP(6)         not null,
   constraint PK_SYS_IMPORT_CONFIG_DETAIL primary key (ID)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

comment on table SYS_IMPORT_CONFIG_DETAIL is
'导入配置子表';

comment on column SYS_IMPORT_CONFIG_DETAIL.ID is
'主键';

comment on column SYS_IMPORT_CONFIG_DETAIL.PARENTID is
'父键';

comment on column SYS_IMPORT_CONFIG_DETAIL.DATATYPE is
'数据类型，0：小数，1：整数，2：文本t，3：日期，4：时间';

comment on column SYS_IMPORT_CONFIG_DETAIL.CELLS is
'所在列';

comment on column SYS_IMPORT_CONFIG_DETAIL.REQUIRED is
'是否必填项，0：否，1：是';

comment on column SYS_IMPORT_CONFIG_DETAIL.MAXVALUE is
'最大值';

comment on column SYS_IMPORT_CONFIG_DETAIL.MINVALUE is
'最小值';

comment on column SYS_IMPORT_CONFIG_DETAIL.DECIMALLIMIT is
'小数位上限';

comment on column SYS_IMPORT_CONFIG_DETAIL.TEXTENUM is
'枚举列表';

comment on column SYS_IMPORT_CONFIG_DETAIL.EXTEND1 is
'扩展字段';

comment on column SYS_IMPORT_CONFIG_DETAIL.EXTEND2 is
'扩展字段';

comment on column SYS_IMPORT_CONFIG_DETAIL.EXTEND3 is
'扩展字段';

comment on column SYS_IMPORT_CONFIG_DETAIL.SERIALNUMBER is
'序号';

comment on column SYS_IMPORT_CONFIG_DETAIL.REMARK is
'备注';

comment on column SYS_IMPORT_CONFIG_DETAIL.ISDEL is
'标志位';

comment on column SYS_IMPORT_CONFIG_DETAIL.CREATOR is
'创建人';

comment on column SYS_IMPORT_CONFIG_DETAIL.CREATETIME is
'创建时间';

comment on column SYS_IMPORT_CONFIG_DETAIL.MODIFIER is
'修改人';

comment on column SYS_IMPORT_CONFIG_DETAIL.MODIFYTIME is
'修改时间';

/*==============================================================*/
/* Table: SYS_LANGUAGE                                          */
/*==============================================================*/
create table SYS_LANGUAGE 
(
   ID                   NVARCHAR2(36)        not null,
   LANGUAGENAME         NVARCHAR2(45)        not null,
   ORDERNUMBER          NUMBER(11),
   REMARK               NVARCHAR2(512),
   ISDEL                NUMBER(11)           not null,
   CREATOR              NVARCHAR2(36)        not null,
   CREATETIME           TIMESTAMP(6)         not null,
   MODIFIER             NVARCHAR2(36)        not null,
   MODIFYTIME           TIMESTAMP(6)         not null,
   constraint PK_SYS_LANGUAGE primary key (ID)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

comment on table SYS_LANGUAGE is
'系统语言';

comment on column SYS_LANGUAGE.ID is
'主键';

comment on column SYS_LANGUAGE.LANGUAGENAME is
'名称';

comment on column SYS_LANGUAGE.ORDERNUMBER is
'序号';

comment on column SYS_LANGUAGE.REMARK is
'备注';

comment on column SYS_LANGUAGE.ISDEL is
'标志位';

comment on column SYS_LANGUAGE.CREATOR is
'创建人';

comment on column SYS_LANGUAGE.CREATETIME is
'创建时间';

comment on column SYS_LANGUAGE.MODIFIER is
'修改人';

comment on column SYS_LANGUAGE.MODIFYTIME is
'修改时间';

/*==============================================================*/
/* Table: SYS_LOG                                               */
/*==============================================================*/
create table SYS_LOG 
(
   ID                   NVARCHAR2(36)        not null,
   USERID               NVARCHAR2(36)        not null,
   USERNAME             NVARCHAR2(128)       not null,
   ORGID                NVARCHAR2(36)        not null,
   ORGNAME              NVARCHAR2(128)       not null,
   SYSTEM               NVARCHAR2(128),
   BROWSER              NVARCHAR2(128),
   IP                   NVARCHAR2(45)        not null,
   OPERATIONTYPE        NUMBER(11)           not null,
   REQUESTURL           NVARCHAR2(512)       not null,
   PARAMS               NVARCHAR2(1024),
   RESULT               CLOB,
   OLDVAUE              CLOB,
   NEWVALUE             CLOB,
   REMARK               NVARCHAR2(512),
   ISDEL                NUMBER(11)           not null,
   CREATOR              NVARCHAR2(36)        not null,
   CREATETIME           TIMESTAMP(6)         not null,
   MODIFIER             NVARCHAR2(36)        not null,
   MODIFYTIME           TIMESTAMP(6)         not null,
   constraint PK_SYS_LOG primary key (ID)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

comment on table SYS_LOG is
'审计日志表';

comment on column SYS_LOG.ID is
'主键';

comment on column SYS_LOG.USERID is
'用户编号';

comment on column SYS_LOG.USERNAME is
'用户姓名';

comment on column SYS_LOG.ORGID is
'机构编号';

comment on column SYS_LOG.ORGNAME is
'机构名称';

comment on column SYS_LOG.SYSTEM is
'使用系统';

comment on column SYS_LOG.BROWSER is
'浏览器';

comment on column SYS_LOG.IP is
'IP地址';

comment on column SYS_LOG.OPERATIONTYPE is
'操作类型，0：查询列表，1：查询记录，2：新增，3：编辑，4：逻辑删除，5：物理删除，6：上传，7：下载，8：导入，9：导出，10：菜单授权，11：用户授权，12：打印，13：登录，14：登出，15：强制登出，16：更新密码，17：添加计划任务，18：暂停计划任务，19：启动计划任务';

comment on column SYS_LOG.REQUESTURL is
'请求路径';

comment on column SYS_LOG.PARAMS is
'请求参数';

comment on column SYS_LOG.RESULT is
'返回结果';

comment on column SYS_LOG.OLDVAUE is
'旧值';

comment on column SYS_LOG.NEWVALUE is
'新值';

comment on column SYS_LOG.REMARK is
'备注';

comment on column SYS_LOG.ISDEL is
'标志位';

comment on column SYS_LOG.CREATOR is
'创建人';

comment on column SYS_LOG.CREATETIME is
'创建时间';

comment on column SYS_LOG.MODIFIER is
'修改人';

comment on column SYS_LOG.MODIFYTIME is
'修改时间';

/*==============================================================*/
/* Table: SYS_MENU                                              */
/*==============================================================*/
create table SYS_MENU 
(
   ID                   NVARCHAR2(36)        not null,
   PATH                 NVARCHAR2(256),
   MENUNAME             NVARCHAR2(128)       not null,
   MENUURL              NVARCHAR2(256),
   PARENTID             NVARCHAR2(36),
   SERIALNUMBER         NUMBER(11),
   MENUTYPE             NUMBER(11)           not null,
   ICON                 NVARCHAR2(256),
   CODE                 NVARCHAR2(125),
   REMARK               NVARCHAR2(512),
   ISDEL                NUMBER(11)           not null,
   CREATOR              NVARCHAR2(36)        not null,
   CREATETIME           TIMESTAMP(6)         not null,
   MODIFIER             NVARCHAR2(36)        not null,
   MODIFYTIME           TIMESTAMP(6)         not null,
   constraint PK_SYS_MENU primary key (ID)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

comment on table SYS_MENU is
'菜单表';

comment on column SYS_MENU.ID is
'主键';

comment on column SYS_MENU.PATH is
'根路径';

comment on column SYS_MENU.MENUNAME is
'角色名称';

comment on column SYS_MENU.MENUURL is
'菜单路径';

comment on column SYS_MENU.PARENTID is
'父键';

comment on column SYS_MENU.SERIALNUMBER is
'序号';

comment on column SYS_MENU.MENUTYPE is
'菜单类型，0：菜单，1：按钮，视图：2';

comment on column SYS_MENU.ICON is
'图标';

comment on column SYS_MENU.CODE is
'编码';

comment on column SYS_MENU.REMARK is
'备注';

comment on column SYS_MENU.ISDEL is
'标志位';

comment on column SYS_MENU.CREATOR is
'创建人';

comment on column SYS_MENU.CREATETIME is
'创建时间';

comment on column SYS_MENU.MODIFIER is
'修改人';

comment on column SYS_MENU.MODIFYTIME is
'修改时间';

/*==============================================================*/
/* Table: SYS_MENU_LANGUAGE                                     */
/*==============================================================*/
create table SYS_MENU_LANGUAGE 
(
   ID                   NVARCHAR2(36)        not null,
   MENUID               NVARCHAR2(36)        not null,
   LANGUAGEID           NVARCHAR2(36)        not null,
   MENUNAME             NVARCHAR2(128)       not null,
   REMARK               NVARCHAR2(512),
   ISDEL                NUMBER(11)           not null,
   CREATOR              NVARCHAR2(36)        not null,
   CREATETIME           TIMESTAMP(6)         not null,
   MODIFIER             NVARCHAR2(36)        not null,
   MODIFYTIME           TIMESTAMP(6)         not null,
   constraint PK_SYS_MENU_LANGUAGE primary key (ID)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

comment on table SYS_MENU_LANGUAGE is
'菜单多语表';

comment on column SYS_MENU_LANGUAGE.ID is
'主键';

comment on column SYS_MENU_LANGUAGE.MENUID is
'菜单编号';

comment on column SYS_MENU_LANGUAGE.LANGUAGEID is
'语言编号';

comment on column SYS_MENU_LANGUAGE.MENUNAME is
'菜单名称';

comment on column SYS_MENU_LANGUAGE.REMARK is
'备注';

comment on column SYS_MENU_LANGUAGE.ISDEL is
'标志位';

comment on column SYS_MENU_LANGUAGE.CREATOR is
'创建人';

comment on column SYS_MENU_LANGUAGE.CREATETIME is
'创建时间';

comment on column SYS_MENU_LANGUAGE.MODIFIER is
'修改人';

comment on column SYS_MENU_LANGUAGE.MODIFYTIME is
'修改时间';

/*==============================================================*/
/* Table: SYS_ORGANIZATION                                      */
/*==============================================================*/
create table SYS_ORGANIZATION 
(
   ID                   NVARCHAR2(36)        not null,
   ORGNAME              NVARCHAR2(128)       not null,
   PARENTID             NVARCHAR2(36),
   LEADER               NVARCHAR2(36),
   SERIALNUMBER         NUMBER(11),
   REMARK               NVARCHAR2(512),
   ISDEL                NUMBER(11)           not null,
   CREATOR              NVARCHAR2(36)        not null,
   CREATETIME           TIMESTAMP(6)         not null,
   MODIFIER             NVARCHAR2(36)        not null,
   MODIFYTIME           TIMESTAMP(6)         not null,
   constraint PK_SYS_ORGANIZATION primary key (ID)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

comment on table SYS_ORGANIZATION is
'机构表';

comment on column SYS_ORGANIZATION.ID is
'主键';

comment on column SYS_ORGANIZATION.ORGNAME is
'机构名称';

comment on column SYS_ORGANIZATION.PARENTID is
'父键';

comment on column SYS_ORGANIZATION.LEADER is
'主管人';

comment on column SYS_ORGANIZATION.SERIALNUMBER is
'序号';

comment on column SYS_ORGANIZATION.REMARK is
'备注';

comment on column SYS_ORGANIZATION.ISDEL is
'标志位';

comment on column SYS_ORGANIZATION.CREATOR is
'创建人';

comment on column SYS_ORGANIZATION.CREATETIME is
'创建时间';

comment on column SYS_ORGANIZATION.MODIFIER is
'修改人';

comment on column SYS_ORGANIZATION.MODIFYTIME is
'修改时间';

/*==============================================================*/
/* Table: SYS_ORG_USER                                          */
/*==============================================================*/
create table SYS_ORG_USER 
(
   ID                   NVARCHAR2(36)        not null,
   ORGID                NVARCHAR2(36)        not null,
   USERID               NVARCHAR2(36)        not null,
   REMARK               NVARCHAR2(512),
   ISDEL                NUMBER(11)           not null,
   CREATOR              NVARCHAR2(36)        not null,
   CREATETIME           TIMESTAMP(6)         not null,
   MODIFIER             NVARCHAR2(36)        not null,
   MODIFYTIME           TIMESTAMP(6)         not null,
   constraint PK_SYS_ORG_USER primary key (ID)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

comment on table SYS_ORG_USER is
'机构用户关系表';

comment on column SYS_ORG_USER.ID is
'主键';

comment on column SYS_ORG_USER.ORGID is
'机构编号';

comment on column SYS_ORG_USER.USERID is
'用户编号';

comment on column SYS_ORG_USER.REMARK is
'备注';

comment on column SYS_ORG_USER.ISDEL is
'标志位';

comment on column SYS_ORG_USER.CREATOR is
'创建人';

comment on column SYS_ORG_USER.CREATETIME is
'创建时间';

comment on column SYS_ORG_USER.MODIFIER is
'修改人';

comment on column SYS_ORG_USER.MODIFYTIME is
'修改时间';

/*==============================================================*/
/* Table: SYS_ROLE                                              */
/*==============================================================*/
create table SYS_ROLE 
(
   ID                   NVARCHAR2(36)        not null,
   ROLENAME             NVARCHAR2(128)       not null,
   SERIALNUMBER         NUMBER(11),
   REMARK               NVARCHAR2(512),
   ISDEL                NUMBER(11)           not null,
   CREATOR              NVARCHAR2(36)        not null,
   CREATETIME           TIMESTAMP(6)         not null,
   MODIFIER             NVARCHAR2(36)        not null,
   MODIFYTIME           TIMESTAMP(6)         not null,
   constraint PK_SYS_ROLE primary key (ID)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

comment on table SYS_ROLE is
'角色表';

comment on column SYS_ROLE.ID is
'主键';

comment on column SYS_ROLE.ROLENAME is
'角色名称';

comment on column SYS_ROLE.SERIALNUMBER is
'序号';

comment on column SYS_ROLE.REMARK is
'备注';

comment on column SYS_ROLE.ISDEL is
'标志位';

comment on column SYS_ROLE.CREATOR is
'创建人';

comment on column SYS_ROLE.CREATETIME is
'创建时间';

comment on column SYS_ROLE.MODIFIER is
'修改人';

comment on column SYS_ROLE.MODIFYTIME is
'修改时间';

/*==============================================================*/
/* Table: SYS_ROLE_MENU                                         */
/*==============================================================*/
create table SYS_ROLE_MENU 
(
   ID                   NVARCHAR2(36)        not null,
   ROLEID               NVARCHAR2(36)        not null,
   MENUID               NVARCHAR2(36)        not null,
   REMARK               NVARCHAR2(512),
   ISDEL                NUMBER(11)           not null,
   CREATOR              NVARCHAR2(36)        not null,
   CREATETIME           TIMESTAMP(6)         not null,
   MODIFIER             NVARCHAR2(36)        not null,
   MODIFYTIME           TIMESTAMP(6)         not null,
   constraint PK_SYS_ROLE_MENU primary key (ID)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

comment on table SYS_ROLE_MENU is
'角色菜单关系表';

comment on column SYS_ROLE_MENU.ID is
'主键';

comment on column SYS_ROLE_MENU.ROLEID is
'角色编号';

comment on column SYS_ROLE_MENU.MENUID is
'菜单编号';

comment on column SYS_ROLE_MENU.REMARK is
'备注';

comment on column SYS_ROLE_MENU.ISDEL is
'标志位';

comment on column SYS_ROLE_MENU.CREATOR is
'创建人';

comment on column SYS_ROLE_MENU.CREATETIME is
'创建时间';

comment on column SYS_ROLE_MENU.MODIFIER is
'修改人';

comment on column SYS_ROLE_MENU.MODIFYTIME is
'修改时间';

/*==============================================================*/
/* Table: SYS_ROLE_ORG                                          */
/*==============================================================*/
create table SYS_ROLE_ORG 
(
   ID                   NVARCHAR2(36)        not null,
   ROLEID               NVARCHAR2(36)        not null,
   ORGID                NVARCHAR2(36)        not null,
   OWNERTYPE            NUMBER(11)           not null,
   REMARK               NVARCHAR2(512),
   ISDEL                NUMBER(11)           not null,
   CREATOR              NVARCHAR2(36)        not null,
   CREATETIME           TIMESTAMP(6)         not null,
   MODIFIER             NVARCHAR2(36)        not null,
   MODIFYTIME           TIMESTAMP(6)         not null,
   constraint PK_SYS_ROLE_ORG primary key (ID)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

comment on table SYS_ROLE_ORG is
'角色机构关系表';

comment on column SYS_ROLE_ORG.ID is
'主键';

comment on column SYS_ROLE_ORG.ROLEID is
'角色编号';

comment on column SYS_ROLE_ORG.ORGID is
'机构编号';

comment on column SYS_ROLE_ORG.OWNERTYPE is
'类型：0，自有；1继承';

comment on column SYS_ROLE_ORG.REMARK is
'备注';

comment on column SYS_ROLE_ORG.ISDEL is
'标志位';

comment on column SYS_ROLE_ORG.CREATOR is
'创建人';

comment on column SYS_ROLE_ORG.CREATETIME is
'创建时间';

comment on column SYS_ROLE_ORG.MODIFIER is
'修改人';

comment on column SYS_ROLE_ORG.MODIFYTIME is
'修改时间';

/*==============================================================*/
/* Table: SYS_ROLE_USER                                         */
/*==============================================================*/
create table SYS_ROLE_USER 
(
   ID                   NVARCHAR2(36)        not null,
   ROLEID               NVARCHAR2(36)        not null,
   USERID               NVARCHAR2(36)        not null,
   REMARK               NVARCHAR2(512),
   ISDEL                NUMBER(11)           not null,
   CREATOR              NVARCHAR2(36)        not null,
   CREATETIME           TIMESTAMP(6)         not null,
   MODIFIER             NVARCHAR2(36)        not null,
   MODIFYTIME           TIMESTAMP(6)         not null,
   constraint PK_SYS_ROLE_USER primary key (ID)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

comment on table SYS_ROLE_USER is
'角色用户关系表';

comment on column SYS_ROLE_USER.ID is
'主键';

comment on column SYS_ROLE_USER.ROLEID is
'角色编号';

comment on column SYS_ROLE_USER.USERID is
'用户编号';

comment on column SYS_ROLE_USER.REMARK is
'备注';

comment on column SYS_ROLE_USER.ISDEL is
'标志位';

comment on column SYS_ROLE_USER.CREATOR is
'创建人';

comment on column SYS_ROLE_USER.CREATETIME is
'创建时间';

comment on column SYS_ROLE_USER.MODIFIER is
'修改人';

comment on column SYS_ROLE_USER.MODIFYTIME is
'修改时间';

/*==============================================================*/
/* Table: SYS_SCHEDULE_JOB                                      */
/*==============================================================*/
create table SYS_SCHEDULE_JOB 
(
   ID                   NVARCHAR2(36)        not null,
   JOBNAME              NVARCHAR2(128)       not null,
   JOBDESCRIPTION       NVARCHAR2(512),
   NAMESPACE            NVARCHAR2(512)       not null,
   JOBIMPLEMENT         NVARCHAR2(128)       not null,
   CRONEXPRESSION       NVARCHAR2(128)       not null,
   STARTTIME            DATE,
   ENDTIME              DATE,
   JOBSTATUS            NUMBER(11)           not null,
   GROUPID              NUMBER(11),
   SERIALNUMBER         NUMBER(11),
   REMARK               NVARCHAR2(512),
   ISDEL                NUMBER(11)           not null,
   CREATOR              NVARCHAR2(36)        not null,
   CREATETIME           TIMESTAMP(6)         not null,
   MODIFIER             NVARCHAR2(36)        not null,
   MODIFYTIME           TIMESTAMP(6)         not null,
   constraint PK_SYS_SCHEDULE_JOB primary key (ID)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

comment on table SYS_SCHEDULE_JOB is
'计划任务表';

comment on column SYS_SCHEDULE_JOB.ID is
'主键';

comment on column SYS_SCHEDULE_JOB.JOBNAME is
'任务名称';

comment on column SYS_SCHEDULE_JOB.JOBDESCRIPTION is
'任务描述';

comment on column SYS_SCHEDULE_JOB.NAMESPACE is
'命名空间';

comment on column SYS_SCHEDULE_JOB.JOBIMPLEMENT is
'实现类';

comment on column SYS_SCHEDULE_JOB.CRONEXPRESSION is
'Cron表达式';

comment on column SYS_SCHEDULE_JOB.STARTTIME is
'开始时间';

comment on column SYS_SCHEDULE_JOB.ENDTIME is
'结束时间';

comment on column SYS_SCHEDULE_JOB.JOBSTATUS is
'任务状态，0：已启用，1：运行中，2：执行中，3：执行完成，4：任务计划中，5：已停止';

comment on column SYS_SCHEDULE_JOB.GROUPID is
'集群编号';

comment on column SYS_SCHEDULE_JOB.SERIALNUMBER is
'序号';

comment on column SYS_SCHEDULE_JOB.REMARK is
'备注';

comment on column SYS_SCHEDULE_JOB.ISDEL is
'标志位';

comment on column SYS_SCHEDULE_JOB.CREATOR is
'创建人';

comment on column SYS_SCHEDULE_JOB.CREATETIME is
'创建时间';

comment on column SYS_SCHEDULE_JOB.MODIFIER is
'修改人';

comment on column SYS_SCHEDULE_JOB.MODIFYTIME is
'修改时间';

/*==============================================================*/
/* Table: SYS_USER                                              */
/*==============================================================*/
create table SYS_USER 
(
   ID                   NVARCHAR2(36)        not null,
   LOGONNAME            NVARCHAR2(128)       not null,
   DISPLAYNAME          NVARCHAR2(128)       not null,
   PASSWORD             NVARCHAR2(512),
   TELEPHONE            NVARCHAR2(45),
   MOBILEPHONE          NVARCHAR2(45),
   EMAIL                NVARCHAR2(45),
   REMARK               NVARCHAR2(512),
   ISSUPPERADMIN        NUMBER(11),
   SERIALNUMBER         NUMBER(11),
   ISENABLED            NUMBER(11)           not null,
   ISDEL                NUMBER(11)           not null,
   CREATOR              NVARCHAR2(36)        not null,
   CREATETIME           TIMESTAMP(6)         not null,
   MODIFIER             NVARCHAR2(36)        not null,
   MODIFYTIME           TIMESTAMP(6)         not null,
   constraint PK_SYS_USER primary key (ID)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

comment on table SYS_USER is
'用户表';

comment on column SYS_USER.ID is
'主键';

comment on column SYS_USER.LOGONNAME is
'登录名称';

comment on column SYS_USER.DISPLAYNAME is
'用户姓名';

comment on column SYS_USER.PASSWORD is
'登陆密码';

comment on column SYS_USER.TELEPHONE is
'座机';

comment on column SYS_USER.MOBILEPHONE is
'手机';

comment on column SYS_USER.EMAIL is
'邮箱';

comment on column SYS_USER.REMARK is
'备注';

comment on column SYS_USER.ISSUPPERADMIN is
'是否是超级管理员';

comment on column SYS_USER.SERIALNUMBER is
'序号';

comment on column SYS_USER.ISENABLED is
'是否启用，0：禁用，1：启用';

comment on column SYS_USER.ISDEL is
'标志位';

comment on column SYS_USER.CREATOR is
'创建人';

comment on column SYS_USER.CREATETIME is
'创建时间';

comment on column SYS_USER.MODIFIER is
'修改人';

comment on column SYS_USER.MODIFYTIME is
'修改时间';

alter table BIZ_ACCOUNT
   add constraint FK_BIZ_ACCO_REFERENCE_BIZ_MODU foreign key (MODULEID)
      references BIZ_MODULE (ID);

alter table BIZ_USER_MODULE
   add constraint FK_BIZ_USER_REFERENCE_BIZ_MODU foreign key (MODULEID)
      references BIZ_MODULE (ID);

alter table BIZ_USER_MODULE
   add constraint FK_BIZ_USER_REFERENCE_BIZ_USER foreign key (USERID)
      references BIZ_USER (ID);

alter table SYS_IMPORT_CONFIG_DETAIL
   add constraint FK_SYS_IMPO_REFERENCE_SYS_IMPO foreign key (PARENTID)
      references SYS_IMPORT_CONFIG (ID);

alter table SYS_MENU_LANGUAGE
   add constraint FK_SYS_MENU_REFERENCE_SYS_LANG foreign key (LANGUAGEID)
      references SYS_LANGUAGE (ID);

alter table SYS_MENU_LANGUAGE
   add constraint FK_SYS_MENU_REFERENCE_SYS_MENU foreign key (MENUID)
      references SYS_MENU (ID);

alter table SYS_ORG_USER
   add constraint FK_SYS_ORG__REFERENCE_SYS_USER foreign key (USERID)
      references SYS_USER (ID);

alter table SYS_ORG_USER
   add constraint FK_SYS_ORG__REFERENCE_SYS_ORGA foreign key (ORGID)
      references SYS_ORGANIZATION (ID);

alter table SYS_ROLE_MENU
   add constraint FK_SYS_ROLE_REFERENCE_SYS_ROLE foreign key (ROLEID)
      references SYS_ROLE (ID);

alter table SYS_ROLE_MENU
   add constraint FK_SYS_ROLE_REFERENCE_SYS_MENU foreign key (MENUID)
      references SYS_MENU (ID);

alter table SYS_ROLE_ORG
   add constraint FK_SYS_ROLE_REFERENCE_SYS_ORGA foreign key (ORGID)
      references SYS_ORGANIZATION (ID);

alter table SYS_ROLE_ORG
   add constraint FK_SYS_ROLE_ORG_SYS_ROLE foreign key (ROLEID)
      references SYS_ROLE (ID);

alter table SYS_ROLE_USER
   add constraint FK_SYS_ROLE_USER_SYS_ROLE foreign key (ROLEID)
      references SYS_ROLE (ID);

alter table SYS_ROLE_USER
   add constraint FK_SYS_ROLE_REFERENCE_SYS_USER foreign key (USERID)
      references SYS_USER (ID);

INSERT INTO biz_module VALUES ('4a6798ec-0296-4516-9618-bd66cb77fa53','一莎Admin开源项目平台','一莎Admin','ys','http','127.0.0.1',8089,NULL,'/sso','/todo',2,'一莎Admin',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-07 10:26:44.670','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-07 10:38:17.460','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO biz_module VALUES ('81762d87-d4c5-483f-937f-4c0e77b534b5','若依Admin开源项目平台','若依Admin','ry','http','127.0.0.1',8086,NULL,'/sso','/todo',1,'若依Admin',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-07 10:22:22.530','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-07 10:32:02.692','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO biz_module VALUES ('97b36999-a9b0-4033-bd06-7f54a03a6fd7','若依Admin开源项目平台','若依Admin','ry','http','127.0.0.1',8086,NULL,'/sso','/todo',1,NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-23 15:30:14.726','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-23 15:32:19.412','yyyy-MM-dd HH24:MI:ss.ff'));

INSERT INTO biz_account VALUES ('bd64eee6-f595-4ebf-a369-3e9b59cdf422','81762d87-d4c5-483f-937f-4c0e77b534b5','XYZ','XYZ','oG1zbi0KVFun7XfTXxzlJw==',NULL,NULL,NULL,1,'!QAZ2wsx',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-03-15 09:34:04.296','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-03-18 10:20:51.540','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO biz_account VALUES ('e353c06a-78d3-4366-b951-1759894913af','4a6798ec-0296-4516-9618-bd66cb77fa53','ABC','ABC','BVm4iKPKu1/MPYhx853sxQ==',NULL,NULL,NULL,1,'qwe123',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-03-16 09:16:41.804','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-03-18 10:20:17.357','yyyy-MM-dd HH24:MI:ss.ff'));

INSERT INTO biz_user VALUES ('225d9edb-1817-4890-8f3a-9c8f78eed3fe','ry_lisi1','李四','kcAVKc+BvoWtRUcIwXZ72w==','010-55558889','13555556667','456@abc.com',1,NULL,0,'00000000-0000-0000-0000-000000000000', to_timestamp('2022-02-17 15:52:24.800','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000', to_timestamp('2022-02-17 15:52:24.800','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO biz_user VALUES ('441ca29e-0294-4ff5-92f4-df65a9996ee1','ry_zhangsan1','张三','kcAVKc+BvoWtRUcIwXZ72w==','010-55558888','13555556666','123@abc.com',1,NULL,0,'00000000-0000-0000-0000-000000000000', to_timestamp('2022-02-17 15:52:24.782','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000', to_timestamp('2022-02-17 15:52:24.782','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO biz_user VALUES ('540b304d-6b23-4b60-9a58-27f4e8c437fc','ys_test','伊莎测试','kcAVKc+BvoWtRUcIwXZ72w==',NULL,'13788889999',NULL,1,NULL,0,'00000000-0000-0000-0000-000000000000', to_timestamp('2022-02-09 16:29:28.351','yyyy-MM-dd HH24:MI:ss.ff'),'540b304d-6b23-4b60-9a58-27f4e8c437fc', to_timestamp('2022-03-17 12:21:32.174','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO biz_user VALUES ('79971bba-0c16-4df8-bd30-e732cda8ae58','ry_admin','若以测试账号','kcAVKc+BvoWtRUcIwXZ72w==',NULL,NULL,NULL,1,NULL,0,'00000000-0000-0000-0000-000000000000', to_timestamp('2022-02-08 16:13:24.863','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000', to_timestamp('2022-02-10 15:21:05.288','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO biz_user VALUES ('8061f56f-03e1-4df6-b2ce-aface3b29871','ry_李四','李四','kcAVKc+BvoWtRUcIwXZ72w==','010-55558889','13555556667','456@abc.com',1,NULL,0,'00000000-0000-0000-0000-000000000000', to_timestamp('2022-02-17 15:46:33.942','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000', to_timestamp('2022-02-18 09:39:56.943','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO biz_user VALUES ('a6e1a97b-772b-4a76-83a4-e55284c6daba','ry_zhangsan','张三','kcAVKc+BvoWtRUcIwXZ72w==','010-55558888','13555556666','123@abc.com',1,NULL,0,'00000000-0000-0000-0000-000000000000', to_timestamp('2022-02-17 15:46:33.922','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000', to_timestamp('2022-02-18 09:40:08.543','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO biz_user VALUES ('e4994527-16de-4a2e-8d07-74a5a1f1ed3e','ry_test','若以测试','kcAVKc+BvoWtRUcIwXZ72w==','010-99987777','13566666666','23@qq.com',1,NULL,0,'00000000-0000-0000-0000-000000000000', to_timestamp('2022-02-08 16:41:01.720','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000', to_timestamp('2022-02-09 16:28:27.240','yyyy-MM-dd HH24:MI:ss.ff'));

INSERT INTO biz_user_module VALUES ('2290effd-f427-45de-a0b6-06173e382f13','8061f56f-03e1-4df6-b2ce-aface3b29871','81762d87-d4c5-483f-937f-4c0e77b534b5','李四','qwertyuiop!@#$6',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-17 15:46:33.950','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 09:39:56.994','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO biz_user_module VALUES ('2daa327d-1de2-4fd9-b12b-f1e67aa56d0e','e4994527-16de-4a2e-8d07-74a5a1f1ed3e','81762d87-d4c5-483f-937f-4c0e77b534b5','test','test1234',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-08 16:41:01.750','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-09 16:28:27.256','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO biz_user_module VALUES ('31ddfd44-e67a-4620-906d-23524fee803f','540b304d-6b23-4b60-9a58-27f4e8c437fc','81762d87-d4c5-483f-937f-4c0e77b534b5','test','test',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 13:46:18.829','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 13:46:18.829','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO biz_user_module VALUES ('4ca173b0-1c73-46a6-9874-070ca7694ec0','225d9edb-1817-4890-8f3a-9c8f78eed3fe','4a6798ec-0296-4516-9618-bd66cb77fa53','lisi1','lisi1lisi1lisi1',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 15:25:25.166','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-22 09:24:36.947','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO biz_user_module VALUES ('5230f576-e558-4ef9-996a-dfcd91aca8cb','441ca29e-0294-4ff5-92f4-df65a9996ee1','81762d87-d4c5-483f-937f-4c0e77b534b5','zhangsan1','qwertyuiop!@#$5',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-17 15:52:24.793','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-17 15:52:24.793','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO biz_user_module VALUES ('9cf1a024-ba9e-4b05-99e0-4842e37fcf36','225d9edb-1817-4890-8f3a-9c8f78eed3fe','4a6798ec-0296-4516-9618-bd66cb77fa53','lisi1','lisi1lisi1lisi1lisi1',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 09:38:53.455','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 09:40:56.554','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO biz_user_module VALUES ('a19618d6-3ae5-44ec-82f4-27f354e28897','441ca29e-0294-4ff5-92f4-df65a9996ee1','4a6798ec-0296-4516-9618-bd66cb77fa53','qq','qq',NULL,0,'45aeff89-2867-43e7-9b40-296233c069c5',to_timestamp('2022-02-24 14:16:54.715','yyyy-MM-dd HH24:MI:ss.ff'),'45aeff89-2867-43e7-9b40-296233c069c5',to_timestamp('2022-02-24 14:16:54.715','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO biz_user_module VALUES ('ad430034-bc8f-4e87-aeb1-68c1ef780864','540b304d-6b23-4b60-9a58-27f4e8c437fc','4a6798ec-0296-4516-9618-bd66cb77fa53','test','test1234',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-09 16:29:28.365','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-09 16:29:28.365','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO biz_user_module VALUES ('bb643c2c-198e-41b4-944c-f31d0ee5d2af','a6e1a97b-772b-4a76-83a4-e55284c6daba','81762d87-d4c5-483f-937f-4c0e77b534b5','zhangsan','qwertyuiop!@#$5',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-17 15:46:33.935','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-22 09:24:50.603','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO biz_user_module VALUES ('d2245564-6e43-4aad-8dd7-aac2db983cca','225d9edb-1817-4890-8f3a-9c8f78eed3fe','81762d87-d4c5-483f-937f-4c0e77b534b5','lisi1','lisi1lisi1lisi1',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 15:25:55.445','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-22 09:24:32.101','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO biz_user_module VALUES ('da0b6394-e193-46df-89f7-5941e17740dc','225d9edb-1817-4890-8f3a-9c8f78eed3fe','81762d87-d4c5-483f-937f-4c0e77b534b5','lisi1','lisi1lisi1',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 15:22:15.450','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 15:24:59.801','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO biz_user_module VALUES ('eed681b9-aed5-4431-899d-de5b2043ac93','225d9edb-1817-4890-8f3a-9c8f78eed3fe','81762d87-d4c5-483f-937f-4c0e77b534b5','lisi1','qwertyuiop!@#$6',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-17 15:52:24.807','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 09:40:37.404','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO biz_user_module VALUES ('fdb6eaae-12f7-4fa6-991b-ae1727b44f7a','79971bba-0c16-4df8-bd30-e732cda8ae58','81762d87-d4c5-483f-937f-4c0e77b534b5','admin','kcAVKc+BvoWtRUcIwXZ72w==',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-08 16:13:24.875','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-10 15:21:05.332','yyyy-MM-dd HH24:MI:ss.ff'));

INSERT INTO sys_code_table VALUES ('221c849a-4943-458c-86f4-d301996228cc','第三性别','SexOther','2','5c6adfba-855d-436b-bb31-cdb2b8c77e66','第三性别',2,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 11:56:12.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 15:48:22.525','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_code_table VALUES ('342e0919-3116-466e-8665-042fb4543df7','是否判断','YesOrNo','',NULL,'是否判断',1,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 11:28:14.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-08 15:46:45.769','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_code_table VALUES ('3ea32b1c-a94c-4524-833d-2b21bf97feb6','纪委书记','JWSJ','JWSJ','a2a26c79-9483-4204-b209-d3b3c53b989b','纪委书记',1,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-20 13:57:50.671','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-20 13:57:50.671','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_code_table VALUES ('5ba22c2c-cd8c-4a43-a88a-a3a721756f4b','女','Female','1','5c6adfba-855d-436b-bb31-cdb2b8c77e66','女',1,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 11:55:45.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 11:59:05.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_code_table VALUES ('5c6adfba-855d-436b-bb31-cdb2b8c77e66','性别','Sex',NULL,NULL,'性别',2,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 11:28:38.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-01-21 15:28:17.680','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_code_table VALUES ('6e175e30-67a7-4d7d-9d67-fd9e4e7c47d7','经理','JL','JL','a2a26c79-9483-4204-b209-d3b3c53b989b','经理',3,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 14:43:59.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 14:43:59.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_code_table VALUES ('6f0e90ff-4b15-4d17-8939-9523c83b3e80','普通员工','YG','YG','a2a26c79-9483-4204-b209-d3b3c53b989b','普通员工',4,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 15:47:21.767','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:25:32.205','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_code_table VALUES ('7f6223be-740f-466d-9327-46405dfa9022','男','Male','0','5c6adfba-855d-436b-bb31-cdb2b8c77e66','男',0,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 11:55:25.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:30:01.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_code_table VALUES ('918a2974-6a77-444f-b5a6-039f01a77a88','董事长','DSZ','DSZ','a2a26c79-9483-4204-b209-d3b3c53b989b','董事长',0,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 14:42:53.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-20 13:55:05.157','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_code_table VALUES ('9847eaa8-12fc-48cd-84ce-8dca31a7b813','书记','SJ','SJ','a2a26c79-9483-4204-b209-d3b3c53b989b','书记',1,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-20 13:48:01.013','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-20 13:55:08.878','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_code_table VALUES ('a2a26c79-9483-4204-b209-d3b3c53b989b','岗位','Position',NULL,NULL,'岗位',3,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 14:29:07.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-06-11 14:12:17.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_code_table VALUES ('c4435185-6779-452f-85ae-14b1251a2920','总经理','ZJL','ZJL','a2a26c79-9483-4204-b209-d3b3c53b989b','总经理',2,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 14:43:20.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 14:44:09.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_code_table VALUES ('dc7a3882-fb25-4ba9-a30b-f9256f58e9c0','是','Yes','1','342e0919-3116-466e-8665-042fb4543df7','是',0,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 12:00:03.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 12:00:03.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_code_table VALUES ('dd12c0c2-768e-42da-b47f-8b386e591cae','否','No','0','342e0919-3116-466e-8665-042fb4543df7','否',1,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 12:00:48.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 12:00:48.000','yyyy-MM-dd HH24:MI:ss.ff'));

INSERT INTO sys_import_config VALUES ('21a45ec5-0758-4ad4-ad5a-31f9cf635f61','BizUserImportConfig',1,1,NULL,3,'业务用户导入配置',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-16 10:06:30.641','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-16 10:07:11.052','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_import_config VALUES ('51510173-0253-48a8-bdcb-4cb4827c5d08','RoleImportConfig',1,1,NULL,2,'角色导入配置',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-05-07 14:36:28.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-08 15:49:05.533','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_import_config VALUES ('ba6070b3-a815-4efb-ac1f-c804502544cf','CommonConfigTest',2,1,'',1,'综合常规性校验',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 16:11:34.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-01-21 15:28:29.480','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_import_config VALUES ('e28c1ee6-1c16-405f-ac6c-a83c5923ea32','UserImportConfig',1,1,NULL,4,'系统用户导入配置',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 10:48:36.916','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 10:48:36.916','yyyy-MM-dd HH24:MI:ss.ff'));

INSERT INTO sys_import_config_detail VALUES ('01ee9cc0-da0b-4635-8704-c0c42e1bcd23','21a45ec5-0758-4ad4-ad5a-31f9cf635f61',2,'5',NULL,NULL,NULL,NULL,NULL,'^(\\d{3,4}-)?\\d{6,8}$',NULL,NULL,NULL,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-16 11:07:43.514','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-17 16:53:41.252','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_import_config_detail VALUES ('14eaa1cd-effc-494f-b6dc-261964852cb9','e28c1ee6-1c16-405f-ac6c-a83c5923ea32',2,'1,2',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 10:49:27.678','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 10:49:27.678','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_import_config_detail VALUES ('211b1c6a-91ac-4c60-8061-183d939e8b94','ba6070b3-a815-4efb-ac1f-c804502544cf',0,'6',0,100,0,2,NULL,NULL,NULL,NULL,NULL,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-05-12 13:53:52.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-05-13 10:02:36.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_import_config_detail VALUES ('3cc6c7ea-1ee2-4885-ae1a-c9eb1ff12b89','51510173-0253-48a8-bdcb-4cb4827c5d08',2,'1',1,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-23 14:12:48.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-23 15:24:17.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_import_config_detail VALUES ('4dc94136-4f5c-42ef-baab-a73cd49160fc','ba6070b3-a815-4efb-ac1f-c804502544cf',4,'10',0,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-05-12 13:54:35.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-05-12 13:54:35.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_import_config_detail VALUES ('53d65856-21a4-40ec-98c1-6a85b5603e22','ba6070b3-a815-4efb-ac1f-c804502544cf',3,'4',1,33,1,0,NULL,NULL,NULL,NULL,NULL,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-05-05 09:08:18.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-05-12 13:53:15.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_import_config_detail VALUES ('59e91b35-be43-4fe1-baa7-9c602b93833a','21a45ec5-0758-4ad4-ad5a-31f9cf635f61',2,'4',NULL,NULL,NULL,NULL,NULL,'^1\\d{10}$',NULL,NULL,NULL,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-16 10:50:15.255','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 10:49:43.687','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_import_config_detail VALUES ('6a31efa0-12f1-480c-878c-1036966796d3','ba6070b3-a815-4efb-ac1f-c804502544cf',0,'8',1,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-05-05 09:08:48.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-05-05 09:11:20.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_import_config_detail VALUES ('721369f5-5f89-47e4-9b1e-dfd8a7795a41','ba6070b3-a815-4efb-ac1f-c804502544cf',0,'1',1,100,0,2,NULL,NULL,NULL,NULL,NULL,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-05-05 09:00:56.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-05-13 10:02:46.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_import_config_detail VALUES ('762ae51f-b973-49d2-8fcc-502aa28ec978','e28c1ee6-1c16-405f-ac6c-a83c5923ea32',2,'5',NULL,NULL,NULL,NULL,NULL,'^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$',NULL,NULL,NULL,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 10:51:17.644','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 10:51:17.644','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_import_config_detail VALUES ('881d73b4-394a-4e49-a985-0c6fa6a59a95','ba6070b3-a815-4efb-ac1f-c804502544cf',3,'9',0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-05-12 13:54:27.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-05-13 10:02:25.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_import_config_detail VALUES ('9c67a664-66c7-4d39-bda8-2f9b074cb0ab','ba6070b3-a815-4efb-ac1f-c804502544cf',1,'7',0,100,0,0,NULL,NULL,NULL,NULL,NULL,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-05-12 13:54:09.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-05-13 10:02:32.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_import_config_detail VALUES ('a0177782-ee2c-4017-ac7c-a09ab50e7b12','21a45ec5-0758-4ad4-ad5a-31f9cf635f61',2,'6',NULL,NULL,NULL,NULL,NULL,'^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$',NULL,NULL,NULL,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-16 11:09:40.739','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-17 16:57:26.192','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_import_config_detail VALUES ('abc36e6e-ae05-41b1-86a4-e09981e05578','ba6070b3-a815-4efb-ac1f-c804502544cf',2,'3',1,NULL,NULL,0,'男,女',NULL,NULL,NULL,NULL,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-05-05 09:06:36.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-24 09:28:17.209','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_import_config_detail VALUES ('bc69a513-2544-4bf9-9843-68685c35d994','ba6070b3-a815-4efb-ac1f-c804502544cf',2,'8',1,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-05-05 21:47:57.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-05-05 21:48:10.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_import_config_detail VALUES ('c107b3c5-4713-4f9f-8b0f-198d7ffe3a6f','51510173-0253-48a8-bdcb-4cb4827c5d08',1,'2',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-23 14:13:00.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-08 15:50:03.045','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_import_config_detail VALUES ('c351de82-9888-4e8c-82ca-16dfd9cfe721','ba6070b3-a815-4efb-ac1f-c804502544cf',1,'2',1,100,0,0,'',NULL,NULL,NULL,NULL,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-05-05 08:53:20.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-05-12 16:19:50.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_import_config_detail VALUES ('c4c349d5-8e82-4e2d-8d5f-36b2535de492','ba6070b3-a815-4efb-ac1f-c804502544cf',2,'8',0,0,0,0,'男,女',NULL,NULL,NULL,NULL,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-05-12 13:54:18.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-06-11 14:14:07.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_import_config_detail VALUES ('d405b74e-d52a-4f9e-b806-1c1d0bcaba40','e28c1ee6-1c16-405f-ac6c-a83c5923ea32',2,'4',NULL,NULL,NULL,NULL,NULL,'^(\\d{3,4}-)?\\d{6,8}$',NULL,NULL,NULL,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 10:50:44.888','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 10:50:44.888','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_import_config_detail VALUES ('e8a1a290-169a-4c9a-bed8-fffff884c1f0','e28c1ee6-1c16-405f-ac6c-a83c5923ea32',2,'3',NULL,NULL,NULL,NULL,NULL,'^1\\d{10}$',NULL,NULL,NULL,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 10:50:17.615','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 10:50:17.615','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_import_config_detail VALUES ('f14059ba-a95c-4b1f-881f-86eea84944f6','ba6070b3-a815-4efb-ac1f-c804502544cf',4,'5',1,90,1,0,NULL,NULL,NULL,NULL,NULL,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-05-05 09:08:35.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-05-12 13:53:27.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_import_config_detail VALUES ('f6e4400e-d695-406c-ae1d-853e59c8d75a','21a45ec5-0758-4ad4-ad5a-31f9cf635f61',2,'1,2,3',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-16 10:08:39.650','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-16 10:10:11.277','yyyy-MM-dd HH24:MI:ss.ff'));

INSERT INTO sys_language VALUES ('8223B5C4-C55A-4F02-98D2-BA7F320B0EA7','ru-RU',2,'俄国-俄国',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 11:56:12.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 11:56:12.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_language VALUES ('8306A39F-BFBD-430F-8315-15B6EBE3861D','en-US',1,'英国-美国',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 11:56:12.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 11:56:12.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_language VALUES ('9F06E722-5994-40F3-8BED-FE65C207E4D5','zh-CN',0,'华-中国',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 11:56:12.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 11:56:12.000','yyyy-MM-dd HH24:MI:ss.ff'));

INSERT INTO sys_menu VALUES ('00d356a5-f24b-4fe9-8616-aa8d36c37be8',NULL,'暂停任务',NULL,'328734a7-3aab-424d-8fb5-101a30335e3c',5,1,NULL,'schedule:job:pause',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-08-24 14:45:05.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-08-24 14:45:05.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('03237dc0-d26f-4611-8fe1-ba08ac1da7d8','/businessModule','离态用户管理','/BizUserNonModule','cc21d837-43ed-49c1-bc5f-9bb647c1fd14',3,0,'list',NULL,'离态用户管理',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 15:09:22.408','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 15:09:22.408','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('0745a4ca-fb04-48d0-8ac5-7114acd15deb',NULL,'编辑同步账号',NULL,'1bb376e1-c420-4bd4-ab39-75f569095ea4',3,1,NULL,'account:edit:entity',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-03-15 09:09:31.572','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-03-15 09:09:31.572','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('0d12b992-92df-4961-a50b-a2aac55da1e2',NULL,'查询列表',NULL,'9321de30-10c8-4709-aff3-bba47312693f',1,2,NULL,'log:query:list','查询列表',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:33:41.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:33:41.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('0d214bcc-a7be-41c2-9bb5-b43e6e1224aa',NULL,'查询WebAPI',NULL,'a796138d-4c0b-435c-bb5f-67f871870130',1,1,NULL,'query:webapi:info',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-31 17:14:35.470','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-31 17:14:35.470','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('0e989075-01cd-4c6e-a622-d336106bf255',NULL,'新增角色',NULL,'c80f26ab-b2bc-4766-a40e-17ed9417a5c7',2,1,NULL,'role:add:entity','新增角色',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:17:12.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:17:12.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('0f43194b-df86-4370-aeeb-26811cfab50c',NULL,'删除菜单',NULL,'cf7929b6-f9d9-4100-85f9-f63582583a61',4,1,NULL,'menu:del:entities','删除菜单',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:07:09.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:07:09.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('13e73c0d-a285-43af-9167-eef89381d08e',NULL,'新增字典',NULL,'a27a2518-53d8-4ed2-9655-3e1781d615dd',2,1,NULL,'code:add:entity','新增字典',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:25:32.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-18 20:26:46.285','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('1553af7d-5749-4894-8c3e-84cb3cf2f2dd',NULL,'删除业务用户',NULL,'ef3148eb-ebe6-4d9a-8806-aa783570290b',4,1,NULL,'user:module:del',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-07 16:24:16.373','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-16 10:03:38.275','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('18cd9b4c-bfbd-442b-86dc-11eee193d886',NULL,'分配权限',NULL,'c80f26ab-b2bc-4766-a40e-17ed9417a5c7',6,1,NULL,'role:grant:permission','分配权限',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 21:57:41.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-12 19:18:31.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('1bb376e1-c420-4bd4-ab39-75f569095ea4','/businessModule','同步账号管理','/BizAccount','cc21d837-43ed-49c1-bc5f-9bb647c1fd14',4,0,'list',NULL,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-03-15 09:07:20.130','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-03-15 09:07:20.130','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('206bb7dc-101a-4d76-89fc-606cdd247355',NULL,'查询用户列表',NULL,'03237dc0-d26f-4611-8fe1-ba08ac1da7d8',1,1,NULL,'user:nonmodule:list','查询用户列表',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 15:10:38.434','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 15:10:38.434','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('21c94e72-6dde-40c0-bae8-c0ef0a1c476c',NULL,'编辑配置',NULL,'63f294fb-eea5-41cf-8343-b43d60fdd680',3,1,NULL,'import:config:edit:entity',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:16:17.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:16:17.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('2634a816-5dd9-4bfd-a573-f6b0020204db',NULL,'编辑字典',NULL,'a27a2518-53d8-4ed2-9655-3e1781d615dd',3,1,NULL,'code:edit:entity','编辑字典',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:26:15.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-18 20:26:55.688','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('26d0ed04-5670-4707-ad91-e2893edca2ed',NULL,'删除机构',NULL,'b0652b6e-51ee-49ff-88b9-019bc8378f0b',4,1,NULL,'org:del:entities','删除机构',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 17:57:09.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-01-21 15:28:02.966','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('2923ffda-b4dc-4d92-9451-59a977a7edcb',NULL,'查询实体',NULL,'9321de30-10c8-4709-aff3-bba47312693f',2,1,NULL,'log:query:entity',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-13 16:41:09.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-13 16:41:13.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('300a8888-11af-4272-a638-baefc8e7858d',NULL,'删除同步账号',NULL,'1bb376e1-c420-4bd4-ab39-75f569095ea4',4,1,NULL,'account:del:entities',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-03-15 09:10:01.527','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-03-15 09:10:01.527','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('328734a7-3aab-424d-8fb5-101a30335e3c','/system','任务调度','/ScheduleJobManagement','aa0a8b78-5e35-46f5-be7e-2a55f20f6470',9,0,'form',NULL,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-08-24 14:41:51.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-08-24 14:46:32.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('371362df-0b05-4eee-9930-9288b6c8cc45',NULL,'删除用户',NULL,'e2363d77-45aa-4be1-b878-48d31aed728b',4,1,NULL,'user:del:entities','删除用户',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:02:15.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:02:15.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('45c8b9cf-5ac2-4e42-9cc8-051871a1d63f',NULL,'查询列表',NULL,'b0652b6e-51ee-49ff-88b9-019bc8378f0b',1,2,NULL,'org:query:list','查询列表',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 17:51:13.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-06-11 14:11:55.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('45e0f73e-4c77-4719-b76d-482de9a0afa6',NULL,'新增配置',NULL,'63f294fb-eea5-41cf-8343-b43d60fdd680',2,1,NULL,'import:config:add:entity',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:15:50.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:15:50.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('4c4126b1-8c20-44a5-a2cf-9ea19e582af9',NULL,'数据导出',NULL,'c80f26ab-b2bc-4766-a40e-17ed9417a5c7',8,1,NULL,'role:list:export',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-28 18:04:02.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-28 18:04:02.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('4ef0101a-9911-47d0-8555-841ce928903f',NULL,'编辑用户',NULL,'e2363d77-45aa-4be1-b878-48d31aed728b',3,1,NULL,'user:edit:entity','编辑用户',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:01:32.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:01:32.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('5be9a12e-1c91-40bb-be8e-3f38da256c19',NULL,'编辑菜单',NULL,'cf7929b6-f9d9-4100-85f9-f63582583a61',3,1,NULL,'menu:edit:entity','编辑菜单',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:06:33.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:06:33.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('60f51a50-7fb8-47df-bbee-20a4bb29d650',NULL,'编辑角色',NULL,'c80f26ab-b2bc-4766-a40e-17ed9417a5c7',3,1,NULL,'role:edit:entity','编辑角色',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:17:47.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:17:47.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('63f294fb-eea5-41cf-8343-b43d60fdd680','/system','导入配置','/ImportConfigManagement','aa0a8b78-5e35-46f5-be7e-2a55f20f6470',7,0,'form',NULL,'导入配置',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:10:12.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 16:51:36.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('66469b10-a974-4abf-bd72-b7a205a09b47',NULL,'删除模块',NULL,'a81bb7aa-d7c2-41b9-b616-b46d3d7d2cf3',4,1,NULL,'module:del:entity','删除模块',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-01-26 15:16:40.206','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-01-26 16:49:43.518','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('66d87039-4811-4ba8-87b4-e53ed0daf21f',NULL,'查询同步账号',NULL,'1bb376e1-c420-4bd4-ab39-75f569095ea4',1,1,NULL,'account:query:list',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-03-15 09:08:26.201','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-03-15 09:11:04.156','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('78c1335e-f05a-4431-a85b-cee182b446f3',NULL,'新增业务用户',NULL,'ef3148eb-ebe6-4d9a-8806-aa783570290b',2,1,NULL,'user:module:add',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-07 16:22:56.673','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-16 10:03:20.885','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('7ffd51d8-3475-4b82-a0e7-bd5e92e5f174',NULL,'新增同步账号',NULL,'1bb376e1-c420-4bd4-ab39-75f569095ea4',2,1,NULL,'account:add:entity',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-03-15 09:08:56.161','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-03-15 09:08:56.161','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('8169ab55-b24e-45b5-99f1-9d65d55af215',NULL,'查询列表',NULL,'a81bb7aa-d7c2-41b9-b616-b46d3d7d2cf3',1,1,NULL,'biz:module:list','查询列表',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-01-26 15:14:32.994','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-01-26 16:49:51.846','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('837a330f-11bd-4a70-93b5-25af2c905408',NULL,'关联菜单',NULL,'c80f26ab-b2bc-4766-a40e-17ed9417a5c7',5,1,NULL,'role:relate:menus','关联菜单',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 21:59:03.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 21:59:03.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('8d549066-fe5d-466c-b540-3525ebf30def',NULL,'新增机构',NULL,'b0652b6e-51ee-49ff-88b9-019bc8378f0b',2,1,NULL,'org:add:entity','新增机构',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 17:55:41.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-18 18:12:54.409','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('8ef1c60e-d626-4a01-9bb2-8e609c0b40fc',NULL,'启动任务',NULL,'328734a7-3aab-424d-8fb5-101a30335e3c',4,1,NULL,'schedule:job:add',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-08-24 14:44:40.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-08-24 14:44:40.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('8ff6bcd2-8701-4f45-aea2-bbc78718c4fb',NULL,'恢复任务',NULL,'328734a7-3aab-424d-8fb5-101a30335e3c',6,1,NULL,'schedule:job:resume',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-08-24 14:45:34.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-08-24 14:53:34.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('9321de30-10c8-4709-aff3-bba47312693f','/system','审计日志','/LogManagement','aa0a8b78-5e35-46f5-be7e-2a55f20f6470',6,0,'documentation',NULL,'审计日志管理菜单',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 16:12:40.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:17:23.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('9e02c48e-3b3f-4f24-8005-b13c63d437c0','/system','机构管理',NULL,'aa0a8b78-5e35-46f5-be7e-2a55f20f6470',20,0,NULL,NULL,NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-01 16:53:04.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-01 17:12:00.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('a27a2518-53d8-4ed2-9655-3e1781d615dd','/system','数据字典','/CodeTableManagement','aa0a8b78-5e35-46f5-be7e-2a55f20f6470',5,0,'table',NULL,'数据字典管理菜单',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 16:11:47.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:17:19.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('a4416f30-7381-48d2-a074-b2e9a9481c3c',NULL,'数据导入',NULL,'c80f26ab-b2bc-4766-a40e-17ed9417a5c7',9,1,NULL,'role:list:import',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-05-07 10:29:59.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-05-07 10:29:59.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('a4a59f63-f397-4850-ac4a-b06b41acfc34',NULL,'权限下放',NULL,'c80f26ab-b2bc-4766-a40e-17ed9417a5c7',7,1,NULL,'role:delegate:permission','权限下放',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-11-11 14:27:50.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-11-11 14:27:50.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('a63b6939-563f-41a2-b526-525560069e3b',NULL,'查询列表',NULL,'328734a7-3aab-424d-8fb5-101a30335e3c',0,1,NULL,'job:query:list',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-08-24 14:42:36.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-08-24 14:42:36.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('a796138d-4c0b-435c-bb5f-67f871870130','/system','系统WebAPI','/SystemWebAPI','aa0a8b78-5e35-46f5-be7e-2a55f20f6470',12,0,'form',NULL,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-31 17:02:55.051','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-31 17:08:54.181','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('a81bb7aa-d7c2-41b9-b616-b46d3d7d2cf3','/businessModule','模块管理','/BizModule','cc21d837-43ed-49c1-bc5f-9bb647c1fd14',1,0,'list',NULL,'模块管理',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-01-26 15:12:57.158','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-01-26 17:29:54.882','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('aa0a8b78-5e35-46f5-be7e-2a55f20f6470','/system','系统管理','',NULL,1,0,'tree',NULL,'系统管理目录',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 15:44:23.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-06-11 14:12:01.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('af62fa5d-4808-4a59-9297-58b0df442fff',NULL,'新增模块',NULL,'a81bb7aa-d7c2-41b9-b616-b46d3d7d2cf3',2,1,NULL,'module:add:entity','新增模块',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-01-26 15:15:13.683','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-01-26 16:49:55.245','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('b0652b6e-51ee-49ff-88b9-019bc8378f0b','/system','机构管理','/OrganizationManagement','aa0a8b78-5e35-46f5-be7e-2a55f20f6470',1,0,'tree-table','','机构管理',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 16:05:25.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-01 16:51:56.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('b25958ea-608d-436f-8e01-b8fcc101905d','/system','多语管理','/LanguageManagement','aa0a8b78-5e35-46f5-be7e-2a55f20f6470',10,0,'form',NULL,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-08-05 11:20:24.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-08-24 14:41:20.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('b3b8f210-c86e-422c-8434-6d9e13bcbaba',NULL,'查询信息',NULL,'f34914ab-50f3-4acf-b844-9cfc70834896',1,1,NULL,'query:hardware:info',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-10 16:38:41.703','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-10 16:40:22.611','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('b807f782-8b8c-49b6-a62d-e230dd44cd7e',NULL,'删除角色',NULL,'c80f26ab-b2bc-4766-a40e-17ed9417a5c7',4,1,NULL,'role:del:entities','删除角色',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:18:23.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:18:23.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('be4b8341-f1a7-473b-bad7-5b4b41732e49','/system','在线管理','/OnlineUserManagement','aa0a8b78-5e35-46f5-be7e-2a55f20f6470',8,0,'form',NULL,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-06-09 15:16:25.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-06-09 15:18:22.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('bec177ff-23ce-449f-ade4-dd0e62142842',NULL,'编辑机构',NULL,'b0652b6e-51ee-49ff-88b9-019bc8378f0b',3,1,NULL,'org:edit:entity','编辑机构',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 17:56:36.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 17:56:36.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('c13577ae-a6aa-41eb-b1f2-62af176891c4',NULL,'编辑任务',NULL,'328734a7-3aab-424d-8fb5-101a30335e3c',2,1,NULL,'job:edit:entity',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-08-24 14:43:48.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-08-24 14:43:48.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('c1dc4140-ef7f-491e-a559-52614b581930',NULL,'编辑业务用户',NULL,'ef3148eb-ebe6-4d9a-8806-aa783570290b',3,1,NULL,'user:module:edit',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-07 16:23:41.207','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-16 10:03:28.321','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('c277f700-ac70-40f4-a290-28eddefac820',NULL,'查询列表',NULL,'63f294fb-eea5-41cf-8343-b43d60fdd680',1,1,NULL,'import:config:query:list',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:15:21.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:15:21.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('c27d9de3-78ae-4577-8bdd-e4eea63010af',NULL,'查询列表',NULL,'cf7929b6-f9d9-4100-85f9-f63582583a61',1,1,NULL,'menu:query:list','查询列表',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:05:09.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:05:09.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('c3be9eb3-d1da-4af7-8820-aeaf0283cdec',NULL,'查询列表',NULL,'b25958ea-608d-436f-8e01-b8fcc101905d',1,1,NULL,'language:query:list','',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-08-05 11:21:36.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:28:13.546','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('c80f26ab-b2bc-4766-a40e-17ed9417a5c7','/system','角色管理','/RoleManagement','aa0a8b78-5e35-46f5-be7e-2a55f20f6470',4,0,'theme',NULL,'角色管理菜单',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 16:10:47.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:24:48.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('cc21d837-43ed-49c1-bc5f-9bb647c1fd14','/businessModule','统一认证','',NULL,2,0,'table',NULL,'统一认证目录',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-01-26 15:07:54.148','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-01-26 15:08:10.363','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('ccb42490-1389-40a2-86a0-ff77553bc740',NULL,'删除任务',NULL,'328734a7-3aab-424d-8fb5-101a30335e3c',3,1,NULL,'job:del:entity',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-08-24 14:44:12.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-08-24 14:44:12.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('cf7929b6-f9d9-4100-85f9-f63582583a61','/system','菜单管理','/MenuManagement','aa0a8b78-5e35-46f5-be7e-2a55f20f6470',3,0,'list',NULL,'菜单管理',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 16:10:01.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 16:10:01.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('d0ebd39e-9a82-42fa-986e-c7235747a758',NULL,'查询列表',NULL,'c80f26ab-b2bc-4766-a40e-17ed9417a5c7',1,2,NULL,'role:query:list','查询列表',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:16:44.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:16:44.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('d1bf1584-3c7a-4642-8cc6-095c42682e15',NULL,'查询列表',NULL,'ef3148eb-ebe6-4d9a-8806-aa783570290b',1,1,NULL,'user:module:list',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-01-26 17:33:36.375','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-16 10:03:10.673','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('d25ea44a-4aba-47eb-b9ba-a42799d7a535',NULL,'新增菜单',NULL,'cf7929b6-f9d9-4100-85f9-f63582583a61',2,1,NULL,'menu:add:entity','新增菜单',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:04:32.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:04:32.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('de435288-c697-4072-8215-394dc7f80ca6',NULL,'查询列表',NULL,'e2363d77-45aa-4be1-b878-48d31aed728b',1,2,NULL,'user:query:list','查询列表',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:00:41.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:00:41.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('e2363d77-45aa-4be1-b878-48d31aed728b','/system','用户管理','/UserManagement','aa0a8b78-5e35-46f5-be7e-2a55f20f6470',2,0,'user',NULL,'用户管理菜单',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 16:05:01.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-01 17:13:02.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('e2aaab2a-1b25-4e45-b54f-160bc6d332df',NULL,'新增用户',NULL,'e2363d77-45aa-4be1-b878-48d31aed728b',2,1,NULL,'user:add:entity','新增用户',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:01:08.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:01:08.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('e2bd72c3-b926-4072-b7a9-429cb77fe351',NULL,'模块访问授权',NULL,'ef3148eb-ebe6-4d9a-8806-aa783570290b',6,1,NULL,'user:module:grant',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-16 10:04:44.109','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-16 10:04:44.109','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('eae2b1cf-26e5-4086-ae44-7f7f3c8a50d4',NULL,'删除配置',NULL,'63f294fb-eea5-41cf-8343-b43d60fdd680',4,1,NULL,'import:config:del:entities',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:16:51.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:16:51.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('eb926dd8-63ba-49a4-8445-df2850f763c9',NULL,'新增任务',NULL,'328734a7-3aab-424d-8fb5-101a30335e3c',1,1,NULL,'job:add:entity',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-08-24 14:43:18.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-08-24 14:43:18.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('ed4354fd-9611-41a9-abdc-383624db7a01',NULL,'查询列表',NULL,'be4b8341-f1a7-473b-bad7-5b4b41732e49',1,1,NULL,'user:query:onlineUsers',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-06-09 15:17:08.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-06-09 15:17:08.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('eed891c4-822d-41d4-ac80-c6b9c327a1af',NULL,'离态用户授权',NULL,'03237dc0-d26f-4611-8fe1-ba08ac1da7d8',2,1,NULL,'user:nonmodule:grant','离态用户授权',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 15:11:32.160','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 15:11:32.160','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('ef3148eb-ebe6-4d9a-8806-aa783570290b','/businessModule','授权管理','/BizUserModule','cc21d837-43ed-49c1-bc5f-9bb647c1fd14',2,0,'list',NULL,'授权管理',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-01-26 17:24:20.373','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-01-26 17:35:51.649','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('f21f1b48-b743-4e01-a5d7-c50ebfd2fe78',NULL,'删除字典',NULL,'a27a2518-53d8-4ed2-9655-3e1781d615dd',4,1,NULL,'code:del:entities','删除字典',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:28:26.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-18 21:39:38.690','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('f34914ab-50f3-4acf-b844-9cfc70834896','/system','服务器监控','/ServerHardwareMonitor','aa0a8b78-5e35-46f5-be7e-2a55f20f6470',11,0,'form',NULL,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-10 16:37:41.453','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-10 16:39:23.402','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('f425487a-72a1-469d-8fa1-2334db69aba4',NULL,'查询列表',NULL,'a27a2518-53d8-4ed2-9655-3e1781d615dd',1,2,NULL,'code:query:list','查询列表',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:24:37.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 18:24:37.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('f6d5b324-ed69-4246-82cd-cb9b3f80efa6',NULL,'导入用户',NULL,'e2363d77-45aa-4be1-b878-48d31aed728b',5,1,NULL,'user:import:entities',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 10:46:50.043','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 10:46:50.043','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('fb1f9114-e18c-4159-8af1-1408651f21fc',NULL,'编辑模块',NULL,'a81bb7aa-d7c2-41b9-b616-b46d3d7d2cf3',3,1,NULL,'module:edit:entity','编辑模块',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-01-26 15:15:55.382','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-01-26 16:49:48.904','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu VALUES ('fb35235e-b771-462e-b84b-269f3d8938ce',NULL,'导入业务用户',NULL,'ef3148eb-ebe6-4d9a-8806-aa783570290b',5,1,NULL,'user:module:import',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-16 10:04:02.319','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-16 10:04:02.319','yyyy-MM-dd HH24:MI:ss.ff'));

INSERT INTO sys_menu_language VALUES ('0bf54370-bd65-4a1c-8b6f-01fde552d168','b0652b6e-51ee-49ff-88b9-019bc8378f0b','8223B5C4-C55A-4F02-98D2-BA7F320B0EA7','OrgManage',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-01 16:51:56.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-01 16:51:56.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu_language VALUES  ('19b2fc8e-b374-43cc-8812-a056f2e87226','e2363d77-45aa-4be1-b878-48d31aed728b','8223B5C4-C55A-4F02-98D2-BA7F320B0EA7','UserManage',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-01 17:13:02.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-01 17:13:02.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu_language VALUES  ('319fd164-d116-441e-bfae-0f24da6db973','9e02c48e-3b3f-4f24-8005-b13c63d437c0','8306A39F-BFBD-430F-8315-15B6EBE3861D','abc',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-01 16:53:09.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-01 17:12:00.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu_language VALUES  ('3f4da931-b98c-4ad9-ace9-5e7fe157cd1f','9e02c48e-3b3f-4f24-8005-b13c63d437c0','8223B5C4-C55A-4F02-98D2-BA7F320B0EA7','ru',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-01 16:53:11.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-01 17:12:00.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu_language VALUES  ('5fb0329f-ff3b-47d6-9075-64a389901aa1','a81bb7aa-d7c2-41b9-b616-b46d3d7d2cf3','8306A39F-BFBD-430F-8315-15B6EBE3861D','BizModuleManagement',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-01-26 15:12:57.172','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-01-26 16:49:58.132','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu_language VALUES  ('6048abf8-ca30-45f4-90d8-7831eedb595e','e2363d77-45aa-4be1-b878-48d31aed728b','8306A39F-BFBD-430F-8315-15B6EBE3861D','UserManagement',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-01 17:13:02.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-01 17:13:02.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu_language VALUES  ('7ee805a9-e045-429c-825f-990426c75a75','b0652b6e-51ee-49ff-88b9-019bc8378f0b','8223B5C4-C55A-4F02-98D2-BA7F320B0EA7','OrgManage',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-01 16:51:35.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-01 16:51:56.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu_language VALUES  ('a08245cd-d3f0-41ba-a5a3-ed95853a4aa6','b0652b6e-51ee-49ff-88b9-019bc8378f0b','8306A39F-BFBD-430F-8315-15B6EBE3861D','OrganizationManagement',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-01 16:51:13.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-01 16:51:34.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu_language VALUES  ('aabbc321-8f26-423f-89ed-46f5e8ed4ae0','b0652b6e-51ee-49ff-88b9-019bc8378f0b','8223B5C4-C55A-4F02-98D2-BA7F320B0EA7','OrgManage',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-01 16:51:28.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-01 16:51:35.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu_language VALUES  ('ea4c0742-28ac-48c2-a7f5-31f0508c9e4b','b0652b6e-51ee-49ff-88b9-019bc8378f0b','8306A39F-BFBD-430F-8315-15B6EBE3861D','OrganizationManagement',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-01 16:51:56.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-01 16:51:56.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu_language VALUES  ('ed30a015-6ceb-4592-aca5-00dd95774aa7','b0652b6e-51ee-49ff-88b9-019bc8378f0b','8306A39F-BFBD-430F-8315-15B6EBE3861D','OrganizationManagement',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-01 16:51:34.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-01 16:51:56.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_menu_language VALUES  ('eee77f5f-b4bb-44fb-92d8-a493b143e6f0','a81bb7aa-d7c2-41b9-b616-b46d3d7d2cf3','8223B5C4-C55A-4F02-98D2-BA7F320B0EA7','BizModuleManagement',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-01-26 15:12:57.184','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-01-26 16:49:58.137','yyyy-MM-dd HH24:MI:ss.ff'));

INSERT INTO sys_organization VALUES ('39b86e17-1b98-4898-a1c0-8faef7f66788','DBA','5a948c7d-56ea-477d-850e-1386f1dcdc2c',NULL,6,'',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-08 10:00:45.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 16:48:08.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_organization VALUES  ('504b3320-1b29-4493-98fd-e7d15f338569','UI设计','5a948c7d-56ea-477d-850e-1386f1dcdc2c',NULL,5,'',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-08 09:44:48.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-08 13:18:16.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_organization VALUES  ('57126a11-842e-493d-bc4f-e8c18853e6ed','硬件开发','5a948c7d-56ea-477d-850e-1386f1dcdc2c',NULL,2,'二级组织',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-08 09:29:54.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-06-22 15:14:12.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_organization VALUES  ('5a948c7d-56ea-477d-850e-1386f1dcdc2c','研发部','ab907b1e-13a3-4c04-85db-f560f9c8f9aa',NULL,1,'一级部门',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-08 09:14:41.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-03-11 14:31:57.021','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_organization VALUES  ('66255038-acda-49a4-b260-ccafeedc6074','实施部','ab907b1e-13a3-4c04-85db-f560f9c8f9aa',NULL,2,'一级部门',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-08 09:27:35.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-08 09:27:48.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_organization VALUES  ('7955d133-040a-4e36-94fe-89c9c4a547e5','售后部','ab907b1e-13a3-4c04-85db-f560f9c8f9aa',NULL,3,'一级部门',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-08 09:28:46.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-08 09:28:46.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_organization VALUES  ('7d664253-e505-4157-83b0-01bb86028d66','技术支持','5a948c7d-56ea-477d-850e-1386f1dcdc2c',NULL,7,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 10:59:25.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 16:47:07.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_organization VALUES  ('8faffd3a-3bd2-4987-9cad-f658e83dc283','系统测试','5a948c7d-56ea-477d-850e-1386f1dcdc2c',NULL,3,'系统测试',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-08 09:31:17.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-08 15:30:36.103','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_organization VALUES  ('9756e90f-9505-4990-ac0e-135c58bc6f29','软件开发','5a948c7d-56ea-477d-850e-1386f1dcdc2c','b09bbb4b-a7f0-4f7a-8f3d-dbb895f3edc5',1,'二级组织',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-08 09:29:38.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-01-21 15:27:49.576','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_organization VALUES  ('99b57766-c9f3-40ec-ac45-9d2cbe79ee08','需求分析','5a948c7d-56ea-477d-850e-1386f1dcdc2c',NULL,4,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-08 10:00:13.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-06-11 14:11:21.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_organization VALUES  ('ab907b1e-13a3-4c04-85db-f560f9c8f9aa','如意Admin',NULL,NULL,1,'根本目录',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-03-14 12:38:56.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 16:51:12.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_organization VALUES  ('e4bcf807-6bd2-4aa5-ba6f-8ef85840fd65','营销部','ab907b1e-13a3-4c04-85db-f560f9c8f9aa',NULL,3,'一级部门',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-08 09:28:25.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-08 09:28:25.000','yyyy-MM-dd HH24:MI:ss.ff'));

INSERT INTO sys_user VALUES ('00000000-0000-0000-0000-000000000000','admin','超级管理员','kcAVKc+BvoWtRUcIwXZ72w==',NULL,NULL,NULL,NULL,1,0,1,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-09 11:46:43.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-09 11:46:43.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_user VALUES  ('0300ae60-86ac-4c4a-a701-c53d8eb00ca6','zhangsan122','张三','kcAVKc+BvoWtRUcIwXZ72w==','010-55558888','13555556666','123@abc.com',NULL,0,0,1,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 12:08:56.732','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-22 09:06:36.970','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_user VALUES  ('1b8f558f-4a5b-43c6-bc14-4b0c1e83587f','xx','xx','kcAVKc+BvoWtRUcIwXZ72w==',NULL,NULL,NULL,NULL,0,0,1,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 11:01:29.377','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-22 09:06:44.497','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_user VALUES  ('24b7ea06-3519-4fd1-9cdc-a7bdbf17f4fd','lisi12','李四','kcAVKc+BvoWtRUcIwXZ72w==','010-55558889','13555556667','456@abc.com',NULL,0,0,1,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 12:04:41.769','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-22 09:06:44.534','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_user VALUES  ('2e24e235-cdb0-464a-9d97-c1d0e989819c','lisi','李四','kcAVKc+BvoWtRUcIwXZ72w==','010-55558889','13555556667','456@abc.com',NULL,0,0,1,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 11:00:20.199','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-22 09:06:44.568','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_user VALUES  ('31cf3eed-848a-40a4-9c9d-75b2bac01d90','zhangsan12','张三','kcAVKc+BvoWtRUcIwXZ72w==','010-55558888','13555556666','123@abc.com',NULL,0,0,1,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 12:04:41.615','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 12:04:41.615','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_user VALUES  ('44d83899-e4d8-4909-9eb7-969aa69a1ca8','lisi122','李四','kcAVKc+BvoWtRUcIwXZ72w==','010-55558889','13555556667','456@abc.com',NULL,0,0,1,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 12:08:56.839','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 12:08:56.839','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_user VALUES  ('45aeff89-2867-43e7-9b40-296233c069c5','ruyi','如意','kcAVKc+BvoWtRUcIwXZ72w==','010-55558888','13555556666','ruyi@qq.com','如意小朋友',0,1,1,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-09 11:46:43.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-21 09:50:57.905','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_user VALUES  ('6585aa62-9ac1-4799-863a-317e26da466a','xz','xz','kcAVKc+BvoWtRUcIwXZ72w==',NULL,NULL,NULL,'xz',0,2,1,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-21 09:53:11.771','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-22 09:05:43.069','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_user VALUES  ('6f415677-cc02-4efa-bfbd-fa7c1c2c561d','hello','你好','kcAVKc+BvoWtRUcIwXZ72w==',NULL,NULL,NULL,'xx',0,1,1,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-06-11 14:01:38.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-07-02 16:04:25.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_user VALUES  ('92329bbc-a81d-4da9-b286-1a8a6444456b','zhangsan','张三','kcAVKc+BvoWtRUcIwXZ72w==','010-55558888','13555556666','123@abc.com',NULL,0,0,1,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 10:59:09.629','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 10:59:09.629','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_user VALUES  ('930cc8e2-f1ae-404d-9258-6167a3acbcbc','shisih','实施','kcAVKc+BvoWtRUcIwXZ72w==',NULL,NULL,NULL,'pp',0,1,1,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 12:14:58.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-06-11 13:58:56.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_user VALUES  ('a668aba7-b317-4468-a0fe-a2e024f0d303','lisi1','李四','kcAVKc+BvoWtRUcIwXZ72w==','010-55558889','13555556667','456@abc.com',NULL,0,0,1,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 11:08:13.468','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 11:08:13.468','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_user VALUES  ('b09bbb4b-a7f0-4f7a-8f3d-dbb895f3edc5','test','测试账号','kcAVKc+BvoWtRUcIwXZ72w==',NULL,NULL,NULL,'xy',0,1,1,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 12:14:37.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-08 15:35:49.900','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_user VALUES  ('b464f199-3369-4dbd-ae7e-497224d5ea1d','zhangsan1','张三','kcAVKc+BvoWtRUcIwXZ72w==','010-55558888','13555556666','123@abc.com',NULL,0,0,1,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 11:08:04.487','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 11:08:04.487','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_user VALUES  ('bacc045e-eefc-4d45-9920-82192cbabb4e','8879','8879','kcAVKc+BvoWtRUcIwXZ72w==',NULL,NULL,NULL,NULL,0,0,1,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 12:03:33.714','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 12:03:33.714','yyyy-MM-dd HH24:MI:ss.ff'));

INSERT INTO sys_org_user VALUES ('02c0da63-02d3-4000-874c-da41deb37ab7','9756e90f-9505-4990-ac0e-135c58bc6f29','a668aba7-b317-4468-a0fe-a2e024f0d303',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 11:08:13.478','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 11:08:13.478','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_org_user VALUES  ('0cd31059-71a1-400d-b91a-d3dc0e79a2e8','9756e90f-9505-4990-ac0e-135c58bc6f29','1b8f558f-4a5b-43c6-bc14-4b0c1e83587f',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 11:01:29.389','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-22 09:06:44.482','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_org_user VALUES  ('0ce71313-2f99-4529-aa3c-bf08b3c613e7','ab907b1e-13a3-4c04-85db-f560f9c8f9aa','45aeff89-2867-43e7-9b40-296233c069c5',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-09 11:46:45.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-09 11:46:45.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_org_user VALUES  ('258511fd-281e-4bb8-baee-71721e278651','9756e90f-9505-4990-ac0e-135c58bc6f29','b464f199-3369-4dbd-ae7e-497224d5ea1d',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 11:08:04.504','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 11:08:04.504','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_org_user VALUES  ('2c9f5064-a72b-4705-9abe-292dd4a00dca','9756e90f-9505-4990-ac0e-135c58bc6f29','24b7ea06-3519-4fd1-9cdc-a7bdbf17f4fd',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 12:04:41.779','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-22 09:06:44.518','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_org_user VALUES  ('4bd496ec-a96c-4de0-a7e5-0e32e4084be7','9756e90f-9505-4990-ac0e-135c58bc6f29','2e24e235-cdb0-464a-9d97-c1d0e989819c',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 11:00:20.209','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-22 09:06:44.553','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_org_user VALUES  ('5ccc079d-b545-453b-b8c7-dbd64dc5d6c0','9756e90f-9505-4990-ac0e-135c58bc6f29','bacc045e-eefc-4d45-9920-82192cbabb4e',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 12:03:33.728','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 12:03:33.728','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_org_user VALUES  ('67ee7a91-40e5-439c-b102-41864076ecae','9756e90f-9505-4990-ac0e-135c58bc6f29','44d83899-e4d8-4909-9eb7-969aa69a1ca8',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 12:08:56.847','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 12:08:56.847','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_org_user VALUES  ('727a3688-20d8-4186-b9d4-c76288d65f28','9756e90f-9505-4990-ac0e-135c58bc6f29','31cf3eed-848a-40a4-9c9d-75b2bac01d90',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 12:04:41.627','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 12:04:41.627','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_org_user VALUES  ('7599136a-e1aa-49c2-8ae8-7689e79d9857','57126a11-842e-493d-bc4f-e8c18853e6ed','6f415677-cc02-4efa-bfbd-fa7c1c2c561d',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-06-11 14:01:38.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-06-11 14:01:38.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_org_user VALUES  ('a863f9ee-75d2-43ae-ab82-c0705bca87cc','9756e90f-9505-4990-ac0e-135c58bc6f29','92329bbc-a81d-4da9-b286-1a8a6444456b',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 10:59:11.955','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 10:59:11.955','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_org_user VALUES  ('abd730cf-e42f-451e-8b19-3ff60f4c1926','9756e90f-9505-4990-ac0e-135c58bc6f29','0300ae60-86ac-4c4a-a701-c53d8eb00ca6',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-18 12:08:56.746','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-22 09:06:36.952','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_org_user VALUES  ('c5390b02-69c5-4829-9d07-f1dc15379aab','9756e90f-9505-4990-ac0e-135c58bc6f29','b09bbb4b-a7f0-4f7a-8f3d-dbb895f3edc5',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 12:14:37.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 16:49:09.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_org_user VALUES  ('ced3716f-38ce-44d3-88ea-75817e3c39be','ab907b1e-13a3-4c04-85db-f560f9c8f9aa','6585aa62-9ac1-4799-863a-317e26da466a',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-21 09:53:11.783','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-22 09:05:25.063','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_org_user VALUES  ('d6774ab4-5cec-46f5-912e-9a6cb3ae269e','66255038-acda-49a4-b260-ccafeedc6074','930cc8e2-f1ae-404d-9258-6167a3acbcbc',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 12:14:58.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 16:49:09.000','yyyy-MM-dd HH24:MI:ss.ff'));

INSERT INTO sys_role VALUES ('00793b23-7ed3-4e30-9906-0927f7f86543','Test1',21,'Test1',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-26 14:35:06.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-26 14:35:06.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role VALUES  ('07ceaf4b-834b-4103-9085-2d302c5bb0f0','Test2',20,'Test2',1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-23 15:40:32.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-26 14:35:01.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role VALUES  ('12460c8c-834a-4074-b3ab-1ea3ac2a6f78','Test2',22,'Test2',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:40:24.905','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:40:24.905','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role VALUES  ('14eb92f0-a212-41e2-8b0a-69c22bdc3919','Test',10,'',1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 13:47:27.430','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:25:59.280','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role VALUES  ('40c3a340-4a71-489a-bdd7-c9f4eee1e743','安全审计岗',4,'安全审计角色',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 14:18:47.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:19:42.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role VALUES  ('43947b77-b453-4783-a3fc-81e6ab8e692c','权限管理岗',3,'权限管理',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 21:56:00.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:29:49.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role VALUES  ('46a7abd1-ed2a-4617-bd38-f03b256efe84','机构管理岗',1,'机构管理角色',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 14:17:10.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-23 14:38:29.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role VALUES  ('4c2f9410-f61e-45bb-96a5-655485f823d0','统一认证',6,'统一认证',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:40:24.864','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:14:55.658','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role VALUES  ('80c9a83f-05c8-4311-98fa-bec2a920746a','Test1',21,'Test1',1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-23 15:40:32.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-26 14:35:01.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role VALUES  ('a59ec5b3-9e0c-4ca9-a13b-1a0b159c928b','用户管理岗',2,'用户管理角色',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 21:55:28.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-01-21 15:28:10.549','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role VALUES  ('df8af9fe-50c5-41b6-97dd-78e7d817c075','数据字典管理岗',5,'数据字典管理角色',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 14:20:20.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-10 21:54:20.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role VALUES  ('f71a85a8-86bf-45e1-8160-70f329f0cbed','Test2',22,'Test2',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-26 14:35:06.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-26 14:35:06.000','yyyy-MM-dd HH24:MI:ss.ff'));

INSERT INTO sys_role_menu VALUES ('0092abd4-bd3c-49cb-885b-75fc4e20795f','46a7abd1-ed2a-4617-bd38-f03b256efe84','4ef0101a-9911-47d0-8555-841ce928903f',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 09:36:08.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:33:42.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('0120d05f-cabb-4263-8197-c65f26b21b36','a59ec5b3-9e0c-4ca9-a13b-1a0b159c928b','4ef0101a-9911-47d0-8555-841ce928903f',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:17:25.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:17:25.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('023c264b-c531-4322-87a5-803bfa6a1b0d','df8af9fe-50c5-41b6-97dd-78e7d817c075','f425487a-72a1-469d-8fa1-2334db69aba4',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-12 11:39:24.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.500','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('05b594c0-4cdb-4ce1-b7a1-09c441540651','46a7abd1-ed2a-4617-bd38-f03b256efe84','26d0ed04-5670-4707-ad91-e2893edca2ed',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:33:42.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:09.815','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('06df3d46-d37b-4593-9019-952ec10a16a4','46a7abd1-ed2a-4617-bd38-f03b256efe84','b0652b6e-51ee-49ff-88b9-019bc8378f0b',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 15:48:04.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:32:20.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('08f07ebf-02b4-4881-8471-66f3e89c3cf4','46a7abd1-ed2a-4617-bd38-f03b256efe84','4ef0101a-9911-47d0-8555-841ce928903f',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:32:20.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:37:14.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('09426ceb-b0b0-4f39-84d7-fc385af403c2','46a7abd1-ed2a-4617-bd38-f03b256efe84','bec177ff-23ce-449f-ade4-dd0e62142842',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 15:48:04.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:32:20.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('0a8bad18-20c9-4118-92cc-d3e8da4015fd','df8af9fe-50c5-41b6-97dd-78e7d817c075','e2bd72c3-b926-4072-b7a9-429cb77fe351',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.558','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.558','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('0a9c7990-be95-40af-9c84-9819ab2d4ea8','46a7abd1-ed2a-4617-bd38-f03b256efe84','e2aaab2a-1b25-4e45-b54f-160bc6d332df',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 09:36:08.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:33:42.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('0c2bfdc2-11ca-4f06-91e1-32cd89d725a7','46a7abd1-ed2a-4617-bd38-f03b256efe84','b0652b6e-51ee-49ff-88b9-019bc8378f0b',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:33:42.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:09.830','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('0d900314-c4f4-4f4c-8b48-e32bce4b6d26','46a7abd1-ed2a-4617-bd38-f03b256efe84','de435288-c697-4072-8215-394dc7f80ca6',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-25 10:17:58.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 09:36:07.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('0e40ab16-84bb-4d75-9ed6-27b057b058f0','df8af9fe-50c5-41b6-97dd-78e7d817c075','1553af7d-5749-4894-8c3e-84cb3cf2f2dd',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.556','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.556','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('0ea56c75-4536-433f-a9e6-4669c2c98ff2','43947b77-b453-4783-a3fc-81e6ab8e692c','18cd9b4c-bfbd-442b-86dc-11eee193d886',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:17:43.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:17:43.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('0fc1ac60-8ab8-4685-b483-b649cb0b704e','46a7abd1-ed2a-4617-bd38-f03b256efe84','b0652b6e-51ee-49ff-88b9-019bc8378f0b',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:09.970','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:09.970','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('11a2e66b-e1ae-42d3-92b6-a97c003fbc32','46a7abd1-ed2a-4617-bd38-f03b256efe84','aa0a8b78-5e35-46f5-be7e-2a55f20f6470',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 09:36:08.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:33:42.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('12d442dd-ec97-4af6-9b60-084fb46e7cab','a59ec5b3-9e0c-4ca9-a13b-1a0b159c928b','e2aaab2a-1b25-4e45-b54f-160bc6d332df',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:17:25.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:17:25.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('1ab766ad-b0f2-43f5-b8cb-563464cb2ee2','df8af9fe-50c5-41b6-97dd-78e7d817c075','78c1335e-f05a-4431-a85b-cee182b446f3',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.555','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.555','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('1cfa9d25-a57b-49f7-ac9d-ace80f3ec2b4','46a7abd1-ed2a-4617-bd38-f03b256efe84','e2aaab2a-1b25-4e45-b54f-160bc6d332df',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:33:43.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:09.845','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('1d2966bc-1e6f-4419-9519-474b3528c455','df8af9fe-50c5-41b6-97dd-78e7d817c075','13e73c0d-a285-43af-9167-eef89381d08e',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-12 11:39:24.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.505','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('205cd604-63db-4b46-acd8-1a40e385006b','46a7abd1-ed2a-4617-bd38-f03b256efe84','45c8b9cf-5ac2-4e42-9cc8-051871a1d63f',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:37:14.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 17:21:32.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('23de9227-c943-4d41-9058-d7f3284a2a67','46a7abd1-ed2a-4617-bd38-f03b256efe84','45c8b9cf-5ac2-4e42-9cc8-051871a1d63f',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-25 10:17:58.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 09:36:07.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('247eade8-d8d6-43a9-b14d-d79f6feb6fc3','46a7abd1-ed2a-4617-bd38-f03b256efe84','e2aaab2a-1b25-4e45-b54f-160bc6d332df',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:32:20.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:37:14.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('25be70b6-477f-416b-a0ab-527ecd564351','46a7abd1-ed2a-4617-bd38-f03b256efe84','bec177ff-23ce-449f-ade4-dd0e62142842',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:37:14.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 17:21:32.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('2725a2e1-20ac-493e-b0f8-7155840cf3b6','46a7abd1-ed2a-4617-bd38-f03b256efe84','aa0a8b78-5e35-46f5-be7e-2a55f20f6470',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:32:20.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:37:14.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('2b3a9db2-8883-47d7-81bf-0da7c2d5854c','46a7abd1-ed2a-4617-bd38-f03b256efe84','e2aaab2a-1b25-4e45-b54f-160bc6d332df',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:10.073','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:10.073','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('2b747e3f-84ee-42de-9ded-f68e356f20f7','46a7abd1-ed2a-4617-bd38-f03b256efe84','8d549066-fe5d-466c-b540-3525ebf30def',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:33:42.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:09.862','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('2cab3394-02a3-4b67-a029-231eb059029c','df8af9fe-50c5-41b6-97dd-78e7d817c075','c1dc4140-ef7f-491e-a559-52614b581930',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.556','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.556','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('2fcec787-17e8-4a5d-a665-03949070c8d1','40c3a340-4a71-489a-bdd7-c9f4eee1e743','9321de30-10c8-4709-aff3-bba47312693f',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:18:01.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:18:01.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('38f5d9c0-399c-4b54-bbcc-60c79b218b2b','46a7abd1-ed2a-4617-bd38-f03b256efe84','8d549066-fe5d-466c-b540-3525ebf30def',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 15:48:04.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:32:20.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('390bda43-f6e8-4099-adab-aa7cb5cf2524','a59ec5b3-9e0c-4ca9-a13b-1a0b159c928b','e2363d77-45aa-4be1-b878-48d31aed728b',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:17:25.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:17:25.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('3914498c-ad99-4e62-98ad-e3d513388902','46a7abd1-ed2a-4617-bd38-f03b256efe84','8d549066-fe5d-466c-b540-3525ebf30def',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 17:21:32.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-25 10:17:58.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('3c49249d-a4a4-4081-873b-32112cf153d5','a59ec5b3-9e0c-4ca9-a13b-1a0b159c928b','de435288-c697-4072-8215-394dc7f80ca6',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:17:25.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:17:25.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('3d312ee5-70e3-4b1d-be84-b6ad700e69ad','46a7abd1-ed2a-4617-bd38-f03b256efe84','45c8b9cf-5ac2-4e42-9cc8-051871a1d63f',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:09.988','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:09.988','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('40670af8-9fa1-40b9-9802-c4df2580e141','40c3a340-4a71-489a-bdd7-c9f4eee1e743','0d12b992-92df-4961-a50b-a2aac55da1e2',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:18:01.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:18:01.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('44bfad32-b2bf-4916-b56d-121d431c64a1','46a7abd1-ed2a-4617-bd38-f03b256efe84','e2363d77-45aa-4be1-b878-48d31aed728b',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 09:36:08.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:33:42.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('480e595a-713d-4bb6-b4fe-2cf6ab1aa397','46a7abd1-ed2a-4617-bd38-f03b256efe84','e2aaab2a-1b25-4e45-b54f-160bc6d332df',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 17:21:32.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-25 10:17:58.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('482820c4-c5f2-4908-8944-f13fb8479a7b','46a7abd1-ed2a-4617-bd38-f03b256efe84','e2363d77-45aa-4be1-b878-48d31aed728b',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 17:21:32.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-25 10:17:58.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('4bc18573-f849-4ccb-b377-403e893fa761','df8af9fe-50c5-41b6-97dd-78e7d817c075','cc21d837-43ed-49c1-bc5f-9bb647c1fd14',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.545','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.545','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('4df789f4-394c-4b39-862f-052c00d3558c','46a7abd1-ed2a-4617-bd38-f03b256efe84','aa0a8b78-5e35-46f5-be7e-2a55f20f6470',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 17:21:32.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-25 10:17:58.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('4e50e27f-3ab4-4689-93d1-3a0b97198fde','46a7abd1-ed2a-4617-bd38-f03b256efe84','b0652b6e-51ee-49ff-88b9-019bc8378f0b',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:37:14.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 17:21:32.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('4e84e8e4-0f8b-4a54-860a-1c053a4567d1','46a7abd1-ed2a-4617-bd38-f03b256efe84','45c8b9cf-5ac2-4e42-9cc8-051871a1d63f',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:16:50.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 15:29:36.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('4eb06dab-73ec-43a5-9667-cd8c9721fef2','46a7abd1-ed2a-4617-bd38-f03b256efe84','26d0ed04-5670-4707-ad91-e2893edca2ed',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 17:21:32.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-25 10:17:58.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('4fbc8990-5ac0-476c-a549-774f1a1ed30c','46a7abd1-ed2a-4617-bd38-f03b256efe84','de435288-c697-4072-8215-394dc7f80ca6',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:33:43.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:09.875','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('50de7f7c-f006-4f2c-ae04-02981c65cf1d','46a7abd1-ed2a-4617-bd38-f03b256efe84','bec177ff-23ce-449f-ade4-dd0e62142842',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:10.019','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:10.019','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('55405817-1731-4cd1-88fb-2274a827c29c','46a7abd1-ed2a-4617-bd38-f03b256efe84','371362df-0b05-4eee-9930-9288b6c8cc45',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 09:36:08.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:33:42.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('569708cb-5437-49ed-aaea-9b5ffd5bae67','df8af9fe-50c5-41b6-97dd-78e7d817c075','f21f1b48-b743-4e01-a5d7-c50ebfd2fe78',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-12 11:39:24.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.510','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('58370717-2dfa-430c-a8c9-64cd9f137d40','46a7abd1-ed2a-4617-bd38-f03b256efe84','b0652b6e-51ee-49ff-88b9-019bc8378f0b',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 15:29:36.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 15:48:04.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('595ab411-a66a-4a2d-948c-2a9bef61811f','46a7abd1-ed2a-4617-bd38-f03b256efe84','26d0ed04-5670-4707-ad91-e2893edca2ed',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 15:48:04.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:32:20.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('5ad3fea9-c150-4456-ad26-7aecd4d7fdbb','43947b77-b453-4783-a3fc-81e6ab8e692c','837a330f-11bd-4a70-93b5-25af2c905408',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:17:43.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:17:43.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('5b69f53f-d52b-40b5-a299-dbc0b5dcf109','46a7abd1-ed2a-4617-bd38-f03b256efe84','8d549066-fe5d-466c-b540-3525ebf30def',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:10.005','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:10.005','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('5d42a663-ac16-44ca-8535-0cd2e26128c3','46a7abd1-ed2a-4617-bd38-f03b256efe84','b0652b6e-51ee-49ff-88b9-019bc8378f0b',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-25 10:17:58.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 09:36:07.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('5dc3abd4-545c-4f02-98ee-3ff8bdad900a','46a7abd1-ed2a-4617-bd38-f03b256efe84','bec177ff-23ce-449f-ade4-dd0e62142842',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:33:42.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:09.890','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('5dcdb141-102b-4ae0-afa4-7d9305aebcad','46a7abd1-ed2a-4617-bd38-f03b256efe84','45c8b9cf-5ac2-4e42-9cc8-051871a1d63f',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:32:20.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:37:14.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('60e7aaf4-ac0b-46b0-b600-68be97a15f66','46a7abd1-ed2a-4617-bd38-f03b256efe84','371362df-0b05-4eee-9930-9288b6c8cc45',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 17:21:32.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-25 10:17:58.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('6376a340-2935-433e-8ece-120428d83f9d','43947b77-b453-4783-a3fc-81e6ab8e692c','0e989075-01cd-4c6e-a622-d336106bf255',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:17:43.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:17:43.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('6477deb3-0d29-4597-8306-988ae3ae5c3b','46a7abd1-ed2a-4617-bd38-f03b256efe84','26d0ed04-5670-4707-ad91-e2893edca2ed',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:16:50.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 15:29:36.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('68d7a041-1ca5-4e8d-8142-8e703d5a8773','46a7abd1-ed2a-4617-bd38-f03b256efe84','bec177ff-23ce-449f-ade4-dd0e62142842',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 15:29:36.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 15:48:04.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('6c806321-463f-4dc8-b852-3c87a3014b94','46a7abd1-ed2a-4617-bd38-f03b256efe84','26d0ed04-5670-4707-ad91-e2893edca2ed',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 09:36:08.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:33:42.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('6ce610ad-7c24-4139-bcf4-c523dbe6e133','46a7abd1-ed2a-4617-bd38-f03b256efe84','de435288-c697-4072-8215-394dc7f80ca6',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 17:21:32.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-25 10:17:58.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('6e6ab298-f019-457d-a395-bd94142af853','46a7abd1-ed2a-4617-bd38-f03b256efe84','26d0ed04-5670-4707-ad91-e2893edca2ed',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:37:14.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 17:21:32.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('6f0d6215-8fe3-49cc-b161-3013a7b66f11','46a7abd1-ed2a-4617-bd38-f03b256efe84','de435288-c697-4072-8215-394dc7f80ca6',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:10.061','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:10.061','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('71e86fff-7f74-4773-b9c9-2d874cbc1f5b','46a7abd1-ed2a-4617-bd38-f03b256efe84','e2363d77-45aa-4be1-b878-48d31aed728b',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:10.048','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:10.048','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('73c66d2e-cd9a-40ef-8f4f-3072bd4a8036','df8af9fe-50c5-41b6-97dd-78e7d817c075','af62fa5d-4808-4a59-9297-58b0df442fff',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.549','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.549','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('7513b64e-3c74-4cd9-85b2-ed945e55ce28','46a7abd1-ed2a-4617-bd38-f03b256efe84','4ef0101a-9911-47d0-8555-841ce928903f',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 17:21:32.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-25 10:17:58.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('7b2ad447-bbba-45c2-aa08-9ac4d67d627b','46a7abd1-ed2a-4617-bd38-f03b256efe84','de435288-c697-4072-8215-394dc7f80ca6',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 09:36:08.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:33:42.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('7baf2a7b-e100-49f8-8acf-f8aee3336f97','df8af9fe-50c5-41b6-97dd-78e7d817c075','66469b10-a974-4abf-bd72-b7a205a09b47',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.551','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.551','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('7ec66b87-405a-4224-87d8-ea9bdd1c09bd','46a7abd1-ed2a-4617-bd38-f03b256efe84','8d549066-fe5d-466c-b540-3525ebf30def',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:16:50.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 15:29:36.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('7f75b282-9cde-4f1c-9e83-c8ce8ea51226','46a7abd1-ed2a-4617-bd38-f03b256efe84','aa0a8b78-5e35-46f5-be7e-2a55f20f6470',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 15:48:04.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:32:20.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('86c2008c-4d75-4989-896b-5c6ce1455f8e','a59ec5b3-9e0c-4ca9-a13b-1a0b159c928b','371362df-0b05-4eee-9930-9288b6c8cc45',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:17:25.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:17:25.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('875e822a-9603-4468-891e-f33ed99f43b4','df8af9fe-50c5-41b6-97dd-78e7d817c075','fb1f9114-e18c-4159-8af1-1408651f21fc',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.550','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.550','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('881a4d5e-cd6a-46d6-972f-59ecde62e56f','43947b77-b453-4783-a3fc-81e6ab8e692c','60f51a50-7fb8-47df-bbee-20a4bb29d650',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:17:43.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:17:43.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('8c1f377b-2660-4c1d-a29f-c23c590195db','46a7abd1-ed2a-4617-bd38-f03b256efe84','de435288-c697-4072-8215-394dc7f80ca6',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:32:20.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:37:14.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('911e2a86-f240-4fe4-adef-a7c5093c0c3b','46a7abd1-ed2a-4617-bd38-f03b256efe84','aa0a8b78-5e35-46f5-be7e-2a55f20f6470',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:33:42.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:09.903','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('922fa344-c537-4815-b7b0-4d5ae4241468','46a7abd1-ed2a-4617-bd38-f03b256efe84','bec177ff-23ce-449f-ade4-dd0e62142842',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 09:36:08.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:33:42.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('9655a999-5662-46a9-b43f-9a8c41ebd406','46a7abd1-ed2a-4617-bd38-f03b256efe84','e2363d77-45aa-4be1-b878-48d31aed728b',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:33:42.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:09.918','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('9697ce5a-ed63-49f4-80d3-52c8ba751f13','46a7abd1-ed2a-4617-bd38-f03b256efe84','26d0ed04-5670-4707-ad91-e2893edca2ed',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 15:29:36.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 15:48:04.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('973f3d07-c2fe-443e-b466-a5bc3987d7b9','46a7abd1-ed2a-4617-bd38-f03b256efe84','bec177ff-23ce-449f-ade4-dd0e62142842',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:32:20.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:37:14.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('9837e688-41d3-47d9-af6d-a8b808f4056c','df8af9fe-50c5-41b6-97dd-78e7d817c075','eed891c4-822d-41d4-ac80-c6b9c327a1af',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.562','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.562','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('9a10f2c7-40f8-4a83-b946-0223fa8ddce3','df8af9fe-50c5-41b6-97dd-78e7d817c075','d1bf1584-3c7a-4642-8cc6-095c42682e15',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.554','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.554','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('9a123bc2-fbd9-4770-9fae-d41e23a046ba','df8af9fe-50c5-41b6-97dd-78e7d817c075','2634a816-5dd9-4bfd-a573-f6b0020204db',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-12 11:39:24.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.515','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('9b8dd0ba-8179-44e1-8f91-ea31005f2af7','df8af9fe-50c5-41b6-97dd-78e7d817c075','ef3148eb-ebe6-4d9a-8806-aa783570290b',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.552','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.552','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('a3cc3ad5-2113-4905-9347-50c53a362c1e','46a7abd1-ed2a-4617-bd38-f03b256efe84','26d0ed04-5670-4707-ad91-e2893edca2ed',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:32:20.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:37:14.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('a5b7a194-508c-4abc-bf8e-962a752aad67','46a7abd1-ed2a-4617-bd38-f03b256efe84','aa0a8b78-5e35-46f5-be7e-2a55f20f6470',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:09.954','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:09.954','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('a5cbe61c-33df-4521-8f3d-9fa0f83f7885','df8af9fe-50c5-41b6-97dd-78e7d817c075','206bb7dc-101a-4d76-89fc-606cdd247355',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.561','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.561','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('aa168dec-ac4d-49ce-9510-ac84d50e3977','46a7abd1-ed2a-4617-bd38-f03b256efe84','371362df-0b05-4eee-9930-9288b6c8cc45',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:32:20.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:37:14.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('b45a293d-777b-4895-ab88-747878ecd04d','df8af9fe-50c5-41b6-97dd-78e7d817c075','8169ab55-b24e-45b5-99f1-9d65d55af215',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.548','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.548','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('b8227404-9296-4f6e-9a96-a861f2453681','46a7abd1-ed2a-4617-bd38-f03b256efe84','bec177ff-23ce-449f-ade4-dd0e62142842',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 17:21:32.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-25 10:17:58.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('b93c9136-00c9-48b0-8b8a-f05340e7e624','46a7abd1-ed2a-4617-bd38-f03b256efe84','45c8b9cf-5ac2-4e42-9cc8-051871a1d63f',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 09:36:08.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:33:42.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('ba80d399-796b-4514-9e4b-35f468c17920','46a7abd1-ed2a-4617-bd38-f03b256efe84','26d0ed04-5670-4707-ad91-e2893edca2ed',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-25 10:17:58.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 09:36:07.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('c340e93a-6955-4641-8069-872f98d1180a','46a7abd1-ed2a-4617-bd38-f03b256efe84','bec177ff-23ce-449f-ade4-dd0e62142842',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:16:50.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 15:29:36.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('c38053fa-3bae-43c9-8275-af0165dba8cb','46a7abd1-ed2a-4617-bd38-f03b256efe84','45c8b9cf-5ac2-4e42-9cc8-051871a1d63f',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 15:48:04.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:32:20.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('c3fab066-492a-4b14-a7d1-aaa853abbe92','46a7abd1-ed2a-4617-bd38-f03b256efe84','45c8b9cf-5ac2-4e42-9cc8-051871a1d63f',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 17:21:32.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-25 10:17:58.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('c65374f5-fc27-440e-b991-714c41102a85','46a7abd1-ed2a-4617-bd38-f03b256efe84','8d549066-fe5d-466c-b540-3525ebf30def',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-25 10:17:58.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 09:36:07.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('c789c2d1-4eef-488a-a12d-5d34d2b09759','46a7abd1-ed2a-4617-bd38-f03b256efe84','45c8b9cf-5ac2-4e42-9cc8-051871a1d63f',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:33:42.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:09.933','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('c81be673-9065-4046-a16e-c705a9440fe1','43947b77-b453-4783-a3fc-81e6ab8e692c','b807f782-8b8c-49b6-a62d-e230dd44cd7e',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:17:43.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:17:43.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('cd33f801-eed3-44a8-be02-e97a13e471a0','43947b77-b453-4783-a3fc-81e6ab8e692c','d0ebd39e-9a82-42fa-986e-c7235747a758',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:17:43.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:17:43.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('ce31fb84-6675-4c4d-8130-1c9adcfcdc75','46a7abd1-ed2a-4617-bd38-f03b256efe84','b0652b6e-51ee-49ff-88b9-019bc8378f0b',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 17:21:32.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-25 10:17:58.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('ceb83520-6be4-478b-b41e-807c9780adef','46a7abd1-ed2a-4617-bd38-f03b256efe84','e2363d77-45aa-4be1-b878-48d31aed728b',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-25 10:17:58.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 09:36:07.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('d0c677b2-ca69-4ec4-8a6e-5bbfff9d90f5','df8af9fe-50c5-41b6-97dd-78e7d817c075','03237dc0-d26f-4611-8fe1-ba08ac1da7d8',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.560','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.560','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('d206cc5f-93b3-4d3b-9bff-24e9b899339b','df8af9fe-50c5-41b6-97dd-78e7d817c075','fb35235e-b771-462e-b84b-269f3d8938ce',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.558','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.558','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('d25c5c8b-b738-4484-8856-029d5423e330','46a7abd1-ed2a-4617-bd38-f03b256efe84','b0652b6e-51ee-49ff-88b9-019bc8378f0b',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:16:50.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 15:29:36.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('d37d8b9e-4d8b-4999-8d2e-9a9ce3511d91','46a7abd1-ed2a-4617-bd38-f03b256efe84','bec177ff-23ce-449f-ade4-dd0e62142842',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-25 10:17:58.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 09:36:07.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('d5334fcf-c37a-44d3-8ee1-4821e42a0c15','46a7abd1-ed2a-4617-bd38-f03b256efe84','371362df-0b05-4eee-9930-9288b6c8cc45',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:10.100','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:10.100','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('dac74003-0e7e-4fa7-b3c4-fe7f7bc35c09','46a7abd1-ed2a-4617-bd38-f03b256efe84','26d0ed04-5670-4707-ad91-e2893edca2ed',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:10.033','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:10.033','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('dc04de41-8510-457f-bbef-581d940a0335','46a7abd1-ed2a-4617-bd38-f03b256efe84','aa0a8b78-5e35-46f5-be7e-2a55f20f6470',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-25 10:17:58.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 09:36:07.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('dcc0d0f4-6200-4999-8f2c-7f137c681947','46a7abd1-ed2a-4617-bd38-f03b256efe84','8d549066-fe5d-466c-b540-3525ebf30def',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 09:36:08.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:33:42.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('df2c829a-c8fd-4404-b47e-2413d7ea2724','46a7abd1-ed2a-4617-bd38-f03b256efe84','e2363d77-45aa-4be1-b878-48d31aed728b',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:32:20.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:37:14.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('e0d022f6-436a-4d64-bffb-c8c863536a63','46a7abd1-ed2a-4617-bd38-f03b256efe84','b0652b6e-51ee-49ff-88b9-019bc8378f0b',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:32:20.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:37:14.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('e426bce5-6045-418e-b169-1480af3fbb4e','df8af9fe-50c5-41b6-97dd-78e7d817c075','a81bb7aa-d7c2-41b9-b616-b46d3d7d2cf3',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.547','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:15.547','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('e6055bd9-e2a9-48c2-af79-62c2303e5866','46a7abd1-ed2a-4617-bd38-f03b256efe84','8d549066-fe5d-466c-b540-3525ebf30def',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:37:14.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 17:21:32.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('e6830da9-8a89-4826-b0fd-a6d2bc7103cf','46a7abd1-ed2a-4617-bd38-f03b256efe84','45c8b9cf-5ac2-4e42-9cc8-051871a1d63f',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 15:29:36.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 15:48:04.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('ec8ca9c7-53d5-459e-859e-3d7db48de4d6','43947b77-b453-4783-a3fc-81e6ab8e692c','c80f26ab-b2bc-4766-a40e-17ed9417a5c7',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:17:43.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:17:43.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('ecae4db3-751d-409e-b9d7-f5910c215a03','46a7abd1-ed2a-4617-bd38-f03b256efe84','4ef0101a-9911-47d0-8555-841ce928903f',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:10.088','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:10.088','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('ed1ba712-b3c0-436e-a236-50aec1b9861c','46a7abd1-ed2a-4617-bd38-f03b256efe84','aa0a8b78-5e35-46f5-be7e-2a55f20f6470',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:37:14.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 17:21:32.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('fdafe34a-60f1-42b1-b470-86677aa2c781','46a7abd1-ed2a-4617-bd38-f03b256efe84','8d549066-fe5d-466c-b540-3525ebf30def',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 15:29:36.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 15:48:04.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('fe1fe74b-d470-48c7-a2de-241cfe273f18','46a7abd1-ed2a-4617-bd38-f03b256efe84','b0652b6e-51ee-49ff-88b9-019bc8378f0b',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 09:36:08.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:33:42.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_menu VALUES ('ff138593-6447-4b54-8b7b-41b548228d67','46a7abd1-ed2a-4617-bd38-f03b256efe84','8d549066-fe5d-466c-b540-3525ebf30def',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:32:20.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 16:37:14.000','yyyy-MM-dd HH24:MI:ss.ff'));

INSERT INTO sys_role_org VALUES ('354845ca-eace-4d95-a65a-37496580c517','40c3a340-4a71-489a-bdd7-c9f4eee1e743','ab907b1e-13a3-4c04-85db-f560f9c8f9aa',0,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-11-09 16:02:29','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-11-09 16:47:19','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_org VALUES  ('354845ca-eace-4d95-a65a-37496580c518','46a7abd1-ed2a-4617-bd38-f03b256efe84','ab907b1e-13a3-4c04-85db-f560f9c8f9aa',0,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-11-09 16:40:50','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-11-09 16:48:39','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_org VALUES  ('354845ca-eace-4d95-a65a-37496580c519','a59ec5b3-9e0c-4ca9-a13b-1a0b159c928b','ab907b1e-13a3-4c04-85db-f560f9c8f9aa',0,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-11-09 16:02:29','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-11-09 16:47:19','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_org VALUES  ('5ec2513e-621c-4b98-b10f-6fc0ef0bada6','12460c8c-834a-4074-b3ab-1ea3ac2a6f78','ab907b1e-13a3-4c04-85db-f560f9c8f9aa',0,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:40:25','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:40:25','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_org VALUES  ('672d6f9b-c295-4972-a71e-56a566f68f88','43947b77-b453-4783-a3fc-81e6ab8e692c','ab907b1e-13a3-4c04-85db-f560f9c8f9aa',0,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-11-09 16:40:50','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-11-09 16:48:39','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_org VALUES  ('7aff73ff-bca8-47f1-bdb3-adb55c251b65','46a7abd1-ed2a-4617-bd38-f03b256efe84','57126a11-842e-493d-bc4f-e8c18853e6ed',1,NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:42','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:46','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_org VALUES  ('8ee4a37b-593d-460a-beb9-88d9ab2cdb7c','4c2f9410-f61e-45bb-96a5-655485f823d0','ab907b1e-13a3-4c04-85db-f560f9c8f9aa',0,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:40:25','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:40:25','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_org VALUES  ('9c406a50-1f09-4aa3-8b2b-7a57fabda9b2','14eb92f0-a212-41e2-8b0a-69c22bdc3919','ab907b1e-13a3-4c04-85db-f560f9c8f9aa',0,NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 13:47:27','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:25:59','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_org VALUES  ('e50ee78e-ee79-4058-b5b0-7cc31114f9af','df8af9fe-50c5-41b6-97dd-78e7d817c075','ab907b1e-13a3-4c04-85db-f560f9c8f9aa',0,NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-11-09 16:40:45','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-11-09 16:47:38','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_org VALUES  ('e743f7bf-6c7c-43e7-8725-c786374c12f1','46a7abd1-ed2a-4617-bd38-f03b256efe84','9756e90f-9505-4990-ac0e-135c58bc6f29',1,NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:37','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-03-17 16:43:23','yyyy-MM-dd HH24:MI:ss.ff'));

INSERT INTO sys_role_user VALUES ('0a1d385e-5bc6-4648-8d43-bad5b642fd7c','df8af9fe-50c5-41b6-97dd-78e7d817c075','930cc8e2-f1ae-404d-9258-6167a3acbcbc',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-12 11:39:37.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-12 11:39:37.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_user VALUES ('0eafa373-aa13-467e-a65b-06b74f813bf9','46a7abd1-ed2a-4617-bd38-f03b256efe84','b09bbb4b-a7f0-4f7a-8f3d-dbb895f3edc5',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:11:59.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 15:11:35.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_user VALUES ('0efed5b5-7eee-48fd-ad21-de8d80e5468d','46a7abd1-ed2a-4617-bd38-f03b256efe84','6f415677-cc02-4efa-bfbd-fa7c1c2c561d',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:20.066','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-12-03 16:26:29.364','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_user VALUES ('11d34b17-f2c2-4a68-ba3a-fa211231e85b','46a7abd1-ed2a-4617-bd38-f03b256efe84','45aeff89-2867-43e7-9b40-296233c069c5',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:09:40.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:09:53.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_user VALUES ('1e119430-430c-438c-9f63-4427f927442e','46a7abd1-ed2a-4617-bd38-f03b256efe84','45aeff89-2867-43e7-9b40-296233c069c5',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 12:52:28.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:02:52.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_user VALUES ('293567d8-8ae4-4731-9975-08721e293dd1','46a7abd1-ed2a-4617-bd38-f03b256efe84','b09bbb4b-a7f0-4f7a-8f3d-dbb895f3edc5',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:09:40.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:09:53.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_user VALUES ('42279870-6f6f-4287-885f-82180b7d64e1','46a7abd1-ed2a-4617-bd38-f03b256efe84','b09bbb4b-a7f0-4f7a-8f3d-dbb895f3edc5',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 12:52:28.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:02:52.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_user VALUES ('43324d78-f054-486e-b688-7618106209a0','46a7abd1-ed2a-4617-bd38-f03b256efe84','45aeff89-2867-43e7-9b40-296233c069c5',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 15:11:35.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:33:23.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_user VALUES ('45f9b5a2-7b43-43bb-8544-b774488535d6','a59ec5b3-9e0c-4ca9-a13b-1a0b159c928b','930cc8e2-f1ae-404d-9258-6167a3acbcbc',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:15:58.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:15:58.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_user VALUES ('4b32b83d-51ca-4a29-a0ea-f05a56fa544b','46a7abd1-ed2a-4617-bd38-f03b256efe84','930cc8e2-f1ae-404d-9258-6167a3acbcbc',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:03:18.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:09:40.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_user VALUES ('4f380cdc-11ae-4405-bd5b-227a7b4acac3','46a7abd1-ed2a-4617-bd38-f03b256efe84','b09bbb4b-a7f0-4f7a-8f3d-dbb895f3edc5',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 12:51:19.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 12:52:19.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_user VALUES ('5e16587c-1d38-443f-a779-b09714c835d3','46a7abd1-ed2a-4617-bd38-f03b256efe84','930cc8e2-f1ae-404d-9258-6167a3acbcbc',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:09:40.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:09:53.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_user VALUES ('6a21a69b-5955-4a89-9611-3cd47eb7c3e0','46a7abd1-ed2a-4617-bd38-f03b256efe84','b09bbb4b-a7f0-4f7a-8f3d-dbb895f3edc5',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:33:23.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-03-17 16:43:30.139','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_user VALUES ('740135f9-568f-476b-91de-583f4efc6b8d','df8af9fe-50c5-41b6-97dd-78e7d817c075','45aeff89-2867-43e7-9b40-296233c069c5',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:42.488','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2022-02-24 14:15:42.488','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_user VALUES ('759d8f21-fecd-445d-9d3f-eef61c197f11','46a7abd1-ed2a-4617-bd38-f03b256efe84','930cc8e2-f1ae-404d-9258-6167a3acbcbc',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 12:51:19.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 12:52:19.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_user VALUES ('834f6a33-68c3-4bbd-b863-85ece4fda4e7','46a7abd1-ed2a-4617-bd38-f03b256efe84','45aeff89-2867-43e7-9b40-296233c069c5',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:03:18.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:09:40.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_user VALUES ('8a72cc3c-a89a-4c30-9438-e96eeab1e6d1','46a7abd1-ed2a-4617-bd38-f03b256efe84','45aeff89-2867-43e7-9b40-296233c069c5',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 12:51:19.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 12:52:19.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_user VALUES ('c6999828-2fd3-43bb-adbf-d5bbcd4de1b7','46a7abd1-ed2a-4617-bd38-f03b256efe84','45aeff89-2867-43e7-9b40-296233c069c5',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:33:23.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:33:23.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_user VALUES ('da6de310-78a3-4775-9048-788acb19bdb1','46a7abd1-ed2a-4617-bd38-f03b256efe84','b09bbb4b-a7f0-4f7a-8f3d-dbb895f3edc5',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-23 15:11:35.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:33:23.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_user VALUES ('dafb911b-c2b2-42c1-aa16-9b704128c5b4','46a7abd1-ed2a-4617-bd38-f03b256efe84','930cc8e2-f1ae-404d-9258-6167a3acbcbc',NULL,0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:33:23.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-29 15:33:23.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_user VALUES ('e7640512-dfbb-44a8-aadf-af3c82f29bf7','46a7abd1-ed2a-4617-bd38-f03b256efe84','45aeff89-2867-43e7-9b40-296233c069c5',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:09:53.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:10:25.000','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_role_user VALUES ('ff59a249-f725-488d-aa8a-2d0e42478231','46a7abd1-ed2a-4617-bd38-f03b256efe84','930cc8e2-f1ae-404d-9258-6167a3acbcbc',NULL,1,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 12:52:28.000','yyyy-MM-dd HH24:MI:ss.ff'),'00000000-0000-0000-0000-000000000000',to_timestamp('2021-04-11 16:02:52.000','yyyy-MM-dd HH24:MI:ss.ff'));

INSERT INTO sys_schedule_job VALUES ('10f976a2-f9ef-4629-8f43-9afce17c2a9d','邮件发送任务','定时发送邮件作业','RuYiAdmin.Net.Service.QuartzJobs','EmailJob','0/5 * * * * ?',to_timestamp('2021-09-10 10:19:12','yyyy-MM-dd HH24:MI:ss.ff'),to_timestamp('9999-12-31 00:00:00','yyyy-MM-dd HH24:MI:ss.ff'),1,1,2,'定时发送邮件作业',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-10 10:16:47.000','yyyy-MM-dd HH24:MI:ss.ff'),'0374f393-ed3b-40d5-9e7c-65b560b67f6a',to_timestamp('2022-03-17 10:20:48.533','yyyy-MM-dd HH24:MI:ss.ff'));
INSERT INTO sys_schedule_job VALUES ('8df4699c-b4d4-468f-9037-df49cd5a62dd','定时短信任务','定时发送短信作业','RuYiAdmin.Net.Service.QuartzJobs','MessageJob','0/5 * * * * ?',to_timestamp('2021-09-10 09:46:28','yyyy-MM-dd HH24:MI:ss.ff'),to_timestamp('9999-12-31 00:00:00','yyyy-MM-dd HH24:MI:ss.ff'),1,1,1,'定时短信任务作业',0,'00000000-0000-0000-0000-000000000000',to_timestamp('2021-09-10 09:33:43.000','yyyy-MM-dd HH24:MI:ss.ff'),'f7d72a4b-b2bc-4bda-ad34-181432a974c0',to_timestamp('2022-03-18 10:21:55.887','yyyy-MM-dd HH24:MI:ss.ff'));
