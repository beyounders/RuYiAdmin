USE [master]
GO
/****** Object:  Database [RuYiAdmin]    Script Date: 2022/4/11 11:12:46 ******/
CREATE DATABASE [RuYiAdmin]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'RuYiAdmin', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\RuYiAdmin.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'RuYiAdmin_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\RuYiAdmin_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [RuYiAdmin] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [RuYiAdmin].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [RuYiAdmin] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [RuYiAdmin] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [RuYiAdmin] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [RuYiAdmin] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [RuYiAdmin] SET ARITHABORT OFF 
GO
ALTER DATABASE [RuYiAdmin] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [RuYiAdmin] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [RuYiAdmin] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [RuYiAdmin] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [RuYiAdmin] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [RuYiAdmin] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [RuYiAdmin] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [RuYiAdmin] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [RuYiAdmin] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [RuYiAdmin] SET  ENABLE_BROKER 
GO
ALTER DATABASE [RuYiAdmin] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [RuYiAdmin] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [RuYiAdmin] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [RuYiAdmin] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [RuYiAdmin] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [RuYiAdmin] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [RuYiAdmin] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [RuYiAdmin] SET RECOVERY FULL 
GO
ALTER DATABASE [RuYiAdmin] SET  MULTI_USER 
GO
ALTER DATABASE [RuYiAdmin] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [RuYiAdmin] SET DB_CHAINING OFF 
GO
ALTER DATABASE [RuYiAdmin] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [RuYiAdmin] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [RuYiAdmin] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [RuYiAdmin] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'RuYiAdmin', N'ON'
GO
ALTER DATABASE [RuYiAdmin] SET QUERY_STORE = OFF
GO
USE [RuYiAdmin]
GO
/****** Object:  Table [dbo].[biz_module]    Script Date: 2022/4/11 11:12:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[biz_module](
	[Id] [uniqueidentifier] NOT NULL,
	[ModuleName] [nvarchar](512) NOT NULL,
	[ModuleShortName] [nvarchar](256) NOT NULL,
	[ModuleShortNameEN] [nvarchar](128) NOT NULL,
	[ModuleProtocol] [nvarchar](15) NOT NULL,
	[ModuleAddress] [nvarchar](256) NOT NULL,
	[ModulePort] [int] NULL,
	[ModuleLogoAddress] [nvarchar](512) NULL,
	[ModuleSsoAddress] [nvarchar](512) NOT NULL,
	[ModuleTodoAddress] [nvarchar](512) NULL,
	[SerialNumber] [int] NULL,
	[Remark] [nvarchar](512) NULL,
	[IsDel] [int] NOT NULL,
	[Creator] [uniqueidentifier] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[Modifier] [uniqueidentifier] NOT NULL,
	[ModifyTime] [datetime] NOT NULL,
 CONSTRAINT [PK_BIZ_MODULE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[biz_account]    Script Date: 2022/4/11 11:12:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[biz_account](
	[Id] [uniqueidentifier] NOT NULL,
	[ModuleId] [uniqueidentifier] NOT NULL,
	[UserLogonName] [nvarchar](128) NOT NULL,
	[UserDisplayName] [nvarchar](128) NOT NULL,
	[UserPassword] [nvarchar](512) NOT NULL,
	[Telephone] [nvarchar](45) NULL,
	[MobilePhone] [nvarchar](45) NULL,
	[Email] [nvarchar](45) NULL,
	[IsEnabled] [int] NOT NULL,
	[Remark] [nvarchar](512) NULL,
	[IsDel] [int] NOT NULL,
	[Creator] [uniqueidentifier] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[Modifier] [uniqueidentifier] NOT NULL,
	[ModifyTime] [datetime] NOT NULL,
 CONSTRAINT [PK_BIZ_ACCOUNT] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[biz_user]    Script Date: 2022/4/11 11:12:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[biz_user](
	[Id] [uniqueidentifier] NOT NULL,
	[UserLogonName] [nvarchar](128) NOT NULL,
	[UserDisplayName] [nvarchar](128) NOT NULL,
	[UserPassword] [nvarchar](512) NOT NULL,
	[Telephone] [nvarchar](45) NULL,
	[MobilePhone] [nvarchar](45) NULL,
	[Email] [nvarchar](45) NULL,
	[IsEnabled] [int] NOT NULL,
	[Remark] [nvarchar](512) NULL,
	[IsDel] [int] NOT NULL,
	[Creator] [uniqueidentifier] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[Modifier] [uniqueidentifier] NOT NULL,
	[ModifyTime] [datetime] NOT NULL,
 CONSTRAINT [PK_BIZ_USER] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[biz_user_module]    Script Date: 2022/4/11 11:12:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[biz_user_module](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[ModuleId] [uniqueidentifier] NOT NULL,
	[UserModuleLogonName] [nvarchar](128) NOT NULL,
	[UserModulePassword] [nvarchar](512) NOT NULL,
	[Remark] [nvarchar](512) NULL,
	[IsDel] [int] NOT NULL,
	[Creator] [uniqueidentifier] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[Modifier] [uniqueidentifier] NOT NULL,
	[ModifyTime] [datetime] NOT NULL,
 CONSTRAINT [PK_BIZ_USER_MODULE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sys_code_table]    Script Date: 2022/4/11 11:12:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_code_table](
	[Id] [uniqueidentifier] NOT NULL,
	[CodeName] [nvarchar](128) NOT NULL,
	[Code] [nvarchar](128) NOT NULL,
	[Value] [nvarchar](256) NULL,
	[ParentId] [uniqueidentifier] NULL,
	[Remark] [nvarchar](512) NULL,
	[SerialNumber] [int] NULL,
	[IsDel] [int] NOT NULL,
	[Creator] [uniqueidentifier] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[Modifier] [uniqueidentifier] NOT NULL,
	[ModifyTime] [datetime] NOT NULL,
 CONSTRAINT [PK_SYS_CODE_TABLE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sys_import_config]    Script Date: 2022/4/11 11:12:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_import_config](
	[Id] [uniqueidentifier] NOT NULL,
	[ConfigName] [nvarchar](128) NOT NULL,
	[StartRow] [int] NOT NULL,
	[StartColumn] [int] NOT NULL,
	[WorkSheetIndexes] [nvarchar](512) NULL,
	[SerialNumber] [int] NULL,
	[Remark] [nvarchar](512) NULL,
	[IsDel] [int] NOT NULL,
	[Creator] [uniqueidentifier] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[Modifier] [uniqueidentifier] NOT NULL,
	[ModifyTime] [datetime] NOT NULL,
 CONSTRAINT [PK_SYS_IMPORT_CONFIG] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sys_import_config_detail]    Script Date: 2022/4/11 11:12:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_import_config_detail](
	[Id] [uniqueidentifier] NOT NULL,
	[ParentId] [uniqueidentifier] NOT NULL,
	[DataType] [int] NOT NULL,
	[Cells] [nvarchar](512) NOT NULL,
	[Required] [int] NULL,
	[MaxValue] [float] NULL,
	[MinValue] [float] NULL,
	[DecimalLimit] [int] NULL,
	[TextEnum] [nvarchar](1024) NULL,
	[Extend1] [nvarchar](64) NULL,
	[Extend2] [nvarchar](128) NULL,
	[Extend3] [nvarchar](256) NULL,
	[SerialNumber] [int] NULL,
	[Remark] [nvarchar](512) NULL,
	[IsDel] [int] NOT NULL,
	[Creator] [uniqueidentifier] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[Modifier] [uniqueidentifier] NOT NULL,
	[ModifyTime] [datetime] NOT NULL,
 CONSTRAINT [PK_SYS_IMPORT_CONFIG_DETAIL] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sys_language]    Script Date: 2022/4/11 11:12:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_language](
	[Id] [uniqueidentifier] NOT NULL,
	[LanguageName] [nvarchar](45) NOT NULL,
	[OrderNumber] [int] NULL,
	[Remark] [nvarchar](512) NULL,
	[IsDel] [int] NOT NULL,
	[Creator] [uniqueidentifier] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[Modifier] [uniqueidentifier] NOT NULL,
	[ModifyTime] [datetime] NOT NULL,
 CONSTRAINT [PK_SYS_LANGUAGE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sys_log]    Script Date: 2022/4/11 11:12:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_log](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[UserName] [nvarchar](128) NOT NULL,
	[OrgId] [uniqueidentifier] NOT NULL,
	[OrgName] [nvarchar](128) NOT NULL,
	[System] [nvarchar](128) NULL,
	[Browser] [nvarchar](128) NULL,
	[IP] [nvarchar](45) NOT NULL,
	[OperationType] [int] NOT NULL,
	[RequestUrl] [nvarchar](512) NOT NULL,
	[Params] [nvarchar](1024) NULL,
	[Result] [nvarchar](max) NULL,
	[OldVaue] [nvarchar](max) NULL,
	[NewValue] [nvarchar](max) NULL,
	[Remark] [nvarchar](512) NULL,
	[IsDel] [int] NOT NULL,
	[Creator] [uniqueidentifier] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[Modifier] [uniqueidentifier] NOT NULL,
	[ModifyTime] [datetime] NOT NULL,
 CONSTRAINT [PK_SYS_LOG] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sys_menu]    Script Date: 2022/4/11 11:12:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_menu](
	[Id] [uniqueidentifier] NOT NULL,
	[Path] [nvarchar](256) NULL,
	[MenuName] [nvarchar](128) NOT NULL,
	[MenuUrl] [nvarchar](256) NULL,
	[ParentId] [uniqueidentifier] NULL,
	[SerialNumber] [int] NULL,
	[MenuType] [int] NOT NULL,
	[Icon] [nvarchar](256) NULL,
	[Code] [nvarchar](125) NULL,
	[Remark] [nvarchar](512) NULL,
	[IsDel] [int] NOT NULL,
	[Creator] [uniqueidentifier] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[Modifier] [uniqueidentifier] NOT NULL,
	[ModifyTime] [datetime] NOT NULL,
 CONSTRAINT [PK_SYS_MENU] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sys_menu_language]    Script Date: 2022/4/11 11:12:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_menu_language](
	[Id] [uniqueidentifier] NOT NULL,
	[MenuId] [uniqueidentifier] NOT NULL,
	[LanguageId] [uniqueidentifier] NOT NULL,
	[MenuName] [nvarchar](128) NOT NULL,
	[Remark] [nvarchar](512) NULL,
	[IsDel] [int] NOT NULL,
	[Creator] [uniqueidentifier] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[Modifier] [uniqueidentifier] NOT NULL,
	[ModifyTime] [datetime] NOT NULL,
 CONSTRAINT [PK_SYS_MENU_LANGUAGE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sys_organization]    Script Date: 2022/4/11 11:12:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_organization](
	[Id] [uniqueidentifier] NOT NULL,
	[OrgName] [nvarchar](128) NOT NULL,
	[ParentId] [uniqueidentifier] NULL,
	[Leader] [uniqueidentifier] NULL,
	[SerialNumber] [int] NULL,
	[Remark] [nvarchar](512) NULL,
	[IsDel] [int] NOT NULL,
	[Creator] [uniqueidentifier] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[Modifier] [uniqueidentifier] NOT NULL,
	[ModifyTime] [datetime] NOT NULL,
 CONSTRAINT [PK_SYS_ORGANIZATION] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sys_user]    Script Date: 2022/4/11 11:12:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_user](
	[Id] [uniqueidentifier] NOT NULL,
	[LogonName] [nvarchar](128) NOT NULL,
	[DisplayName] [nvarchar](128) NOT NULL,
	[Password] [nvarchar](512) NULL,
	[Telephone] [nvarchar](45) NULL,
	[MobilePhone] [nvarchar](45) NULL,
	[Email] [nvarchar](45) NULL,
	[Remark] [nvarchar](512) NULL,
	[IsSupperAdmin] [int] NULL,
	[SerialNumber] [int] NULL,
	[IsEnabled] [int] NOT NULL,
	[IsDel] [int] NOT NULL,
	[Creator] [uniqueidentifier] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[Modifier] [uniqueidentifier] NOT NULL,
	[ModifyTime] [datetime] NOT NULL,
 CONSTRAINT [PK_SYS_USER] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sys_org_user]    Script Date: 2022/4/11 11:12:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_org_user](
	[Id] [uniqueidentifier] NOT NULL,
	[OrgId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[Remark] [nvarchar](512) NULL,
	[IsDel] [int] NOT NULL,
	[Creator] [uniqueidentifier] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[Modifier] [uniqueidentifier] NOT NULL,
	[ModifyTime] [datetime] NOT NULL,
 CONSTRAINT [PK_SYS_ORG_USER] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sys_role]    Script Date: 2022/4/11 11:12:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_role](
	[Id] [uniqueidentifier] NOT NULL,
	[RoleName] [nvarchar](128) NOT NULL,
	[SerialNumber] [int] NULL,
	[Remark] [nvarchar](512) NULL,
	[IsDel] [int] NOT NULL,
	[Creator] [uniqueidentifier] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[Modifier] [uniqueidentifier] NOT NULL,
	[ModifyTime] [datetime] NOT NULL,
 CONSTRAINT [PK_SYS_ROLE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sys_role_menu]    Script Date: 2022/4/11 11:12:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_role_menu](
	[Id] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
	[MenuId] [uniqueidentifier] NOT NULL,
	[Remark] [nvarchar](512) NULL,
	[IsDel] [int] NOT NULL,
	[Creator] [uniqueidentifier] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[Modifier] [uniqueidentifier] NOT NULL,
	[ModifyTime] [datetime] NOT NULL,
 CONSTRAINT [PK_SYS_ROLE_MENU] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sys_role_org]    Script Date: 2022/4/11 11:12:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_role_org](
	[Id] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
	[OrgId] [uniqueidentifier] NOT NULL,
	[OwnerType] [int] NOT NULL,
	[Remark] [nvarchar](512) NULL,
	[IsDel] [int] NOT NULL,
	[Creator] [uniqueidentifier] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[Modifier] [uniqueidentifier] NOT NULL,
	[ModifyTime] [datetime] NOT NULL,
 CONSTRAINT [PK_SYS_ROLE_ORG] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sys_role_user]    Script Date: 2022/4/11 11:12:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_role_user](
	[Id] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[Remark] [nvarchar](512) NULL,
	[IsDel] [int] NOT NULL,
	[Creator] [uniqueidentifier] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[Modifier] [uniqueidentifier] NOT NULL,
	[ModifyTime] [datetime] NOT NULL,
 CONSTRAINT [PK_SYS_ROLE_USER] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sys_schedule_job]    Script Date: 2022/4/11 11:12:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sys_schedule_job](
	[Id] [uniqueidentifier] NOT NULL,
	[JobName] [nvarchar](128) NOT NULL,
	[JobDescription] [nvarchar](512) NULL,
	[NameSpace] [nvarchar](512) NOT NULL,
	[JobImplement] [nvarchar](128) NOT NULL,
	[CronExpression] [nvarchar](128) NOT NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[JobStatus] [int] NOT NULL,
	[GroupId] [int] NULL,
	[SerialNumber] [int] NULL,
	[Remark] [nvarchar](512) NULL,
	[IsDel] [int] NOT NULL,
	[Creator] [uniqueidentifier] NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[Modifier] [uniqueidentifier] NOT NULL,
	[ModifyTime] [datetime] NOT NULL,
 CONSTRAINT [PK_SYS_SCHEDULE_JOB] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[biz_module] ([Id], [ModuleName], [ModuleShortName], [ModuleShortNameEN], [ModuleProtocol], [ModuleAddress], [ModulePort], [ModuleLogoAddress], [ModuleSsoAddress], [ModuleTodoAddress], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'81762d87-d4c5-483f-937f-4c0e77b534b5', N'若依Admin开源项目平台', N'若依Admin', N'ry', N'http', N'127.0.0.1', 8086, NULL, N'/sso', N'/todo', 1, N'若依Admin', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-07T10:22:22.530' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-07T10:32:02.693' AS DateTime))
INSERT [dbo].[biz_module] ([Id], [ModuleName], [ModuleShortName], [ModuleShortNameEN], [ModuleProtocol], [ModuleAddress], [ModulePort], [ModuleLogoAddress], [ModuleSsoAddress], [ModuleTodoAddress], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'97b36999-a9b0-4033-bd06-7f54a03a6fd7', N'若依Admin开源项目平台', N'若依Admin', N'ry', N'http', N'127.0.0.1', 8086, NULL, N'/sso', N'/todo', 1, NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-23T15:30:14.727' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-23T15:32:19.413' AS DateTime))
INSERT [dbo].[biz_module] ([Id], [ModuleName], [ModuleShortName], [ModuleShortNameEN], [ModuleProtocol], [ModuleAddress], [ModulePort], [ModuleLogoAddress], [ModuleSsoAddress], [ModuleTodoAddress], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'4a6798ec-0296-4516-9618-bd66cb77fa53', N'一莎Admin开源项目平台', N'一莎Admin', N'ys', N'http', N'127.0.0.1', 8089, NULL, N'/sso', N'/todo', 2, N'一莎Admin', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-07T10:26:44.670' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-07T10:38:17.460' AS DateTime))
GO
INSERT [dbo].[biz_account] ([Id], [ModuleId], [UserLogonName], [UserDisplayName], [UserPassword], [Telephone], [MobilePhone], [Email], [IsEnabled], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'e353c06a-78d3-4366-b951-1759894913af', N'4a6798ec-0296-4516-9618-bd66cb77fa53', N'ABC', N'ABC', N'BVm4iKPKu1/MPYhx853sxQ==', NULL, NULL, NULL, 1, N'qwe123', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-03-16T09:16:41.803' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-03-18T10:20:17.357' AS DateTime))
INSERT [dbo].[biz_account] ([Id], [ModuleId], [UserLogonName], [UserDisplayName], [UserPassword], [Telephone], [MobilePhone], [Email], [IsEnabled], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'bd64eee6-f595-4ebf-a369-3e9b59cdf422', N'81762d87-d4c5-483f-937f-4c0e77b534b5', N'XYZ', N'XYZ', N'oG1zbi0KVFun7XfTXxzlJw==', NULL, NULL, NULL, 1, N'!QAZ2wsx', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-03-15T09:34:04.297' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-03-18T10:20:51.540' AS DateTime))
GO
INSERT [dbo].[biz_user] ([Id], [UserLogonName], [UserDisplayName], [UserPassword], [Telephone], [MobilePhone], [Email], [IsEnabled], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'540b304d-6b23-4b60-9a58-27f4e8c437fc', N'ys_test', N'伊莎测试', N'kcAVKc+BvoWtRUcIwXZ72w==', NULL, N'13788889999', NULL, 1, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-09T16:29:28.350' AS DateTime), N'540b304d-6b23-4b60-9a58-27f4e8c437fc', CAST(N'2022-03-17T12:21:32.173' AS DateTime))
INSERT [dbo].[biz_user] ([Id], [UserLogonName], [UserDisplayName], [UserPassword], [Telephone], [MobilePhone], [Email], [IsEnabled], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'e4994527-16de-4a2e-8d07-74a5a1f1ed3e', N'ry_test', N'若以测试', N'kcAVKc+BvoWtRUcIwXZ72w==', N'010-99987777', N'13566666666', N'23@qq.com', 1, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-08T16:41:01.720' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-09T16:28:27.240' AS DateTime))
INSERT [dbo].[biz_user] ([Id], [UserLogonName], [UserDisplayName], [UserPassword], [Telephone], [MobilePhone], [Email], [IsEnabled], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'225d9edb-1817-4890-8f3a-9c8f78eed3fe', N'ry_lisi1', N'李四', N'kcAVKc+BvoWtRUcIwXZ72w==', N'010-55558889', N'13555556667', N'456@abc.com', 1, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-17T15:52:24.800' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-17T15:52:24.800' AS DateTime))
INSERT [dbo].[biz_user] ([Id], [UserLogonName], [UserDisplayName], [UserPassword], [Telephone], [MobilePhone], [Email], [IsEnabled], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'8061f56f-03e1-4df6-b2ce-aface3b29871', N'ry_李四', N'李四', N'kcAVKc+BvoWtRUcIwXZ72w==', N'010-55558889', N'13555556667', N'456@abc.com', 1, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-17T15:46:33.943' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T09:39:56.943' AS DateTime))
INSERT [dbo].[biz_user] ([Id], [UserLogonName], [UserDisplayName], [UserPassword], [Telephone], [MobilePhone], [Email], [IsEnabled], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'441ca29e-0294-4ff5-92f4-df65a9996ee1', N'ry_zhangsan1', N'张三', N'kcAVKc+BvoWtRUcIwXZ72w==', N'010-55558888', N'13555556666', N'123@abc.com', 1, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-17T15:52:24.783' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-17T15:52:24.783' AS DateTime))
INSERT [dbo].[biz_user] ([Id], [UserLogonName], [UserDisplayName], [UserPassword], [Telephone], [MobilePhone], [Email], [IsEnabled], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'a6e1a97b-772b-4a76-83a4-e55284c6daba', N'ry_zhangsan', N'张三', N'kcAVKc+BvoWtRUcIwXZ72w==', N'010-55558888', N'13555556666', N'123@abc.com', 1, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-17T15:46:33.923' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T09:40:08.543' AS DateTime))
INSERT [dbo].[biz_user] ([Id], [UserLogonName], [UserDisplayName], [UserPassword], [Telephone], [MobilePhone], [Email], [IsEnabled], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'79971bba-0c16-4df8-bd30-e732cda8ae58', N'ry_admin', N'若以测试账号', N'kcAVKc+BvoWtRUcIwXZ72w==', NULL, NULL, NULL, 1, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-08T16:13:24.863' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-10T15:21:05.287' AS DateTime))
GO
INSERT [dbo].[biz_user_module] ([Id], [UserId], [ModuleId], [UserModuleLogonName], [UserModulePassword], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'2290effd-f427-45de-a0b6-06173e382f13', N'8061f56f-03e1-4df6-b2ce-aface3b29871', N'81762d87-d4c5-483f-937f-4c0e77b534b5', N'李四', N'qwertyuiop!@#$6', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-17T15:46:33.950' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T09:39:56.993' AS DateTime))
INSERT [dbo].[biz_user_module] ([Id], [UserId], [ModuleId], [UserModuleLogonName], [UserModulePassword], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'4ca173b0-1c73-46a6-9874-070ca7694ec0', N'225d9edb-1817-4890-8f3a-9c8f78eed3fe', N'4a6798ec-0296-4516-9618-bd66cb77fa53', N'lisi1', N'lisi1lisi1lisi1', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T15:25:25.167' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-22T09:24:36.947' AS DateTime))
INSERT [dbo].[biz_user_module] ([Id], [UserId], [ModuleId], [UserModuleLogonName], [UserModulePassword], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'31ddfd44-e67a-4620-906d-23524fee803f', N'540b304d-6b23-4b60-9a58-27f4e8c437fc', N'81762d87-d4c5-483f-937f-4c0e77b534b5', N'test', N'test', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T13:46:18.830' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T13:46:18.830' AS DateTime))
INSERT [dbo].[biz_user_module] ([Id], [UserId], [ModuleId], [UserModuleLogonName], [UserModulePassword], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'a19618d6-3ae5-44ec-82f4-27f354e28897', N'441ca29e-0294-4ff5-92f4-df65a9996ee1', N'4a6798ec-0296-4516-9618-bd66cb77fa53', N'qq', N'qq', NULL, 0, N'45aeff89-2867-43e7-9b40-296233c069c5', CAST(N'2022-02-24T14:16:54.717' AS DateTime), N'45aeff89-2867-43e7-9b40-296233c069c5', CAST(N'2022-02-24T14:16:54.717' AS DateTime))
INSERT [dbo].[biz_user_module] ([Id], [UserId], [ModuleId], [UserModuleLogonName], [UserModulePassword], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'9cf1a024-ba9e-4b05-99e0-4842e37fcf36', N'225d9edb-1817-4890-8f3a-9c8f78eed3fe', N'4a6798ec-0296-4516-9618-bd66cb77fa53', N'lisi1', N'lisi1lisi1lisi1lisi1', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T09:38:53.457' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T09:40:56.553' AS DateTime))
INSERT [dbo].[biz_user_module] ([Id], [UserId], [ModuleId], [UserModuleLogonName], [UserModulePassword], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'da0b6394-e193-46df-89f7-5941e17740dc', N'225d9edb-1817-4890-8f3a-9c8f78eed3fe', N'81762d87-d4c5-483f-937f-4c0e77b534b5', N'lisi1', N'lisi1lisi1', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T15:22:15.450' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T15:24:59.800' AS DateTime))
INSERT [dbo].[biz_user_module] ([Id], [UserId], [ModuleId], [UserModuleLogonName], [UserModulePassword], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'ad430034-bc8f-4e87-aeb1-68c1ef780864', N'540b304d-6b23-4b60-9a58-27f4e8c437fc', N'4a6798ec-0296-4516-9618-bd66cb77fa53', N'test', N'test1234', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-09T16:29:28.367' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-09T16:29:28.367' AS DateTime))
INSERT [dbo].[biz_user_module] ([Id], [UserId], [ModuleId], [UserModuleLogonName], [UserModulePassword], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'd2245564-6e43-4aad-8dd7-aac2db983cca', N'225d9edb-1817-4890-8f3a-9c8f78eed3fe', N'81762d87-d4c5-483f-937f-4c0e77b534b5', N'lisi1', N'lisi1lisi1lisi1', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T15:25:55.447' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-22T09:24:32.100' AS DateTime))
INSERT [dbo].[biz_user_module] ([Id], [UserId], [ModuleId], [UserModuleLogonName], [UserModulePassword], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'fdb6eaae-12f7-4fa6-991b-ae1727b44f7a', N'79971bba-0c16-4df8-bd30-e732cda8ae58', N'81762d87-d4c5-483f-937f-4c0e77b534b5', N'admin', N'kcAVKc+BvoWtRUcIwXZ72w==', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-08T16:13:24.877' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-10T15:21:05.333' AS DateTime))
INSERT [dbo].[biz_user_module] ([Id], [UserId], [ModuleId], [UserModuleLogonName], [UserModulePassword], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'eed681b9-aed5-4431-899d-de5b2043ac93', N'225d9edb-1817-4890-8f3a-9c8f78eed3fe', N'81762d87-d4c5-483f-937f-4c0e77b534b5', N'lisi1', N'qwertyuiop!@#$6', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-17T15:52:24.807' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T09:40:37.403' AS DateTime))
INSERT [dbo].[biz_user_module] ([Id], [UserId], [ModuleId], [UserModuleLogonName], [UserModulePassword], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'5230f576-e558-4ef9-996a-dfcd91aca8cb', N'441ca29e-0294-4ff5-92f4-df65a9996ee1', N'81762d87-d4c5-483f-937f-4c0e77b534b5', N'zhangsan1', N'qwertyuiop!@#$5', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-17T15:52:24.793' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-17T15:52:24.793' AS DateTime))
INSERT [dbo].[biz_user_module] ([Id], [UserId], [ModuleId], [UserModuleLogonName], [UserModulePassword], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'2daa327d-1de2-4fd9-b12b-f1e67aa56d0e', N'e4994527-16de-4a2e-8d07-74a5a1f1ed3e', N'81762d87-d4c5-483f-937f-4c0e77b534b5', N'test', N'test1234', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-08T16:41:01.750' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-09T16:28:27.257' AS DateTime))
INSERT [dbo].[biz_user_module] ([Id], [UserId], [ModuleId], [UserModuleLogonName], [UserModulePassword], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'bb643c2c-198e-41b4-944c-f31d0ee5d2af', N'a6e1a97b-772b-4a76-83a4-e55284c6daba', N'81762d87-d4c5-483f-937f-4c0e77b534b5', N'zhangsan', N'qwertyuiop!@#$5', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-17T15:46:33.937' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-22T09:24:50.603' AS DateTime))
GO
INSERT [dbo].[sys_code_table] ([Id], [CodeName], [Code], [Value], [ParentId], [Remark], [SerialNumber], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'918a2974-6a77-444f-b5a6-039f01a77a88', N'董事长', N'DSZ', N'DSZ', N'a2a26c79-9483-4204-b209-d3b3c53b989b', N'董事长', 0, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T14:42:53.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-20T13:55:05.157' AS DateTime))
INSERT [dbo].[sys_code_table] ([Id], [CodeName], [Code], [Value], [ParentId], [Remark], [SerialNumber], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'342e0919-3116-466e-8665-042fb4543df7', N'是否判断', N'YesOrNo', N'', NULL, N'是否判断', 1, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T11:28:14.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-08T15:46:45.770' AS DateTime))
INSERT [dbo].[sys_code_table] ([Id], [CodeName], [Code], [Value], [ParentId], [Remark], [SerialNumber], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'c4435185-6779-452f-85ae-14b1251a2920', N'总经理', N'ZJL', N'ZJL', N'a2a26c79-9483-4204-b209-d3b3c53b989b', N'总经理', 2, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T14:43:20.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T14:44:09.000' AS DateTime))
INSERT [dbo].[sys_code_table] ([Id], [CodeName], [Code], [Value], [ParentId], [Remark], [SerialNumber], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'3ea32b1c-a94c-4524-833d-2b21bf97feb6', N'纪委书记', N'JWSJ', N'JWSJ', N'a2a26c79-9483-4204-b209-d3b3c53b989b', N'纪委书记', 1, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-20T13:57:50.670' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-20T13:57:50.670' AS DateTime))
INSERT [dbo].[sys_code_table] ([Id], [CodeName], [Code], [Value], [ParentId], [Remark], [SerialNumber], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'7f6223be-740f-466d-9327-46405dfa9022', N'男', N'Male', N'0', N'5c6adfba-855d-436b-bb31-cdb2b8c77e66', N'男', 0, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T11:55:25.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:30:01.000' AS DateTime))
INSERT [dbo].[sys_code_table] ([Id], [CodeName], [Code], [Value], [ParentId], [Remark], [SerialNumber], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'dd12c0c2-768e-42da-b47f-8b386e591cae', N'否', N'No', N'0', N'342e0919-3116-466e-8665-042fb4543df7', N'否', 1, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T12:00:48.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T12:00:48.000' AS DateTime))
INSERT [dbo].[sys_code_table] ([Id], [CodeName], [Code], [Value], [ParentId], [Remark], [SerialNumber], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'9847eaa8-12fc-48cd-84ce-8dca31a7b813', N'书记', N'SJ', N'SJ', N'a2a26c79-9483-4204-b209-d3b3c53b989b', N'书记', 1, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-20T13:48:01.013' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-20T13:55:08.877' AS DateTime))
INSERT [dbo].[sys_code_table] ([Id], [CodeName], [Code], [Value], [ParentId], [Remark], [SerialNumber], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'6f0e90ff-4b15-4d17-8939-9523c83b3e80', N'普通员工', N'YG', N'YG', N'a2a26c79-9483-4204-b209-d3b3c53b989b', N'普通员工', 4, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T15:47:21.767' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:25:32.207' AS DateTime))
INSERT [dbo].[sys_code_table] ([Id], [CodeName], [Code], [Value], [ParentId], [Remark], [SerialNumber], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'5ba22c2c-cd8c-4a43-a88a-a3a721756f4b', N'女', N'Female', N'1', N'5c6adfba-855d-436b-bb31-cdb2b8c77e66', N'女', 1, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T11:55:45.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T11:59:05.000' AS DateTime))
INSERT [dbo].[sys_code_table] ([Id], [CodeName], [Code], [Value], [ParentId], [Remark], [SerialNumber], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'5c6adfba-855d-436b-bb31-cdb2b8c77e66', N'性别', N'Sex', NULL, NULL, N'性别', 2, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T11:28:38.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-01-21T15:28:17.680' AS DateTime))
INSERT [dbo].[sys_code_table] ([Id], [CodeName], [Code], [Value], [ParentId], [Remark], [SerialNumber], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'221c849a-4943-458c-86f4-d301996228cc', N'第三性别', N'SexOther', N'2', N'5c6adfba-855d-436b-bb31-cdb2b8c77e66', N'第三性别', 2, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T11:56:12.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T15:48:22.527' AS DateTime))
INSERT [dbo].[sys_code_table] ([Id], [CodeName], [Code], [Value], [ParentId], [Remark], [SerialNumber], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'a2a26c79-9483-4204-b209-d3b3c53b989b', N'岗位', N'Position', NULL, NULL, N'岗位', 3, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T14:29:07.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-06-11T14:12:17.000' AS DateTime))
INSERT [dbo].[sys_code_table] ([Id], [CodeName], [Code], [Value], [ParentId], [Remark], [SerialNumber], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'dc7a3882-fb25-4ba9-a30b-f9256f58e9c0', N'是', N'Yes', N'1', N'342e0919-3116-466e-8665-042fb4543df7', N'是', 0, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T12:00:03.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T12:00:03.000' AS DateTime))
INSERT [dbo].[sys_code_table] ([Id], [CodeName], [Code], [Value], [ParentId], [Remark], [SerialNumber], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'6e175e30-67a7-4d7d-9d67-fd9e4e7c47d7', N'经理', N'JL', N'JL', N'a2a26c79-9483-4204-b209-d3b3c53b989b', N'经理', 3, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T14:43:59.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T14:43:59.000' AS DateTime))
GO
INSERT [dbo].[sys_import_config] ([Id], [ConfigName], [StartRow], [StartColumn], [WorkSheetIndexes], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'21a45ec5-0758-4ad4-ad5a-31f9cf635f61', N'BizUserImportConfig', 1, 1, NULL, 3, N'业务用户导入配置', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-16T10:06:30.640' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-16T10:07:11.053' AS DateTime))
INSERT [dbo].[sys_import_config] ([Id], [ConfigName], [StartRow], [StartColumn], [WorkSheetIndexes], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'51510173-0253-48a8-bdcb-4cb4827c5d08', N'RoleImportConfig', 1, 1, NULL, 2, N'角色导入配置', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-05-07T14:36:28.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-08T15:49:05.533' AS DateTime))
INSERT [dbo].[sys_import_config] ([Id], [ConfigName], [StartRow], [StartColumn], [WorkSheetIndexes], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'e28c1ee6-1c16-405f-ac6c-a83c5923ea32', N'UserImportConfig', 1, 1, NULL, 4, N'系统用户导入配置', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T10:48:36.917' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T10:48:36.917' AS DateTime))
INSERT [dbo].[sys_import_config] ([Id], [ConfigName], [StartRow], [StartColumn], [WorkSheetIndexes], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'ba6070b3-a815-4efb-ac1f-c804502544cf', N'CommonConfigTest', 2, 1, N'', 1, N'综合常规性校验', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T16:11:34.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-01-21T15:28:29.480' AS DateTime))
GO
INSERT [dbo].[sys_import_config_detail] ([Id], [ParentId], [DataType], [Cells], [Required], [MaxValue], [MinValue], [DecimalLimit], [TextEnum], [Extend1], [Extend2], [Extend3], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'881d73b4-394a-4e49-a985-0c6fa6a59a95', N'ba6070b3-a815-4efb-ac1f-c804502544cf', 3, N'9', 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-05-12T13:54:27.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-05-13T10:02:25.000' AS DateTime))
INSERT [dbo].[sys_import_config_detail] ([Id], [ParentId], [DataType], [Cells], [Required], [MaxValue], [MinValue], [DecimalLimit], [TextEnum], [Extend1], [Extend2], [Extend3], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'6a31efa0-12f1-480c-878c-1036966796d3', N'ba6070b3-a815-4efb-ac1f-c804502544cf', 0, N'8', 1, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-05-05T09:08:48.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-05-05T09:11:20.000' AS DateTime))
INSERT [dbo].[sys_import_config_detail] ([Id], [ParentId], [DataType], [Cells], [Required], [MaxValue], [MinValue], [DecimalLimit], [TextEnum], [Extend1], [Extend2], [Extend3], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'c351de82-9888-4e8c-82ca-16dfd9cfe721', N'ba6070b3-a815-4efb-ac1f-c804502544cf', 1, N'2', 1, 100, 0, 0, N'', NULL, NULL, NULL, NULL, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-05-05T08:53:20.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-05-12T16:19:50.000' AS DateTime))
INSERT [dbo].[sys_import_config_detail] ([Id], [ParentId], [DataType], [Cells], [Required], [MaxValue], [MinValue], [DecimalLimit], [TextEnum], [Extend1], [Extend2], [Extend3], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'211b1c6a-91ac-4c60-8061-183d939e8b94', N'ba6070b3-a815-4efb-ac1f-c804502544cf', 0, N'6', 0, 100, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-05-12T13:53:52.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-05-13T10:02:36.000' AS DateTime))
INSERT [dbo].[sys_import_config_detail] ([Id], [ParentId], [DataType], [Cells], [Required], [MaxValue], [MinValue], [DecimalLimit], [TextEnum], [Extend1], [Extend2], [Extend3], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'c107b3c5-4713-4f9f-8b0f-198d7ffe3a6f', N'51510173-0253-48a8-bdcb-4cb4827c5d08', 1, N'2', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-23T14:13:00.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-08T15:50:03.047' AS DateTime))
INSERT [dbo].[sys_import_config_detail] ([Id], [ParentId], [DataType], [Cells], [Required], [MaxValue], [MinValue], [DecimalLimit], [TextEnum], [Extend1], [Extend2], [Extend3], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'd405b74e-d52a-4f9e-b806-1c1d0bcaba40', N'e28c1ee6-1c16-405f-ac6c-a83c5923ea32', 2, N'4', NULL, NULL, NULL, NULL, NULL, N'^(\\d{3,4}-)?\\d{6,8}$', NULL, NULL, NULL, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T10:50:44.887' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T10:50:44.887' AS DateTime))
INSERT [dbo].[sys_import_config_detail] ([Id], [ParentId], [DataType], [Cells], [Required], [MaxValue], [MinValue], [DecimalLimit], [TextEnum], [Extend1], [Extend2], [Extend3], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'14eaa1cd-effc-494f-b6dc-261964852cb9', N'e28c1ee6-1c16-405f-ac6c-a83c5923ea32', 2, N'1,2', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T10:49:27.677' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T10:49:27.677' AS DateTime))
INSERT [dbo].[sys_import_config_detail] ([Id], [ParentId], [DataType], [Cells], [Required], [MaxValue], [MinValue], [DecimalLimit], [TextEnum], [Extend1], [Extend2], [Extend3], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'9c67a664-66c7-4d39-bda8-2f9b074cb0ab', N'ba6070b3-a815-4efb-ac1f-c804502544cf', 1, N'7', 0, 100, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-05-12T13:54:09.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-05-13T10:02:32.000' AS DateTime))
INSERT [dbo].[sys_import_config_detail] ([Id], [ParentId], [DataType], [Cells], [Required], [MaxValue], [MinValue], [DecimalLimit], [TextEnum], [Extend1], [Extend2], [Extend3], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'c4c349d5-8e82-4e2d-8d5f-36b2535de492', N'ba6070b3-a815-4efb-ac1f-c804502544cf', 2, N'8', 0, 0, 0, 0, N'男,女', NULL, NULL, NULL, NULL, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-05-12T13:54:18.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-06-11T14:14:07.000' AS DateTime))
INSERT [dbo].[sys_import_config_detail] ([Id], [ParentId], [DataType], [Cells], [Required], [MaxValue], [MinValue], [DecimalLimit], [TextEnum], [Extend1], [Extend2], [Extend3], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'762ae51f-b973-49d2-8fcc-502aa28ec978', N'e28c1ee6-1c16-405f-ac6c-a83c5923ea32', 2, N'5', NULL, NULL, NULL, NULL, NULL, N'^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$', NULL, NULL, NULL, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T10:51:17.643' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T10:51:17.643' AS DateTime))
INSERT [dbo].[sys_import_config_detail] ([Id], [ParentId], [DataType], [Cells], [Required], [MaxValue], [MinValue], [DecimalLimit], [TextEnum], [Extend1], [Extend2], [Extend3], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'bc69a513-2544-4bf9-9843-68685c35d994', N'ba6070b3-a815-4efb-ac1f-c804502544cf', 2, N'8', 1, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-05-05T21:47:57.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-05-05T21:48:10.000' AS DateTime))
INSERT [dbo].[sys_import_config_detail] ([Id], [ParentId], [DataType], [Cells], [Required], [MaxValue], [MinValue], [DecimalLimit], [TextEnum], [Extend1], [Extend2], [Extend3], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'53d65856-21a4-40ec-98c1-6a85b5603e22', N'ba6070b3-a815-4efb-ac1f-c804502544cf', 3, N'4', 1, 33, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-05-05T09:08:18.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-05-12T13:53:15.000' AS DateTime))
INSERT [dbo].[sys_import_config_detail] ([Id], [ParentId], [DataType], [Cells], [Required], [MaxValue], [MinValue], [DecimalLimit], [TextEnum], [Extend1], [Extend2], [Extend3], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'f6e4400e-d695-406c-ae1d-853e59c8d75a', N'21a45ec5-0758-4ad4-ad5a-31f9cf635f61', 2, N'1,2,3', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-16T10:08:39.650' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-16T10:10:11.277' AS DateTime))
INSERT [dbo].[sys_import_config_detail] ([Id], [ParentId], [DataType], [Cells], [Required], [MaxValue], [MinValue], [DecimalLimit], [TextEnum], [Extend1], [Extend2], [Extend3], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'f14059ba-a95c-4b1f-881f-86eea84944f6', N'ba6070b3-a815-4efb-ac1f-c804502544cf', 4, N'5', 1, 90, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-05-05T09:08:35.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-05-12T13:53:27.000' AS DateTime))
INSERT [dbo].[sys_import_config_detail] ([Id], [ParentId], [DataType], [Cells], [Required], [MaxValue], [MinValue], [DecimalLimit], [TextEnum], [Extend1], [Extend2], [Extend3], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'59e91b35-be43-4fe1-baa7-9c602b93833a', N'21a45ec5-0758-4ad4-ad5a-31f9cf635f61', 2, N'4', NULL, NULL, NULL, NULL, NULL, N'^1\\d{10}$', NULL, NULL, NULL, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-16T10:50:15.257' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T10:49:43.687' AS DateTime))
INSERT [dbo].[sys_import_config_detail] ([Id], [ParentId], [DataType], [Cells], [Required], [MaxValue], [MinValue], [DecimalLimit], [TextEnum], [Extend1], [Extend2], [Extend3], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'a0177782-ee2c-4017-ac7c-a09ab50e7b12', N'21a45ec5-0758-4ad4-ad5a-31f9cf635f61', 2, N'6', NULL, NULL, NULL, NULL, NULL, N'^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$', NULL, NULL, NULL, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-16T11:09:40.740' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-17T16:57:26.193' AS DateTime))
INSERT [dbo].[sys_import_config_detail] ([Id], [ParentId], [DataType], [Cells], [Required], [MaxValue], [MinValue], [DecimalLimit], [TextEnum], [Extend1], [Extend2], [Extend3], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'4dc94136-4f5c-42ef-baab-a73cd49160fc', N'ba6070b3-a815-4efb-ac1f-c804502544cf', 4, N'10', 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-05-12T13:54:35.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-05-12T13:54:35.000' AS DateTime))
INSERT [dbo].[sys_import_config_detail] ([Id], [ParentId], [DataType], [Cells], [Required], [MaxValue], [MinValue], [DecimalLimit], [TextEnum], [Extend1], [Extend2], [Extend3], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'01ee9cc0-da0b-4635-8704-c0c42e1bcd23', N'21a45ec5-0758-4ad4-ad5a-31f9cf635f61', 2, N'5', NULL, NULL, NULL, NULL, NULL, N'^(\\d{3,4}-)?\\d{6,8}$', NULL, NULL, NULL, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-16T11:07:43.513' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-17T16:53:41.253' AS DateTime))
INSERT [dbo].[sys_import_config_detail] ([Id], [ParentId], [DataType], [Cells], [Required], [MaxValue], [MinValue], [DecimalLimit], [TextEnum], [Extend1], [Extend2], [Extend3], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'3cc6c7ea-1ee2-4885-ae1a-c9eb1ff12b89', N'51510173-0253-48a8-bdcb-4cb4827c5d08', 2, N'1', 1, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-23T14:12:48.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-23T15:24:17.000' AS DateTime))
INSERT [dbo].[sys_import_config_detail] ([Id], [ParentId], [DataType], [Cells], [Required], [MaxValue], [MinValue], [DecimalLimit], [TextEnum], [Extend1], [Extend2], [Extend3], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'721369f5-5f89-47e4-9b1e-dfd8a7795a41', N'ba6070b3-a815-4efb-ac1f-c804502544cf', 0, N'1', 1, 100, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-05-05T09:00:56.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-05-13T10:02:46.000' AS DateTime))
INSERT [dbo].[sys_import_config_detail] ([Id], [ParentId], [DataType], [Cells], [Required], [MaxValue], [MinValue], [DecimalLimit], [TextEnum], [Extend1], [Extend2], [Extend3], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'abc36e6e-ae05-41b1-86a4-e09981e05578', N'ba6070b3-a815-4efb-ac1f-c804502544cf', 2, N'3', 1, NULL, NULL, 0, N'男,女', NULL, NULL, NULL, NULL, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-05-05T09:06:36.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-24T09:28:17.210' AS DateTime))
INSERT [dbo].[sys_import_config_detail] ([Id], [ParentId], [DataType], [Cells], [Required], [MaxValue], [MinValue], [DecimalLimit], [TextEnum], [Extend1], [Extend2], [Extend3], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'e8a1a290-169a-4c9a-bed8-fffff884c1f0', N'e28c1ee6-1c16-405f-ac6c-a83c5923ea32', 2, N'3', NULL, NULL, NULL, NULL, NULL, N'^1\\d{10}$', NULL, NULL, NULL, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T10:50:17.617' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T10:50:17.617' AS DateTime))
GO
INSERT [dbo].[sys_language] ([Id], [LanguageName], [OrderNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'8306a39f-bfbd-430f-8315-15b6ebe3861d', N'en-US', 1, N'英国-美国', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T11:56:12.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T11:56:12.000' AS DateTime))
INSERT [dbo].[sys_language] ([Id], [LanguageName], [OrderNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'8223b5c4-c55a-4f02-98d2-ba7f320b0ea7', N'ru-RU', 2, N'俄国-俄国', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T11:56:12.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T11:56:12.000' AS DateTime))
INSERT [dbo].[sys_language] ([Id], [LanguageName], [OrderNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'9f06e722-5994-40f3-8bed-fe65c207e4d5', N'zh-CN', 0, N'华-中国', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T11:56:12.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T11:56:12.000' AS DateTime))
GO
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'b0652b6e-51ee-49ff-88b9-019bc8378f0b', N'/system', N'机构管理', N'/OrganizationManagement', N'aa0a8b78-5e35-46f5-be7e-2a55f20f6470', 1, 0, N'tree-table', N'', N'机构管理', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T16:05:25.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-01T16:51:56.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'45c8b9cf-5ac2-4e42-9cc8-051871a1d63f', NULL, N'查询列表', NULL, N'b0652b6e-51ee-49ff-88b9-019bc8378f0b', 1, 2, NULL, N'org:query:list', N'查询列表', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T17:51:13.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-06-11T14:11:55.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'd1bf1584-3c7a-4642-8cc6-095c42682e15', NULL, N'查询列表', NULL, N'ef3148eb-ebe6-4d9a-8806-aa783570290b', 1, 1, NULL, N'user:module:list', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-01-26T17:33:36.377' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-16T10:03:10.673' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'328734a7-3aab-424d-8fb5-101a30335e3c', N'/system', N'任务调度', N'/ScheduleJobManagement', N'aa0a8b78-5e35-46f5-be7e-2a55f20f6470', 9, 0, N'form', NULL, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-08-24T14:41:51.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-08-24T14:46:32.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'18cd9b4c-bfbd-442b-86dc-11eee193d886', NULL, N'分配权限', NULL, N'c80f26ab-b2bc-4766-a40e-17ed9417a5c7', 6, 1, NULL, N'role:grant:permission', N'分配权限', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T21:57:41.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-12T19:18:31.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'fb1f9114-e18c-4159-8af1-1408651f21fc', NULL, N'编辑模块', NULL, N'a81bb7aa-d7c2-41b9-b616-b46d3d7d2cf3', 3, 1, NULL, N'module:edit:entity', N'编辑模块', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-01-26T15:15:55.383' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-01-26T16:49:48.903' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'e2aaab2a-1b25-4e45-b54f-160bc6d332df', NULL, N'新增用户', NULL, N'e2363d77-45aa-4be1-b878-48d31aed728b', 2, 1, NULL, N'user:add:entity', N'新增用户', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:01:08.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:01:08.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'c80f26ab-b2bc-4766-a40e-17ed9417a5c7', N'/system', N'角色管理', N'/RoleManagement', N'aa0a8b78-5e35-46f5-be7e-2a55f20f6470', 4, 0, N'theme', NULL, N'角色管理菜单', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T16:10:47.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:24:48.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'60f51a50-7fb8-47df-bbee-20a4bb29d650', NULL, N'编辑角色', NULL, N'c80f26ab-b2bc-4766-a40e-17ed9417a5c7', 3, 1, NULL, N'role:edit:entity', N'编辑角色', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:17:47.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:17:47.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'f425487a-72a1-469d-8fa1-2334db69aba4', NULL, N'查询列表', NULL, N'a27a2518-53d8-4ed2-9655-3e1781d615dd', 1, 2, NULL, N'code:query:list', N'查询列表', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:24:37.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:24:37.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'837a330f-11bd-4a70-93b5-25af2c905408', NULL, N'关联菜单', NULL, N'c80f26ab-b2bc-4766-a40e-17ed9417a5c7', 5, 1, NULL, N'role:relate:menus', N'关联菜单', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T21:59:03.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T21:59:03.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'0f43194b-df86-4370-aeeb-26811cfab50c', NULL, N'删除菜单', NULL, N'cf7929b6-f9d9-4100-85f9-f63582583a61', 4, 1, NULL, N'menu:del:entities', N'删除菜单', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:07:09.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:07:09.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'fb35235e-b771-462e-b84b-269f3d8938ce', NULL, N'导入业务用户', NULL, N'ef3148eb-ebe6-4d9a-8806-aa783570290b', 5, 1, NULL, N'user:module:import', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-16T10:04:02.320' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-16T10:04:02.320' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'c277f700-ac70-40f4-a290-28eddefac820', NULL, N'查询列表', NULL, N'63f294fb-eea5-41cf-8343-b43d60fdd680', 1, 1, NULL, N'import:config:query:list', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:15:21.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:15:21.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'aa0a8b78-5e35-46f5-be7e-2a55f20f6470', N'/system', N'系统管理', N'', NULL, 1, 0, N'tree', NULL, N'系统管理目录', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T15:44:23.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-06-11T14:12:01.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'8d549066-fe5d-466c-b540-3525ebf30def', NULL, N'新增机构', NULL, N'b0652b6e-51ee-49ff-88b9-019bc8378f0b', 2, 1, NULL, N'org:add:entity', N'新增机构', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T17:55:41.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-18T18:12:54.410' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'ed4354fd-9611-41a9-abdc-383624db7a01', NULL, N'查询列表', NULL, N'be4b8341-f1a7-473b-bad7-5b4b41732e49', 1, 1, NULL, N'user:query:onlineUsers', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-06-09T15:17:08.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-06-09T15:17:08.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'de435288-c697-4072-8215-394dc7f80ca6', NULL, N'查询列表', NULL, N'e2363d77-45aa-4be1-b878-48d31aed728b', 1, 2, NULL, N'user:query:list', N'查询列表', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:00:41.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:00:41.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'a27a2518-53d8-4ed2-9655-3e1781d615dd', N'/system', N'数据字典', N'/CodeTableManagement', N'aa0a8b78-5e35-46f5-be7e-2a55f20f6470', 5, 0, N'table', NULL, N'数据字典管理菜单', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T16:11:47.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:17:19.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'5be9a12e-1c91-40bb-be8e-3f38da256c19', NULL, N'编辑菜单', NULL, N'cf7929b6-f9d9-4100-85f9-f63582583a61', 3, 1, NULL, N'menu:edit:entity', N'编辑菜单', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:06:33.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:06:33.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'e2bd72c3-b926-4072-b7a9-429cb77fe351', NULL, N'模块访问授权', NULL, N'ef3148eb-ebe6-4d9a-8806-aa783570290b', 6, 1, NULL, N'user:module:grant', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-16T10:04:44.110' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-16T10:04:44.110' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'45e0f73e-4c77-4719-b76d-482de9a0afa6', NULL, N'新增配置', NULL, N'63f294fb-eea5-41cf-8343-b43d60fdd680', 2, 1, NULL, N'import:config:add:entity', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:15:50.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:15:50.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'e2363d77-45aa-4be1-b878-48d31aed728b', N'/system', N'用户管理', N'/UserManagement', N'aa0a8b78-5e35-46f5-be7e-2a55f20f6470', 2, 0, N'user', NULL, N'用户管理菜单', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T16:05:01.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-01T17:13:02.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'a63b6939-563f-41a2-b526-525560069e3b', NULL, N'查询列表', NULL, N'328734a7-3aab-424d-8fb5-101a30335e3c', 0, 1, NULL, N'job:query:list', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-08-24T14:42:36.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-08-24T14:42:36.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'c1dc4140-ef7f-491e-a559-52614b581930', NULL, N'编辑业务用户', NULL, N'ef3148eb-ebe6-4d9a-8806-aa783570290b', 3, 1, NULL, N'user:module:edit', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-07T16:23:41.207' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-16T10:03:28.320' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'af62fa5d-4808-4a59-9297-58b0df442fff', NULL, N'新增模块', NULL, N'a81bb7aa-d7c2-41b9-b616-b46d3d7d2cf3', 2, 1, NULL, N'module:add:entity', N'新增模块', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-01-26T15:15:13.683' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-01-26T16:49:55.247' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'2923ffda-b4dc-4d92-9451-59a977a7edcb', NULL, N'查询实体', NULL, N'9321de30-10c8-4709-aff3-bba47312693f', 2, 1, NULL, N'log:query:entity', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-13T16:41:09.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-13T16:41:13.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'be4b8341-f1a7-473b-bad7-5b4b41732e49', N'/system', N'在线管理', N'/OnlineUserManagement', N'aa0a8b78-5e35-46f5-be7e-2a55f20f6470', 8, 0, N'form', NULL, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-06-09T15:16:25.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-06-09T15:18:22.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'206bb7dc-101a-4d76-89fc-606cdd247355', NULL, N'查询用户列表', NULL, N'03237dc0-d26f-4611-8fe1-ba08ac1da7d8', 1, 1, NULL, N'user:nonmodule:list', N'查询用户列表', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T15:10:38.433' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T15:10:38.433' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'c13577ae-a6aa-41eb-b1f2-62af176891c4', NULL, N'编辑任务', NULL, N'328734a7-3aab-424d-8fb5-101a30335e3c', 2, 1, NULL, N'job:edit:entity', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-08-24T14:43:48.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-08-24T14:43:48.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'a796138d-4c0b-435c-bb5f-67f871870130', N'/system', N'系统WebAPI', N'/SystemWebAPI', N'aa0a8b78-5e35-46f5-be7e-2a55f20f6470', 12, 0, N'form', NULL, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-31T17:02:55.050' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-31T17:08:54.180' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'b3b8f210-c86e-422c-8434-6d9e13bcbaba', NULL, N'查询信息', NULL, N'f34914ab-50f3-4acf-b844-9cfc70834896', 1, 1, NULL, N'query:hardware:info', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-10T16:38:41.703' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-10T16:40:22.610' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'0745a4ca-fb04-48d0-8ac5-7114acd15deb', NULL, N'编辑同步账号', NULL, N'1bb376e1-c420-4bd4-ab39-75f569095ea4', 3, 1, NULL, N'account:edit:entity', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-03-15T09:09:31.573' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-03-15T09:09:31.573' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'1bb376e1-c420-4bd4-ab39-75f569095ea4', N'/businessModule', N'同步账号管理', N'/BizAccount', N'cc21d837-43ed-49c1-bc5f-9bb647c1fd14', 4, 0, N'list', NULL, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-03-15T09:07:20.130' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-03-15T09:07:20.130' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'eae2b1cf-26e5-4086-ae44-7f7f3c8a50d4', NULL, N'删除配置', NULL, N'63f294fb-eea5-41cf-8343-b43d60fdd680', 4, 1, NULL, N'import:config:del:entities', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:16:51.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:16:51.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'4ef0101a-9911-47d0-8555-841ce928903f', NULL, N'编辑用户', NULL, N'e2363d77-45aa-4be1-b878-48d31aed728b', 3, 1, NULL, N'user:edit:entity', N'编辑用户', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:01:32.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:01:32.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'1553af7d-5749-4894-8c3e-84cb3cf2f2dd', NULL, N'删除业务用户', NULL, N'ef3148eb-ebe6-4d9a-8806-aa783570290b', 4, 1, NULL, N'user:module:del', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-07T16:24:16.373' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-16T10:03:38.277' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'8ef1c60e-d626-4a01-9bb2-8e609c0b40fc', NULL, N'启动任务', NULL, N'328734a7-3aab-424d-8fb5-101a30335e3c', 4, 1, NULL, N'schedule:job:add', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-08-24T14:44:40.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-08-24T14:44:40.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'371362df-0b05-4eee-9930-9288b6c8cc45', NULL, N'删除用户', NULL, N'e2363d77-45aa-4be1-b878-48d31aed728b', 4, 1, NULL, N'user:del:entities', N'删除用户', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:02:15.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:02:15.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'cc21d837-43ed-49c1-bc5f-9bb647c1fd14', N'/businessModule', N'统一认证', N'', NULL, 2, 0, N'table', NULL, N'统一认证目录', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-01-26T15:07:54.147' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-01-26T15:08:10.363' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'f34914ab-50f3-4acf-b844-9cfc70834896', N'/system', N'服务器监控', N'/ServerHardwareMonitor', N'aa0a8b78-5e35-46f5-be7e-2a55f20f6470', 11, 0, N'form', NULL, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-10T16:37:41.453' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-10T16:39:23.403' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'8169ab55-b24e-45b5-99f1-9d65d55af215', NULL, N'查询列表', NULL, N'a81bb7aa-d7c2-41b9-b616-b46d3d7d2cf3', 1, 1, NULL, N'biz:module:list', N'查询列表', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-01-26T15:14:32.993' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-01-26T16:49:51.847' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'4c4126b1-8c20-44a5-a2cf-9ea19e582af9', NULL, N'数据导出', NULL, N'c80f26ab-b2bc-4766-a40e-17ed9417a5c7', 8, 1, NULL, N'role:list:export', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-28T18:04:02.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-28T18:04:02.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'0d12b992-92df-4961-a50b-a2aac55da1e2', NULL, N'查询列表', NULL, N'9321de30-10c8-4709-aff3-bba47312693f', 1, 2, NULL, N'log:query:list', N'查询列表', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:33:41.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:33:41.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'd25ea44a-4aba-47eb-b9ba-a42799d7a535', NULL, N'新增菜单', NULL, N'cf7929b6-f9d9-4100-85f9-f63582583a61', 2, 1, NULL, N'menu:add:entity', N'新增菜单', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:04:32.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:04:32.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'ef3148eb-ebe6-4d9a-8806-aa783570290b', N'/businessModule', N'授权管理', N'/BizUserModule', N'cc21d837-43ed-49c1-bc5f-9bb647c1fd14', 2, 0, N'list', NULL, N'授权管理', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-01-26T17:24:20.373' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-01-26T17:35:51.650' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'00d356a5-f24b-4fe9-8616-aa8d36c37be8', NULL, N'暂停任务', NULL, N'328734a7-3aab-424d-8fb5-101a30335e3c', 5, 1, NULL, N'schedule:job:pause', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-08-24T14:45:05.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-08-24T14:45:05.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'c3be9eb3-d1da-4af7-8820-aeaf0283cdec', NULL, N'查询列表', NULL, N'b25958ea-608d-436f-8e01-b8fcc101905d', 1, 1, NULL, N'language:query:list', N'', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-08-05T11:21:36.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:28:13.547' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'a4a59f63-f397-4850-ac4a-b06b41acfc34', NULL, N'权限下放', NULL, N'c80f26ab-b2bc-4766-a40e-17ed9417a5c7', 7, 1, NULL, N'role:delegate:permission', N'权限下放', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-11-11T14:27:50.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-11-11T14:27:50.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'9e02c48e-3b3f-4f24-8005-b13c63d437c0', N'/system', N'机构管理', NULL, N'aa0a8b78-5e35-46f5-be7e-2a55f20f6470', 20, 0, NULL, NULL, NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-01T16:53:04.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-01T17:12:00.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'a4416f30-7381-48d2-a074-b2e9a9481c3c', NULL, N'数据导入', NULL, N'c80f26ab-b2bc-4766-a40e-17ed9417a5c7', 9, 1, NULL, N'role:list:import', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-05-07T10:29:59.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-05-07T10:29:59.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'63f294fb-eea5-41cf-8343-b43d60fdd680', N'/system', N'导入配置', N'/ImportConfigManagement', N'aa0a8b78-5e35-46f5-be7e-2a55f20f6470', 7, 0, N'form', NULL, N'导入配置', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:10:12.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T16:51:36.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'0d214bcc-a7be-41c2-9bb5-b43e6e1224aa', NULL, N'查询WebAPI', NULL, N'a796138d-4c0b-435c-bb5f-67f871870130', 1, 1, NULL, N'query:webapi:info', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-31T17:14:35.470' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-31T17:14:35.470' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'a81bb7aa-d7c2-41b9-b616-b46d3d7d2cf3', N'/businessModule', N'模块管理', N'/BizModule', N'cc21d837-43ed-49c1-bc5f-9bb647c1fd14', 1, 0, N'list', NULL, N'模块管理', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-01-26T15:12:57.157' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-01-26T17:29:54.883' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'66469b10-a974-4abf-bd72-b7a205a09b47', NULL, N'删除模块', NULL, N'a81bb7aa-d7c2-41b9-b616-b46d3d7d2cf3', 4, 1, NULL, N'module:del:entity', N'删除模块', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-01-26T15:16:40.207' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-01-26T16:49:43.517' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'b25958ea-608d-436f-8e01-b8fcc101905d', N'/system', N'多语管理', N'/LanguageManagement', N'aa0a8b78-5e35-46f5-be7e-2a55f20f6470', 10, 0, N'form', NULL, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-08-05T11:20:24.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-08-24T14:41:20.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'03237dc0-d26f-4611-8fe1-ba08ac1da7d8', N'/businessModule', N'离态用户管理', N'/BizUserNonModule', N'cc21d837-43ed-49c1-bc5f-9bb647c1fd14', 3, 0, N'list', NULL, N'离态用户管理', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T15:09:22.407' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T15:09:22.407' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'300a8888-11af-4272-a638-baefc8e7858d', NULL, N'删除同步账号', NULL, N'1bb376e1-c420-4bd4-ab39-75f569095ea4', 4, 1, NULL, N'account:del:entities', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-03-15T09:10:01.527' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-03-15T09:10:01.527' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'9321de30-10c8-4709-aff3-bba47312693f', N'/system', N'审计日志', N'/LogManagement', N'aa0a8b78-5e35-46f5-be7e-2a55f20f6470', 6, 0, N'documentation', NULL, N'审计日志管理菜单', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T16:12:40.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:17:23.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'8ff6bcd2-8701-4f45-aea2-bbc78718c4fb', NULL, N'恢复任务', NULL, N'328734a7-3aab-424d-8fb5-101a30335e3c', 6, 1, NULL, N'schedule:job:resume', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-08-24T14:45:34.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-08-24T14:53:34.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'7ffd51d8-3475-4b82-a0e7-bd5e92e5f174', NULL, N'新增同步账号', NULL, N'1bb376e1-c420-4bd4-ab39-75f569095ea4', 2, 1, NULL, N'account:add:entity', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-03-15T09:08:56.160' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-03-15T09:08:56.160' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'21c94e72-6dde-40c0-bae8-c0ef0a1c476c', NULL, N'编辑配置', NULL, N'63f294fb-eea5-41cf-8343-b43d60fdd680', 3, 1, NULL, N'import:config:edit:entity', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:16:17.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:16:17.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'f21f1b48-b743-4e01-a5d7-c50ebfd2fe78', NULL, N'删除字典', NULL, N'a27a2518-53d8-4ed2-9655-3e1781d615dd', 4, 1, NULL, N'code:del:entities', N'删除字典', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:28:26.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-18T21:39:38.690' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'eed891c4-822d-41d4-ac80-c6b9c327a1af', NULL, N'离态用户授权', NULL, N'03237dc0-d26f-4611-8fe1-ba08ac1da7d8', 2, 1, NULL, N'user:nonmodule:grant', N'离态用户授权', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T15:11:32.160' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T15:11:32.160' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'd0ebd39e-9a82-42fa-986e-c7235747a758', NULL, N'查询列表', NULL, N'c80f26ab-b2bc-4766-a40e-17ed9417a5c7', 1, 2, NULL, N'role:query:list', N'查询列表', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:16:44.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:16:44.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'f6d5b324-ed69-4246-82cd-cb9b3f80efa6', NULL, N'导入用户', NULL, N'e2363d77-45aa-4be1-b878-48d31aed728b', 5, 1, NULL, N'user:import:entities', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T10:46:50.043' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T10:46:50.043' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'78c1335e-f05a-4431-a85b-cee182b446f3', NULL, N'新增业务用户', NULL, N'ef3148eb-ebe6-4d9a-8806-aa783570290b', 2, 1, NULL, N'user:module:add', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-07T16:22:56.673' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-16T10:03:20.887' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'0e989075-01cd-4c6e-a622-d336106bf255', NULL, N'新增角色', NULL, N'c80f26ab-b2bc-4766-a40e-17ed9417a5c7', 2, 1, NULL, N'role:add:entity', N'新增角色', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:17:12.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:17:12.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'bec177ff-23ce-449f-ade4-dd0e62142842', NULL, N'编辑机构', NULL, N'b0652b6e-51ee-49ff-88b9-019bc8378f0b', 3, 1, NULL, N'org:edit:entity', N'编辑机构', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T17:56:36.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T17:56:36.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'eb926dd8-63ba-49a4-8445-df2850f763c9', NULL, N'新增任务', NULL, N'328734a7-3aab-424d-8fb5-101a30335e3c', 1, 1, NULL, N'job:add:entity', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-08-24T14:43:18.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-08-24T14:43:18.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'b807f782-8b8c-49b6-a62d-e230dd44cd7e', NULL, N'删除角色', NULL, N'c80f26ab-b2bc-4766-a40e-17ed9417a5c7', 4, 1, NULL, N'role:del:entities', N'删除角色', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:18:23.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:18:23.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'26d0ed04-5670-4707-ad91-e2893edca2ed', NULL, N'删除机构', NULL, N'b0652b6e-51ee-49ff-88b9-019bc8378f0b', 4, 1, NULL, N'org:del:entities', N'删除机构', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T17:57:09.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-01-21T15:28:02.967' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'c27d9de3-78ae-4577-8bdd-e4eea63010af', NULL, N'查询列表', NULL, N'cf7929b6-f9d9-4100-85f9-f63582583a61', 1, 1, NULL, N'menu:query:list', N'查询列表', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:05:09.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:05:09.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'66d87039-4811-4ba8-87b4-e53ed0daf21f', NULL, N'查询同步账号', NULL, N'1bb376e1-c420-4bd4-ab39-75f569095ea4', 1, 1, NULL, N'account:query:list', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-03-15T09:08:26.200' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-03-15T09:11:04.157' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'13e73c0d-a285-43af-9167-eef89381d08e', NULL, N'新增字典', NULL, N'a27a2518-53d8-4ed2-9655-3e1781d615dd', 2, 1, NULL, N'code:add:entity', N'新增字典', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:25:32.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-18T20:26:46.287' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'cf7929b6-f9d9-4100-85f9-f63582583a61', N'/system', N'菜单管理', N'/MenuManagement', N'aa0a8b78-5e35-46f5-be7e-2a55f20f6470', 3, 0, N'list', NULL, N'菜单管理', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T16:10:01.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T16:10:01.000' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'2634a816-5dd9-4bfd-a573-f6b0020204db', NULL, N'编辑字典', NULL, N'a27a2518-53d8-4ed2-9655-3e1781d615dd', 3, 1, NULL, N'code:edit:entity', N'编辑字典', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T18:26:15.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-18T20:26:55.687' AS DateTime))
INSERT [dbo].[sys_menu] ([Id], [Path], [MenuName], [MenuUrl], [ParentId], [SerialNumber], [MenuType], [Icon], [Code], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'ccb42490-1389-40a2-86a0-ff77553bc740', NULL, N'删除任务', NULL, N'328734a7-3aab-424d-8fb5-101a30335e3c', 3, 1, NULL, N'job:del:entity', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-08-24T14:44:12.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-08-24T14:44:12.000' AS DateTime))
GO
INSERT [dbo].[sys_menu_language] ([Id], [MenuId], [LanguageId], [MenuName], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'ed30a015-6ceb-4592-aca5-00dd95774aa7', N'b0652b6e-51ee-49ff-88b9-019bc8378f0b', N'8306a39f-bfbd-430f-8315-15b6ebe3861d', N'OrganizationManagement', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-01T16:51:34.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-01T16:51:56.000' AS DateTime))
INSERT [dbo].[sys_menu_language] ([Id], [MenuId], [LanguageId], [MenuName], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'0bf54370-bd65-4a1c-8b6f-01fde552d168', N'b0652b6e-51ee-49ff-88b9-019bc8378f0b', N'8223b5c4-c55a-4f02-98d2-ba7f320b0ea7', N'OrgManage', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-01T16:51:56.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-01T16:51:56.000' AS DateTime))
INSERT [dbo].[sys_menu_language] ([Id], [MenuId], [LanguageId], [MenuName], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'319fd164-d116-441e-bfae-0f24da6db973', N'9e02c48e-3b3f-4f24-8005-b13c63d437c0', N'8306a39f-bfbd-430f-8315-15b6ebe3861d', N'abc', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-01T16:53:09.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-01T17:12:00.000' AS DateTime))
INSERT [dbo].[sys_menu_language] ([Id], [MenuId], [LanguageId], [MenuName], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'ea4c0742-28ac-48c2-a7f5-31f0508c9e4b', N'b0652b6e-51ee-49ff-88b9-019bc8378f0b', N'8306a39f-bfbd-430f-8315-15b6ebe3861d', N'OrganizationManagement', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-01T16:51:56.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-01T16:51:56.000' AS DateTime))
INSERT [dbo].[sys_menu_language] ([Id], [MenuId], [LanguageId], [MenuName], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'aabbc321-8f26-423f-89ed-46f5e8ed4ae0', N'b0652b6e-51ee-49ff-88b9-019bc8378f0b', N'8223b5c4-c55a-4f02-98d2-ba7f320b0ea7', N'OrgManage', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-01T16:51:28.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-01T16:51:35.000' AS DateTime))
INSERT [dbo].[sys_menu_language] ([Id], [MenuId], [LanguageId], [MenuName], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'3f4da931-b98c-4ad9-ace9-5e7fe157cd1f', N'9e02c48e-3b3f-4f24-8005-b13c63d437c0', N'8223b5c4-c55a-4f02-98d2-ba7f320b0ea7', N'ru', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-01T16:53:11.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-01T17:12:00.000' AS DateTime))
INSERT [dbo].[sys_menu_language] ([Id], [MenuId], [LanguageId], [MenuName], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'5fb0329f-ff3b-47d6-9075-64a389901aa1', N'a81bb7aa-d7c2-41b9-b616-b46d3d7d2cf3', N'8306a39f-bfbd-430f-8315-15b6ebe3861d', N'BizModuleManagement', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-01-26T15:12:57.173' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-01-26T16:49:58.133' AS DateTime))
INSERT [dbo].[sys_menu_language] ([Id], [MenuId], [LanguageId], [MenuName], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'6048abf8-ca30-45f4-90d8-7831eedb595e', N'e2363d77-45aa-4be1-b878-48d31aed728b', N'8306a39f-bfbd-430f-8315-15b6ebe3861d', N'UserManagement', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-01T17:13:02.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-01T17:13:02.000' AS DateTime))
INSERT [dbo].[sys_menu_language] ([Id], [MenuId], [LanguageId], [MenuName], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'7ee805a9-e045-429c-825f-990426c75a75', N'b0652b6e-51ee-49ff-88b9-019bc8378f0b', N'8223b5c4-c55a-4f02-98d2-ba7f320b0ea7', N'OrgManage', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-01T16:51:35.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-01T16:51:56.000' AS DateTime))
INSERT [dbo].[sys_menu_language] ([Id], [MenuId], [LanguageId], [MenuName], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'19b2fc8e-b374-43cc-8812-a056f2e87226', N'e2363d77-45aa-4be1-b878-48d31aed728b', N'8223b5c4-c55a-4f02-98d2-ba7f320b0ea7', N'UserManage', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-01T17:13:02.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-01T17:13:02.000' AS DateTime))
INSERT [dbo].[sys_menu_language] ([Id], [MenuId], [LanguageId], [MenuName], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'eee77f5f-b4bb-44fb-92d8-a493b143e6f0', N'a81bb7aa-d7c2-41b9-b616-b46d3d7d2cf3', N'8223b5c4-c55a-4f02-98d2-ba7f320b0ea7', N'BizModuleManagement', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-01-26T15:12:57.183' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-01-26T16:49:58.137' AS DateTime))
INSERT [dbo].[sys_menu_language] ([Id], [MenuId], [LanguageId], [MenuName], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'a08245cd-d3f0-41ba-a5a3-ed95853a4aa6', N'b0652b6e-51ee-49ff-88b9-019bc8378f0b', N'8306a39f-bfbd-430f-8315-15b6ebe3861d', N'OrganizationManagement', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-01T16:51:13.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-01T16:51:34.000' AS DateTime))
GO
INSERT [dbo].[sys_organization] ([Id], [OrgName], [ParentId], [Leader], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'7d664253-e505-4157-83b0-01bb86028d66', N'技术支持', N'5a948c7d-56ea-477d-850e-1386f1dcdc2c', NULL, 7, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T10:59:25.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T16:47:07.000' AS DateTime))
INSERT [dbo].[sys_organization] ([Id], [OrgName], [ParentId], [Leader], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'9756e90f-9505-4990-ac0e-135c58bc6f29', N'软件开发', N'5a948c7d-56ea-477d-850e-1386f1dcdc2c', N'b09bbb4b-a7f0-4f7a-8f3d-dbb895f3edc5', 1, N'二级组织', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-08T09:29:38.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-01-21T15:27:49.577' AS DateTime))
INSERT [dbo].[sys_organization] ([Id], [OrgName], [ParentId], [Leader], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'5a948c7d-56ea-477d-850e-1386f1dcdc2c', N'研发部', N'ab907b1e-13a3-4c04-85db-f560f9c8f9aa', NULL, 1, N'一级部门', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-08T09:14:41.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-03-11T14:31:57.020' AS DateTime))
INSERT [dbo].[sys_organization] ([Id], [OrgName], [ParentId], [Leader], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'7955d133-040a-4e36-94fe-89c9c4a547e5', N'售后部', N'ab907b1e-13a3-4c04-85db-f560f9c8f9aa', NULL, 3, N'一级部门', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-08T09:28:46.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-08T09:28:46.000' AS DateTime))
INSERT [dbo].[sys_organization] ([Id], [OrgName], [ParentId], [Leader], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'e4bcf807-6bd2-4aa5-ba6f-8ef85840fd65', N'营销部', N'ab907b1e-13a3-4c04-85db-f560f9c8f9aa', NULL, 3, N'一级部门', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-08T09:28:25.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-08T09:28:25.000' AS DateTime))
INSERT [dbo].[sys_organization] ([Id], [OrgName], [ParentId], [Leader], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'39b86e17-1b98-4898-a1c0-8faef7f66788', N'DBA', N'5a948c7d-56ea-477d-850e-1386f1dcdc2c', NULL, 6, N'', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-08T10:00:45.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T16:48:08.000' AS DateTime))
INSERT [dbo].[sys_organization] ([Id], [OrgName], [ParentId], [Leader], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'99b57766-c9f3-40ec-ac45-9d2cbe79ee08', N'需求分析', N'5a948c7d-56ea-477d-850e-1386f1dcdc2c', NULL, 4, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-08T10:00:13.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-06-11T14:11:21.000' AS DateTime))
INSERT [dbo].[sys_organization] ([Id], [OrgName], [ParentId], [Leader], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'66255038-acda-49a4-b260-ccafeedc6074', N'实施部', N'ab907b1e-13a3-4c04-85db-f560f9c8f9aa', NULL, 2, N'一级部门', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-08T09:27:35.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-08T09:27:48.000' AS DateTime))
INSERT [dbo].[sys_organization] ([Id], [OrgName], [ParentId], [Leader], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'504b3320-1b29-4493-98fd-e7d15f338569', N'UI设计', N'5a948c7d-56ea-477d-850e-1386f1dcdc2c', NULL, 5, N'', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-08T09:44:48.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-08T13:18:16.000' AS DateTime))
INSERT [dbo].[sys_organization] ([Id], [OrgName], [ParentId], [Leader], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'57126a11-842e-493d-bc4f-e8c18853e6ed', N'硬件开发', N'5a948c7d-56ea-477d-850e-1386f1dcdc2c', NULL, 2, N'二级组织', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-08T09:29:54.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-06-22T15:14:12.000' AS DateTime))
INSERT [dbo].[sys_organization] ([Id], [OrgName], [ParentId], [Leader], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'ab907b1e-13a3-4c04-85db-f560f9c8f9aa', N'如意Admin', NULL, NULL, 1, N'根本目录', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-03-14T12:38:56.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T16:51:12.000' AS DateTime))
INSERT [dbo].[sys_organization] ([Id], [OrgName], [ParentId], [Leader], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'8faffd3a-3bd2-4987-9cad-f658e83dc283', N'系统测试', N'5a948c7d-56ea-477d-850e-1386f1dcdc2c', NULL, 3, N'系统测试', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-08T09:31:17.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-08T15:30:36.103' AS DateTime))
GO
INSERT [dbo].[sys_user] ([Id], [LogonName], [DisplayName], [Password], [Telephone], [MobilePhone], [Email], [Remark], [IsSupperAdmin], [SerialNumber], [IsEnabled], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'00000000-0000-0000-0000-000000000000', N'admin', N'超级管理员', N'kcAVKc+BvoWtRUcIwXZ72w==', NULL, NULL, NULL, NULL, 1, 0, 1, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-09T11:46:43.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-09T11:46:43.000' AS DateTime))
INSERT [dbo].[sys_user] ([Id], [LogonName], [DisplayName], [Password], [Telephone], [MobilePhone], [Email], [Remark], [IsSupperAdmin], [SerialNumber], [IsEnabled], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'92329bbc-a81d-4da9-b286-1a8a6444456b', N'zhangsan', N'张三', N'kcAVKc+BvoWtRUcIwXZ72w==', N'010-55558888', N'13555556666', N'123@abc.com', NULL, 0, 0, 1, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T10:59:09.630' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T10:59:09.630' AS DateTime))
INSERT [dbo].[sys_user] ([Id], [LogonName], [DisplayName], [Password], [Telephone], [MobilePhone], [Email], [Remark], [IsSupperAdmin], [SerialNumber], [IsEnabled], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'45aeff89-2867-43e7-9b40-296233c069c5', N'ruyi', N'如意', N'kcAVKc+BvoWtRUcIwXZ72w==', N'010-55558888', N'13555556666', N'ruyi@qq.com', N'如意小朋友', 0, 1, 1, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-09T11:46:43.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-21T09:50:57.907' AS DateTime))
INSERT [dbo].[sys_user] ([Id], [LogonName], [DisplayName], [Password], [Telephone], [MobilePhone], [Email], [Remark], [IsSupperAdmin], [SerialNumber], [IsEnabled], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'6585aa62-9ac1-4799-863a-317e26da466a', N'xz', N'xz', N'kcAVKc+BvoWtRUcIwXZ72w==', NULL, NULL, NULL, N'xz', 0, 2, 1, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-21T09:53:11.770' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-22T09:05:43.070' AS DateTime))
INSERT [dbo].[sys_user] ([Id], [LogonName], [DisplayName], [Password], [Telephone], [MobilePhone], [Email], [Remark], [IsSupperAdmin], [SerialNumber], [IsEnabled], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'b464f199-3369-4dbd-ae7e-497224d5ea1d', N'zhangsan1', N'张三', N'kcAVKc+BvoWtRUcIwXZ72w==', N'010-55558888', N'13555556666', N'123@abc.com', NULL, 0, 0, 1, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T11:08:04.487' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T11:08:04.487' AS DateTime))
INSERT [dbo].[sys_user] ([Id], [LogonName], [DisplayName], [Password], [Telephone], [MobilePhone], [Email], [Remark], [IsSupperAdmin], [SerialNumber], [IsEnabled], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'1b8f558f-4a5b-43c6-bc14-4b0c1e83587f', N'xx', N'xx', N'kcAVKc+BvoWtRUcIwXZ72w==', NULL, NULL, NULL, NULL, 0, 0, 1, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T11:01:29.377' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-22T09:06:44.497' AS DateTime))
INSERT [dbo].[sys_user] ([Id], [LogonName], [DisplayName], [Password], [Telephone], [MobilePhone], [Email], [Remark], [IsSupperAdmin], [SerialNumber], [IsEnabled], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'930cc8e2-f1ae-404d-9258-6167a3acbcbc', N'shisih', N'实施', N'kcAVKc+BvoWtRUcIwXZ72w==', NULL, NULL, NULL, N'pp', 0, 1, 1, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T12:14:58.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-06-11T13:58:56.000' AS DateTime))
INSERT [dbo].[sys_user] ([Id], [LogonName], [DisplayName], [Password], [Telephone], [MobilePhone], [Email], [Remark], [IsSupperAdmin], [SerialNumber], [IsEnabled], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'31cf3eed-848a-40a4-9c9d-75b2bac01d90', N'zhangsan12', N'张三', N'kcAVKc+BvoWtRUcIwXZ72w==', N'010-55558888', N'13555556666', N'123@abc.com', NULL, 0, 0, 1, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T12:04:41.617' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T12:04:41.617' AS DateTime))
INSERT [dbo].[sys_user] ([Id], [LogonName], [DisplayName], [Password], [Telephone], [MobilePhone], [Email], [Remark], [IsSupperAdmin], [SerialNumber], [IsEnabled], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'bacc045e-eefc-4d45-9920-82192cbabb4e', N'8879', N'8879', N'kcAVKc+BvoWtRUcIwXZ72w==', NULL, NULL, NULL, NULL, 0, 0, 1, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T12:03:33.713' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T12:03:33.713' AS DateTime))
INSERT [dbo].[sys_user] ([Id], [LogonName], [DisplayName], [Password], [Telephone], [MobilePhone], [Email], [Remark], [IsSupperAdmin], [SerialNumber], [IsEnabled], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'44d83899-e4d8-4909-9eb7-969aa69a1ca8', N'lisi122', N'李四', N'kcAVKc+BvoWtRUcIwXZ72w==', N'010-55558889', N'13555556667', N'456@abc.com', NULL, 0, 0, 1, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T12:08:56.840' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T12:08:56.840' AS DateTime))
INSERT [dbo].[sys_user] ([Id], [LogonName], [DisplayName], [Password], [Telephone], [MobilePhone], [Email], [Remark], [IsSupperAdmin], [SerialNumber], [IsEnabled], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'a668aba7-b317-4468-a0fe-a2e024f0d303', N'lisi1', N'李四', N'kcAVKc+BvoWtRUcIwXZ72w==', N'010-55558889', N'13555556667', N'456@abc.com', NULL, 0, 0, 1, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T11:08:13.467' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T11:08:13.467' AS DateTime))
INSERT [dbo].[sys_user] ([Id], [LogonName], [DisplayName], [Password], [Telephone], [MobilePhone], [Email], [Remark], [IsSupperAdmin], [SerialNumber], [IsEnabled], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'24b7ea06-3519-4fd1-9cdc-a7bdbf17f4fd', N'lisi12', N'李四', N'kcAVKc+BvoWtRUcIwXZ72w==', N'010-55558889', N'13555556667', N'456@abc.com', NULL, 0, 0, 1, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T12:04:41.770' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-22T09:06:44.533' AS DateTime))
INSERT [dbo].[sys_user] ([Id], [LogonName], [DisplayName], [Password], [Telephone], [MobilePhone], [Email], [Remark], [IsSupperAdmin], [SerialNumber], [IsEnabled], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'2e24e235-cdb0-464a-9d97-c1d0e989819c', N'lisi', N'李四', N'kcAVKc+BvoWtRUcIwXZ72w==', N'010-55558889', N'13555556667', N'456@abc.com', NULL, 0, 0, 1, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T11:00:20.200' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-22T09:06:44.567' AS DateTime))
INSERT [dbo].[sys_user] ([Id], [LogonName], [DisplayName], [Password], [Telephone], [MobilePhone], [Email], [Remark], [IsSupperAdmin], [SerialNumber], [IsEnabled], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'0300ae60-86ac-4c4a-a701-c53d8eb00ca6', N'zhangsan122', N'张三', N'kcAVKc+BvoWtRUcIwXZ72w==', N'010-55558888', N'13555556666', N'123@abc.com', NULL, 0, 0, 1, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T12:08:56.733' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-22T09:06:36.970' AS DateTime))
INSERT [dbo].[sys_user] ([Id], [LogonName], [DisplayName], [Password], [Telephone], [MobilePhone], [Email], [Remark], [IsSupperAdmin], [SerialNumber], [IsEnabled], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'b09bbb4b-a7f0-4f7a-8f3d-dbb895f3edc5', N'test', N'测试账号', N'kcAVKc+BvoWtRUcIwXZ72w==', NULL, NULL, NULL, N'xy', 0, 1, 1, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T12:14:37.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-08T15:35:49.900' AS DateTime))
INSERT [dbo].[sys_user] ([Id], [LogonName], [DisplayName], [Password], [Telephone], [MobilePhone], [Email], [Remark], [IsSupperAdmin], [SerialNumber], [IsEnabled], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'6f415677-cc02-4efa-bfbd-fa7c1c2c561d', N'hello', N'你好', N'kcAVKc+BvoWtRUcIwXZ72w==', NULL, NULL, NULL, N'xx', 0, 1, 1, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-06-11T14:01:38.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-07-02T16:04:25.000' AS DateTime))
GO
INSERT [dbo].[sys_org_user] ([Id], [OrgId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'4bd496ec-a96c-4de0-a7e5-0e32e4084be7', N'9756e90f-9505-4990-ac0e-135c58bc6f29', N'2e24e235-cdb0-464a-9d97-c1d0e989819c', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T11:00:20.210' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-22T09:06:44.553' AS DateTime))
INSERT [dbo].[sys_org_user] ([Id], [OrgId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'2c9f5064-a72b-4705-9abe-292dd4a00dca', N'9756e90f-9505-4990-ac0e-135c58bc6f29', N'24b7ea06-3519-4fd1-9cdc-a7bdbf17f4fd', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T12:04:41.780' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-22T09:06:44.517' AS DateTime))
INSERT [dbo].[sys_org_user] ([Id], [OrgId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'abd730cf-e42f-451e-8b19-3ff60f4c1926', N'9756e90f-9505-4990-ac0e-135c58bc6f29', N'0300ae60-86ac-4c4a-a701-c53d8eb00ca6', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T12:08:56.747' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-22T09:06:36.953' AS DateTime))
INSERT [dbo].[sys_org_user] ([Id], [OrgId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'67ee7a91-40e5-439c-b102-41864076ecae', N'9756e90f-9505-4990-ac0e-135c58bc6f29', N'44d83899-e4d8-4909-9eb7-969aa69a1ca8', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T12:08:56.847' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T12:08:56.847' AS DateTime))
INSERT [dbo].[sys_org_user] ([Id], [OrgId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'258511fd-281e-4bb8-baee-71721e278651', N'9756e90f-9505-4990-ac0e-135c58bc6f29', N'b464f199-3369-4dbd-ae7e-497224d5ea1d', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T11:08:04.503' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T11:08:04.503' AS DateTime))
INSERT [dbo].[sys_org_user] ([Id], [OrgId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'ced3716f-38ce-44d3-88ea-75817e3c39be', N'ab907b1e-13a3-4c04-85db-f560f9c8f9aa', N'6585aa62-9ac1-4799-863a-317e26da466a', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-21T09:53:11.783' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-22T09:05:25.063' AS DateTime))
INSERT [dbo].[sys_org_user] ([Id], [OrgId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'7599136a-e1aa-49c2-8ae8-7689e79d9857', N'57126a11-842e-493d-bc4f-e8c18853e6ed', N'6f415677-cc02-4efa-bfbd-fa7c1c2c561d', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-06-11T14:01:38.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-06-11T14:01:38.000' AS DateTime))
INSERT [dbo].[sys_org_user] ([Id], [OrgId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'd6774ab4-5cec-46f5-912e-9a6cb3ae269e', N'66255038-acda-49a4-b260-ccafeedc6074', N'930cc8e2-f1ae-404d-9258-6167a3acbcbc', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T12:14:58.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T16:49:09.000' AS DateTime))
INSERT [dbo].[sys_org_user] ([Id], [OrgId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'0ce71313-2f99-4529-aa3c-bf08b3c613e7', N'ab907b1e-13a3-4c04-85db-f560f9c8f9aa', N'45aeff89-2867-43e7-9b40-296233c069c5', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-09T11:46:45.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-09T11:46:45.000' AS DateTime))
INSERT [dbo].[sys_org_user] ([Id], [OrgId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'a863f9ee-75d2-43ae-ab82-c0705bca87cc', N'9756e90f-9505-4990-ac0e-135c58bc6f29', N'92329bbc-a81d-4da9-b286-1a8a6444456b', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T10:59:11.957' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T10:59:11.957' AS DateTime))
INSERT [dbo].[sys_org_user] ([Id], [OrgId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'727a3688-20d8-4186-b9d4-c76288d65f28', N'9756e90f-9505-4990-ac0e-135c58bc6f29', N'31cf3eed-848a-40a4-9c9d-75b2bac01d90', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T12:04:41.627' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T12:04:41.627' AS DateTime))
INSERT [dbo].[sys_org_user] ([Id], [OrgId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'0cd31059-71a1-400d-b91a-d3dc0e79a2e8', N'9756e90f-9505-4990-ac0e-135c58bc6f29', N'1b8f558f-4a5b-43c6-bc14-4b0c1e83587f', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T11:01:29.390' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-22T09:06:44.483' AS DateTime))
INSERT [dbo].[sys_org_user] ([Id], [OrgId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'02c0da63-02d3-4000-874c-da41deb37ab7', N'9756e90f-9505-4990-ac0e-135c58bc6f29', N'a668aba7-b317-4468-a0fe-a2e024f0d303', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T11:08:13.477' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T11:08:13.477' AS DateTime))
INSERT [dbo].[sys_org_user] ([Id], [OrgId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'5ccc079d-b545-453b-b8c7-dbd64dc5d6c0', N'9756e90f-9505-4990-ac0e-135c58bc6f29', N'bacc045e-eefc-4d45-9920-82192cbabb4e', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T12:03:33.727' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-18T12:03:33.727' AS DateTime))
INSERT [dbo].[sys_org_user] ([Id], [OrgId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'c5390b02-69c5-4829-9d07-f1dc15379aab', N'9756e90f-9505-4990-ac0e-135c58bc6f29', N'b09bbb4b-a7f0-4f7a-8f3d-dbb895f3edc5', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T12:14:37.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T16:49:09.000' AS DateTime))
GO
INSERT [dbo].[sys_role] ([Id], [RoleName], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'00793b23-7ed3-4e30-9906-0927f7f86543', N'Test1', 21, N'Test1', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-26T14:35:06.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-26T14:35:06.000' AS DateTime))
INSERT [dbo].[sys_role] ([Id], [RoleName], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'a59ec5b3-9e0c-4ca9-a13b-1a0b159c928b', N'用户管理岗', 2, N'用户管理角色', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T21:55:28.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-01-21T15:28:10.550' AS DateTime))
INSERT [dbo].[sys_role] ([Id], [RoleName], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'12460c8c-834a-4074-b3ab-1ea3ac2a6f78', N'Test2', 22, N'Test2', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:40:24.907' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:40:24.907' AS DateTime))
INSERT [dbo].[sys_role] ([Id], [RoleName], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'07ceaf4b-834b-4103-9085-2d302c5bb0f0', N'Test2', 20, N'Test2', 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-23T15:40:32.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-26T14:35:01.000' AS DateTime))
INSERT [dbo].[sys_role] ([Id], [RoleName], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'4c2f9410-f61e-45bb-96a5-655485f823d0', N'统一认证', 6, N'统一认证', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:40:24.863' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:14:55.657' AS DateTime))
INSERT [dbo].[sys_role] ([Id], [RoleName], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'14eb92f0-a212-41e2-8b0a-69c22bdc3919', N'Test', 10, N'', 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T13:47:27.430' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:25:59.280' AS DateTime))
INSERT [dbo].[sys_role] ([Id], [RoleName], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'f71a85a8-86bf-45e1-8160-70f329f0cbed', N'Test2', 22, N'Test2', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-26T14:35:06.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-26T14:35:06.000' AS DateTime))
INSERT [dbo].[sys_role] ([Id], [RoleName], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'df8af9fe-50c5-41b6-97dd-78e7d817c075', N'数据字典管理岗', 5, N'数据字典管理角色', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T14:20:20.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T21:54:20.000' AS DateTime))
INSERT [dbo].[sys_role] ([Id], [RoleName], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'43947b77-b453-4783-a3fc-81e6ab8e692c', N'权限管理岗', 3, N'权限管理', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T21:56:00.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:29:49.000' AS DateTime))
INSERT [dbo].[sys_role] ([Id], [RoleName], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'80c9a83f-05c8-4311-98fa-bec2a920746a', N'Test1', 21, N'Test1', 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-23T15:40:32.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-26T14:35:01.000' AS DateTime))
INSERT [dbo].[sys_role] ([Id], [RoleName], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'40c3a340-4a71-489a-bdd7-c9f4eee1e743', N'安全审计岗', 4, N'安全审计角色', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T14:18:47.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:19:42.000' AS DateTime))
INSERT [dbo].[sys_role] ([Id], [RoleName], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'机构管理岗', 1, N'机构管理角色', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-10T14:17:10.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-23T14:38:29.000' AS DateTime))
GO
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'9a10f2c7-40f8-4a83-b946-0223fa8ddce3', N'df8af9fe-50c5-41b6-97dd-78e7d817c075', N'd1bf1584-3c7a-4642-8cc6-095c42682e15', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.553' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.553' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'50de7f7c-f006-4f2c-ae04-02981c65cf1d', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'bec177ff-23ce-449f-ade4-dd0e62142842', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:10.020' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:10.020' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'd25c5c8b-b738-4484-8856-029d5423e330', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'b0652b6e-51ee-49ff-88b9-019bc8378f0b', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:16:50.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T15:29:36.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'2fcec787-17e8-4a5d-a665-03949070c8d1', N'40c3a340-4a71-489a-bdd7-c9f4eee1e743', N'9321de30-10c8-4709-aff3-bba47312693f', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:18:01.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:18:01.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'4df789f4-394c-4b39-862f-052c00d3558c', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'aa0a8b78-5e35-46f5-be7e-2a55f20f6470', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T17:21:32.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-25T10:17:58.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'12d442dd-ec97-4af6-9b60-084fb46e7cab', N'a59ec5b3-9e0c-4ca9-a13b-1a0b159c928b', N'e2aaab2a-1b25-4e45-b54f-160bc6d332df', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:17:25.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:17:25.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'05b594c0-4cdb-4ce1-b7a1-09c441540651', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'26d0ed04-5670-4707-ad91-e2893edca2ed', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:33:42.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:09.817' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'5d42a663-ac16-44ca-8535-0cd2e26128c3', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'b0652b6e-51ee-49ff-88b9-019bc8378f0b', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-25T10:17:58.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T09:36:07.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'2b3a9db2-8883-47d7-81bf-0da7c2d5854c', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'e2aaab2a-1b25-4e45-b54f-160bc6d332df', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:10.073' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:10.073' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'6376a340-2935-433e-8ece-120428d83f9d', N'43947b77-b453-4783-a3fc-81e6ab8e692c', N'0e989075-01cd-4c6e-a622-d336106bf255', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:17:43.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:17:43.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'44bfad32-b2bf-4916-b56d-121d431c64a1', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'e2363d77-45aa-4be1-b878-48d31aed728b', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T09:36:08.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:33:42.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'e426bce5-6045-418e-b169-1480af3fbb4e', N'df8af9fe-50c5-41b6-97dd-78e7d817c075', N'a81bb7aa-d7c2-41b9-b616-b46d3d7d2cf3', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.547' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.547' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'205cd604-63db-4b46-acd8-1a40e385006b', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'45c8b9cf-5ac2-4e42-9cc8-051871a1d63f', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:37:14.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T17:21:32.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'4e84e8e4-0f8b-4a54-860a-1c053a4567d1', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'45c8b9cf-5ac2-4e42-9cc8-051871a1d63f', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:16:50.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T15:29:36.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'ce31fb84-6675-4c4d-8130-1c9adcfcdc75', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'b0652b6e-51ee-49ff-88b9-019bc8378f0b', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T17:21:32.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-25T10:17:58.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'55405817-1731-4cd1-88fb-2274a827c29c', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'371362df-0b05-4eee-9930-9288b6c8cc45', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T09:36:08.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:33:42.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'2cab3394-02a3-4b67-a029-231eb059029c', N'df8af9fe-50c5-41b6-97dd-78e7d817c075', N'c1dc4140-ef7f-491e-a559-52614b581930', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.557' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.557' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'df2c829a-c8fd-4404-b47e-2413d7ea2724', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'e2363d77-45aa-4be1-b878-48d31aed728b', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:32:20.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:37:14.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'fe1fe74b-d470-48c7-a2de-241cfe273f18', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'b0652b6e-51ee-49ff-88b9-019bc8378f0b', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T09:36:08.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:33:42.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'd206cc5f-93b3-4d3b-9bff-24e9b899339b', N'df8af9fe-50c5-41b6-97dd-78e7d817c075', N'fb35235e-b771-462e-b84b-269f3d8938ce', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.557' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.557' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'0e40ab16-84bb-4d75-9ed6-27b057b058f0', N'df8af9fe-50c5-41b6-97dd-78e7d817c075', N'1553af7d-5749-4894-8c3e-84cb3cf2f2dd', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.557' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.557' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'595ab411-a66a-4a2d-948c-2a9bef61811f', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'26d0ed04-5670-4707-ad91-e2893edca2ed', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T15:48:04.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:32:20.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'480e595a-713d-4bb6-b4fe-2cf6ab1aa397', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'e2aaab2a-1b25-4e45-b54f-160bc6d332df', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T17:21:32.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-25T10:17:58.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'71e86fff-7f74-4773-b9c9-2d874cbc1f5b', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'e2363d77-45aa-4be1-b878-48d31aed728b', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:10.047' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:10.047' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'6f0d6215-8fe3-49cc-b161-3013a7b66f11', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'de435288-c697-4072-8215-394dc7f80ca6', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:10.060' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:10.060' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'73c66d2e-cd9a-40ef-8f4f-3072bd4a8036', N'df8af9fe-50c5-41b6-97dd-78e7d817c075', N'af62fa5d-4808-4a59-9297-58b0df442fff', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.550' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.550' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'3c49249d-a4a4-4081-873b-32112cf153d5', N'a59ec5b3-9e0c-4ca9-a13b-1a0b159c928b', N'de435288-c697-4072-8215-394dc7f80ca6', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:17:25.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:17:25.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'0c2bfdc2-11ca-4f06-91e1-32cd89d725a7', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'b0652b6e-51ee-49ff-88b9-019bc8378f0b', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:33:42.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:09.830' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'ba80d399-796b-4514-9e4b-35f468c17920', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'26d0ed04-5670-4707-ad91-e2893edca2ed', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-25T10:17:58.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T09:36:07.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'4e50e27f-3ab4-4689-93d1-3a0b97198fde', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'b0652b6e-51ee-49ff-88b9-019bc8378f0b', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:37:14.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T17:21:32.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'6c806321-463f-4dc8-b852-3c87a3014b94', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'26d0ed04-5670-4707-ad91-e2893edca2ed', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T09:36:08.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:33:42.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'ec8ca9c7-53d5-459e-859e-3d7db48de4d6', N'43947b77-b453-4783-a3fc-81e6ab8e692c', N'c80f26ab-b2bc-4766-a40e-17ed9417a5c7', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:17:43.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:17:43.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'5dc3abd4-545c-4f02-98ee-3ff8bdad900a', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'bec177ff-23ce-449f-ade4-dd0e62142842', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:33:42.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:09.890' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'4bc18573-f849-4ccb-b377-403e893fa761', N'df8af9fe-50c5-41b6-97dd-78e7d817c075', N'cc21d837-43ed-49c1-bc5f-9bb647c1fd14', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.547' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.547' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'ff138593-6447-4b54-8b7b-41b548228d67', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'8d549066-fe5d-466c-b540-3525ebf30def', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:32:20.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:37:14.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'0ea56c75-4536-433f-a9e6-4669c2c98ff2', N'43947b77-b453-4783-a3fc-81e6ab8e692c', N'18cd9b4c-bfbd-442b-86dc-11eee193d886', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:17:43.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:17:43.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'1d2966bc-1e6f-4419-9519-474b3528c455', N'df8af9fe-50c5-41b6-97dd-78e7d817c075', N'13e73c0d-a285-43af-9167-eef89381d08e', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-12T11:39:24.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.507' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'd5334fcf-c37a-44d3-8ee1-4821e42a0c15', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'371362df-0b05-4eee-9930-9288b6c8cc45', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:10.100' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:10.100' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'922fa344-c537-4815-b7b0-4d5ae4241468', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'bec177ff-23ce-449f-ade4-dd0e62142842', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T09:36:08.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:33:42.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'ed1ba712-b3c0-436e-a236-50aec1b9861c', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'aa0a8b78-5e35-46f5-be7e-2a55f20f6470', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:37:14.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T17:21:32.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'a3cc3ad5-2113-4905-9347-50c53a362c1e', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'26d0ed04-5670-4707-ad91-e2893edca2ed', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:32:20.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:37:14.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'25be70b6-477f-416b-a0ab-527ecd564351', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'bec177ff-23ce-449f-ade4-dd0e62142842', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:37:14.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T17:21:32.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'9697ce5a-ed63-49f4-80d3-52c8ba751f13', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'26d0ed04-5670-4707-ad91-e2893edca2ed', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T15:29:36.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T15:48:04.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'1ab766ad-b0f2-43f5-b8cb-563464cb2ee2', N'df8af9fe-50c5-41b6-97dd-78e7d817c075', N'78c1335e-f05a-4431-a85b-cee182b446f3', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.557' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.557' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'dc04de41-8510-457f-bbef-581d940a0335', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'aa0a8b78-5e35-46f5-be7e-2a55f20f6470', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-25T10:17:58.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T09:36:07.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'881a4d5e-cd6a-46d6-972f-59ecde62e56f', N'43947b77-b453-4783-a3fc-81e6ab8e692c', N'60f51a50-7fb8-47df-bbee-20a4bb29d650', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:17:43.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:17:43.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'd0c677b2-ca69-4ec4-8a6e-5bbfff9d90f5', N'df8af9fe-50c5-41b6-97dd-78e7d817c075', N'03237dc0-d26f-4611-8fe1-ba08ac1da7d8', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.560' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.560' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'86c2008c-4d75-4989-896b-5c6ce1455f8e', N'a59ec5b3-9e0c-4ca9-a13b-1a0b159c928b', N'371362df-0b05-4eee-9930-9288b6c8cc45', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:17:25.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:17:25.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'c789c2d1-4eef-488a-a12d-5d34d2b09759', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'45c8b9cf-5ac2-4e42-9cc8-051871a1d63f', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:33:42.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:09.933' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'38f5d9c0-399c-4b54-bbcc-60c79b218b2b', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'8d549066-fe5d-466c-b540-3525ebf30def', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T15:48:04.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:32:20.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'e6055bd9-e2a9-48c2-af79-62c2303e5866', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'8d549066-fe5d-466c-b540-3525ebf30def', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:37:14.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T17:21:32.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'58370717-2dfa-430c-a8c9-64cd9f137d40', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'b0652b6e-51ee-49ff-88b9-019bc8378f0b', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T15:29:36.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T15:48:04.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'08f07ebf-02b4-4881-8471-66f3e89c3cf4', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'4ef0101a-9911-47d0-8555-841ce928903f', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:32:20.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:37:14.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'60e7aaf4-ac0b-46b0-b600-68be97a15f66', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'371362df-0b05-4eee-9930-9288b6c8cc45', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T17:21:32.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-25T10:17:58.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'c65374f5-fc27-440e-b991-714c41102a85', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'8d549066-fe5d-466c-b540-3525ebf30def', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-25T10:17:58.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T09:36:07.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'2725a2e1-20ac-493e-b0f8-7155840cf3b6', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'aa0a8b78-5e35-46f5-be7e-2a55f20f6470', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:32:20.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:37:14.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'b45a293d-777b-4895-ab88-747878ecd04d', N'df8af9fe-50c5-41b6-97dd-78e7d817c075', N'8169ab55-b24e-45b5-99f1-9d65d55af215', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.547' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.547' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'0092abd4-bd3c-49cb-885b-75fc4e20795f', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'4ef0101a-9911-47d0-8555-841ce928903f', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T09:36:08.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:33:42.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'4fbc8990-5ac0-476c-a549-774f1a1ed30c', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'de435288-c697-4072-8215-394dc7f80ca6', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:33:43.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:09.877' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'5ad3fea9-c150-4456-ad26-7aecd4d7fdbb', N'43947b77-b453-4783-a3fc-81e6ab8e692c', N'837a330f-11bd-4a70-93b5-25af2c905408', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:17:43.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:17:43.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'5dcdb141-102b-4ae0-afa4-7d9305aebcad', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'45c8b9cf-5ac2-4e42-9cc8-051871a1d63f', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:32:20.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:37:14.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'dcc0d0f4-6200-4999-8f2c-7f137c681947', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'8d549066-fe5d-466c-b540-3525ebf30def', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T09:36:08.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:33:42.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'023c264b-c531-4322-87a5-803bfa6a1b0d', N'df8af9fe-50c5-41b6-97dd-78e7d817c075', N'f425487a-72a1-469d-8fa1-2334db69aba4', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-12T11:39:24.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.500' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'ceb83520-6be4-478b-b41e-807c9780adef', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'e2363d77-45aa-4be1-b878-48d31aed728b', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-25T10:17:58.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T09:36:07.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'fdafe34a-60f1-42b1-b470-86677aa2c781', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'8d549066-fe5d-466c-b540-3525ebf30def', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T15:29:36.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T15:48:04.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'c340e93a-6955-4641-8069-872f98d1180a', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'bec177ff-23ce-449f-ade4-dd0e62142842', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:16:50.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T15:29:36.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'68d7a041-1ca5-4e8d-8142-8e703d5a8773', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'bec177ff-23ce-449f-ade4-dd0e62142842', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T15:29:36.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T15:48:04.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'06df3d46-d37b-4593-9019-952ec10a16a4', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'b0652b6e-51ee-49ff-88b9-019bc8378f0b', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T15:48:04.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:32:20.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'a5b7a194-508c-4abc-bf8e-962a752aad67', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'aa0a8b78-5e35-46f5-be7e-2a55f20f6470', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:09.953' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:09.953' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'0a9c7990-be95-40af-9c84-9819ab2d4ea8', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'e2aaab2a-1b25-4e45-b54f-160bc6d332df', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T09:36:08.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:33:42.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'6477deb3-0d29-4597-8306-988ae3ae5c3b', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'26d0ed04-5670-4707-ad91-e2893edca2ed', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:16:50.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T15:29:36.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'9655a999-5662-46a9-b43f-9a8c41ebd406', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'e2363d77-45aa-4be1-b878-48d31aed728b', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:33:42.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:09.917' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'd37d8b9e-4d8b-4999-8d2e-9a9ce3511d91', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'bec177ff-23ce-449f-ade4-dd0e62142842', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-25T10:17:58.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T09:36:07.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'7b2ad447-bbba-45c2-aa08-9ac4d67d627b', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'de435288-c697-4072-8215-394dc7f80ca6', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T09:36:08.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:33:42.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'569708cb-5437-49ed-aaea-9b5ffd5bae67', N'df8af9fe-50c5-41b6-97dd-78e7d817c075', N'f21f1b48-b743-4e01-a5d7-c50ebfd2fe78', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-12T11:39:24.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.510' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'a5cbe61c-33df-4521-8f3d-9fa0f83f7885', N'df8af9fe-50c5-41b6-97dd-78e7d817c075', N'206bb7dc-101a-4d76-89fc-606cdd247355', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.560' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.560' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'973f3d07-c2fe-443e-b466-a5bc3987d7b9', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'bec177ff-23ce-449f-ade4-dd0e62142842', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:32:20.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:37:14.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'e6830da9-8a89-4826-b0fd-a6d2bc7103cf', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'45c8b9cf-5ac2-4e42-9cc8-051871a1d63f', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T15:29:36.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T15:48:04.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'911e2a86-f240-4fe4-adef-a7c5093c0c3b', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'aa0a8b78-5e35-46f5-be7e-2a55f20f6470', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:33:42.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:09.903' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'b8227404-9296-4f6e-9a96-a861f2453681', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'bec177ff-23ce-449f-ade4-dd0e62142842', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T17:21:32.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-25T10:17:58.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'9837e688-41d3-47d9-af6d-a8b808f4056c', N'df8af9fe-50c5-41b6-97dd-78e7d817c075', N'eed891c4-822d-41d4-ac80-c6b9c327a1af', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.563' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.563' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'11a2e66b-e1ae-42d3-92b6-a97c003fbc32', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'aa0a8b78-5e35-46f5-be7e-2a55f20f6470', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T09:36:08.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:33:42.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'390bda43-f6e8-4099-adab-aa7cb5cf2524', N'a59ec5b3-9e0c-4ca9-a13b-1a0b159c928b', N'e2363d77-45aa-4be1-b878-48d31aed728b', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:17:25.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:17:25.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'c3fab066-492a-4b14-a7d1-aaa853abbe92', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'45c8b9cf-5ac2-4e42-9cc8-051871a1d63f', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T17:21:32.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-25T10:17:58.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'aa168dec-ac4d-49ce-9510-ac84d50e3977', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'371362df-0b05-4eee-9930-9288b6c8cc45', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:32:20.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:37:14.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'1cfa9d25-a57b-49f7-ac9d-ace80f3ec2b4', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'e2aaab2a-1b25-4e45-b54f-160bc6d332df', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:33:43.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:09.847' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'c38053fa-3bae-43c9-8275-af0165dba8cb', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'45c8b9cf-5ac2-4e42-9cc8-051871a1d63f', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T15:48:04.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:32:20.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'0fc1ac60-8ab8-4685-b483-b649cb0b704e', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'b0652b6e-51ee-49ff-88b9-019bc8378f0b', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:09.970' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:09.970' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'3d312ee5-70e3-4b1d-be84-b6ad700e69ad', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'45c8b9cf-5ac2-4e42-9cc8-051871a1d63f', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:09.987' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:09.987' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'6e6ab298-f019-457d-a395-bd94142af853', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'26d0ed04-5670-4707-ad91-e2893edca2ed', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:37:14.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T17:21:32.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'8c1f377b-2660-4c1d-a29f-c23c590195db', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'de435288-c697-4072-8215-394dc7f80ca6', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:32:20.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:37:14.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'40670af8-9fa1-40b9-9802-c4df2580e141', N'40c3a340-4a71-489a-bdd7-c9f4eee1e743', N'0d12b992-92df-4961-a50b-a2aac55da1e2', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:18:01.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:18:01.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'6ce610ad-7c24-4139-bcf4-c523dbe6e133', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'de435288-c697-4072-8215-394dc7f80ca6', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T17:21:32.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-25T10:17:58.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'0120d05f-cabb-4263-8197-c65f26b21b36', N'a59ec5b3-9e0c-4ca9-a13b-1a0b159c928b', N'4ef0101a-9911-47d0-8555-841ce928903f', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:17:25.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:17:25.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'c81be673-9065-4046-a16e-c705a9440fe1', N'43947b77-b453-4783-a3fc-81e6ab8e692c', N'b807f782-8b8c-49b6-a62d-e230dd44cd7e', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:17:43.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:17:43.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'e0d022f6-436a-4d64-bffb-c8c863536a63', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'b0652b6e-51ee-49ff-88b9-019bc8378f0b', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:32:20.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:37:14.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'7f75b282-9cde-4f1c-9e83-c8ce8ea51226', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'aa0a8b78-5e35-46f5-be7e-2a55f20f6470', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T15:48:04.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:32:20.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'4eb06dab-73ec-43a5-9667-cd8c9721fef2', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'26d0ed04-5670-4707-ad91-e2893edca2ed', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T17:21:32.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-25T10:17:58.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'0a8bad18-20c9-4118-92cc-d3e8da4015fd', N'df8af9fe-50c5-41b6-97dd-78e7d817c075', N'e2bd72c3-b926-4072-b7a9-429cb77fe351', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.557' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.557' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'9a123bc2-fbd9-4770-9fae-d41e23a046ba', N'df8af9fe-50c5-41b6-97dd-78e7d817c075', N'2634a816-5dd9-4bfd-a573-f6b0020204db', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-12T11:39:24.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.517' AS DateTime))
GO
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'247eade8-d8d6-43a9-b14d-d79f6feb6fc3', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'e2aaab2a-1b25-4e45-b54f-160bc6d332df', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:32:20.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:37:14.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'23de9227-c943-4d41-9058-d7f3284a2a67', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'45c8b9cf-5ac2-4e42-9cc8-051871a1d63f', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-25T10:17:58.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T09:36:07.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'5b69f53f-d52b-40b5-a299-dbc0b5dcf109', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'8d549066-fe5d-466c-b540-3525ebf30def', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:10.007' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:10.007' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'0d900314-c4f4-4f4c-8b48-e32bce4b6d26', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'de435288-c697-4072-8215-394dc7f80ca6', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-25T10:17:58.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T09:36:07.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'3914498c-ad99-4e62-98ad-e3d513388902', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'8d549066-fe5d-466c-b540-3525ebf30def', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T17:21:32.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-25T10:17:58.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'cd33f801-eed3-44a8-be02-e97a13e471a0', N'43947b77-b453-4783-a3fc-81e6ab8e692c', N'd0ebd39e-9a82-42fa-986e-c7235747a758', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:17:43.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:17:43.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'9b8dd0ba-8179-44e1-8f91-ea31005f2af7', N'df8af9fe-50c5-41b6-97dd-78e7d817c075', N'ef3148eb-ebe6-4d9a-8806-aa783570290b', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.553' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.553' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'7ec66b87-405a-4224-87d8-ea9bdd1c09bd', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'8d549066-fe5d-466c-b540-3525ebf30def', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:16:50.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T15:29:36.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'7513b64e-3c74-4cd9-85b2-ed945e55ce28', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'4ef0101a-9911-47d0-8555-841ce928903f', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T17:21:32.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-25T10:17:58.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'b93c9136-00c9-48b0-8b8a-f05340e7e624', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'45c8b9cf-5ac2-4e42-9cc8-051871a1d63f', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T09:36:08.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:33:42.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'482820c4-c5f2-4908-8944-f13fb8479a7b', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'e2363d77-45aa-4be1-b878-48d31aed728b', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T17:21:32.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-25T10:17:58.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'875e822a-9603-4468-891e-f33ed99f43b4', N'df8af9fe-50c5-41b6-97dd-78e7d817c075', N'fb1f9114-e18c-4159-8af1-1408651f21fc', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.550' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.550' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'ecae4db3-751d-409e-b9d7-f5910c215a03', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'4ef0101a-9911-47d0-8555-841ce928903f', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:10.087' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:10.087' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'2b747e3f-84ee-42de-9ded-f68e356f20f7', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'8d549066-fe5d-466c-b540-3525ebf30def', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:33:42.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:09.863' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'7baf2a7b-e100-49f8-8acf-f8aee3336f97', N'df8af9fe-50c5-41b6-97dd-78e7d817c075', N'66469b10-a974-4abf-bd72-b7a205a09b47', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.550' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:15.550' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'09426ceb-b0b0-4f39-84d7-fc385af403c2', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'bec177ff-23ce-449f-ade4-dd0e62142842', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T15:48:04.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T16:32:20.000' AS DateTime))
INSERT [dbo].[sys_role_menu] ([Id], [RoleId], [MenuId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'dac74003-0e7e-4fa7-b3c4-fe7f7bc35c09', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'26d0ed04-5670-4707-ad91-e2893edca2ed', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:10.033' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:10.033' AS DateTime))
GO
INSERT [dbo].[sys_role_org] ([Id], [RoleId], [OrgId], [OwnerType], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'354845ca-eace-4d95-a65a-37496580c517', N'40c3a340-4a71-489a-bdd7-c9f4eee1e743', N'ab907b1e-13a3-4c04-85db-f560f9c8f9aa', 0, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-11-09T16:02:29.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-11-09T16:47:19.000' AS DateTime))
INSERT [dbo].[sys_role_org] ([Id], [RoleId], [OrgId], [OwnerType], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'354845ca-eace-4d95-a65a-37496580c518', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'ab907b1e-13a3-4c04-85db-f560f9c8f9aa', 0, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-11-09T16:40:50.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-11-09T16:48:39.000' AS DateTime))
INSERT [dbo].[sys_role_org] ([Id], [RoleId], [OrgId], [OwnerType], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'354845ca-eace-4d95-a65a-37496580c519', N'a59ec5b3-9e0c-4ca9-a13b-1a0b159c928b', N'ab907b1e-13a3-4c04-85db-f560f9c8f9aa', 0, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-11-09T16:02:29.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-11-09T16:47:19.000' AS DateTime))
INSERT [dbo].[sys_role_org] ([Id], [RoleId], [OrgId], [OwnerType], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'672d6f9b-c295-4972-a71e-56a566f68f88', N'43947b77-b453-4783-a3fc-81e6ab8e692c', N'ab907b1e-13a3-4c04-85db-f560f9c8f9aa', 0, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-11-09T16:40:50.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-11-09T16:48:39.000' AS DateTime))
INSERT [dbo].[sys_role_org] ([Id], [RoleId], [OrgId], [OwnerType], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'5ec2513e-621c-4b98-b10f-6fc0ef0bada6', N'12460c8c-834a-4074-b3ab-1ea3ac2a6f78', N'ab907b1e-13a3-4c04-85db-f560f9c8f9aa', 0, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:40:25.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:40:25.000' AS DateTime))
INSERT [dbo].[sys_role_org] ([Id], [RoleId], [OrgId], [OwnerType], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'9c406a50-1f09-4aa3-8b2b-7a57fabda9b2', N'14eb92f0-a212-41e2-8b0a-69c22bdc3919', N'ab907b1e-13a3-4c04-85db-f560f9c8f9aa', 0, NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T13:47:27.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:25:59.000' AS DateTime))
INSERT [dbo].[sys_role_org] ([Id], [RoleId], [OrgId], [OwnerType], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'e50ee78e-ee79-4058-b5b0-7cc31114f9af', N'df8af9fe-50c5-41b6-97dd-78e7d817c075', N'ab907b1e-13a3-4c04-85db-f560f9c8f9aa', 0, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-11-09T16:40:45.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-11-09T16:47:38.000' AS DateTime))
INSERT [dbo].[sys_role_org] ([Id], [RoleId], [OrgId], [OwnerType], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'8ee4a37b-593d-460a-beb9-88d9ab2cdb7c', N'4c2f9410-f61e-45bb-96a5-655485f823d0', N'ab907b1e-13a3-4c04-85db-f560f9c8f9aa', 0, NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:40:25.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:40:25.000' AS DateTime))
INSERT [dbo].[sys_role_org] ([Id], [RoleId], [OrgId], [OwnerType], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'7aff73ff-bca8-47f1-bdb3-adb55c251b65', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'57126a11-842e-493d-bc4f-e8c18853e6ed', 1, NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:42.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:46.000' AS DateTime))
INSERT [dbo].[sys_role_org] ([Id], [RoleId], [OrgId], [OwnerType], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'e743f7bf-6c7c-43e7-8725-c786374c12f1', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'9756e90f-9505-4990-ac0e-135c58bc6f29', 1, NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:37.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-03-17T16:43:23.000' AS DateTime))
GO
INSERT [dbo].[sys_role_user] ([Id], [RoleId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'0eafa373-aa13-467e-a65b-06b74f813bf9', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'b09bbb4b-a7f0-4f7a-8f3d-dbb895f3edc5', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:11:59.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T15:11:35.000' AS DateTime))
INSERT [dbo].[sys_role_user] ([Id], [RoleId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'293567d8-8ae4-4731-9975-08721e293dd1', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'b09bbb4b-a7f0-4f7a-8f3d-dbb895f3edc5', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:09:40.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:09:53.000' AS DateTime))
INSERT [dbo].[sys_role_user] ([Id], [RoleId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'4f380cdc-11ae-4405-bd5b-227a7b4acac3', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'b09bbb4b-a7f0-4f7a-8f3d-dbb895f3edc5', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T12:51:19.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T12:52:19.000' AS DateTime))
INSERT [dbo].[sys_role_user] ([Id], [RoleId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'ff59a249-f725-488d-aa8a-2d0e42478231', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'930cc8e2-f1ae-404d-9258-6167a3acbcbc', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T12:52:28.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:02:52.000' AS DateTime))
INSERT [dbo].[sys_role_user] ([Id], [RoleId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'6a21a69b-5955-4a89-9611-3cd47eb7c3e0', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'b09bbb4b-a7f0-4f7a-8f3d-dbb895f3edc5', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:33:23.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-03-17T16:43:30.140' AS DateTime))
INSERT [dbo].[sys_role_user] ([Id], [RoleId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'1e119430-430c-438c-9f63-4427f927442e', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'45aeff89-2867-43e7-9b40-296233c069c5', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T12:52:28.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:02:52.000' AS DateTime))
INSERT [dbo].[sys_role_user] ([Id], [RoleId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'740135f9-568f-476b-91de-583f4efc6b8d', N'df8af9fe-50c5-41b6-97dd-78e7d817c075', N'45aeff89-2867-43e7-9b40-296233c069c5', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:42.487' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2022-02-24T14:15:42.487' AS DateTime))
INSERT [dbo].[sys_role_user] ([Id], [RoleId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'43324d78-f054-486e-b688-7618106209a0', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'45aeff89-2867-43e7-9b40-296233c069c5', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T15:11:35.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:33:23.000' AS DateTime))
INSERT [dbo].[sys_role_user] ([Id], [RoleId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'da6de310-78a3-4775-9048-788acb19bdb1', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'b09bbb4b-a7f0-4f7a-8f3d-dbb895f3edc5', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-23T15:11:35.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:33:23.000' AS DateTime))
INSERT [dbo].[sys_role_user] ([Id], [RoleId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'42279870-6f6f-4287-885f-82180b7d64e1', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'b09bbb4b-a7f0-4f7a-8f3d-dbb895f3edc5', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T12:52:28.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:02:52.000' AS DateTime))
INSERT [dbo].[sys_role_user] ([Id], [RoleId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'834f6a33-68c3-4bbd-b863-85ece4fda4e7', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'45aeff89-2867-43e7-9b40-296233c069c5', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:03:18.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:09:40.000' AS DateTime))
INSERT [dbo].[sys_role_user] ([Id], [RoleId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'dafb911b-c2b2-42c1-aa16-9b704128c5b4', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'930cc8e2-f1ae-404d-9258-6167a3acbcbc', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:33:23.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:33:23.000' AS DateTime))
INSERT [dbo].[sys_role_user] ([Id], [RoleId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'e7640512-dfbb-44a8-aadf-af3c82f29bf7', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'45aeff89-2867-43e7-9b40-296233c069c5', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:09:53.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:10:25.000' AS DateTime))
INSERT [dbo].[sys_role_user] ([Id], [RoleId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'5e16587c-1d38-443f-a779-b09714c835d3', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'930cc8e2-f1ae-404d-9258-6167a3acbcbc', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:09:40.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:09:53.000' AS DateTime))
INSERT [dbo].[sys_role_user] ([Id], [RoleId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'45f9b5a2-7b43-43bb-8544-b774488535d6', N'a59ec5b3-9e0c-4ca9-a13b-1a0b159c928b', N'930cc8e2-f1ae-404d-9258-6167a3acbcbc', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:15:58.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:15:58.000' AS DateTime))
INSERT [dbo].[sys_role_user] ([Id], [RoleId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'0a1d385e-5bc6-4648-8d43-bad5b642fd7c', N'df8af9fe-50c5-41b6-97dd-78e7d817c075', N'930cc8e2-f1ae-404d-9258-6167a3acbcbc', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-12T11:39:37.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-12T11:39:37.000' AS DateTime))
INSERT [dbo].[sys_role_user] ([Id], [RoleId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'c6999828-2fd3-43bb-adbf-d5bbcd4de1b7', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'45aeff89-2867-43e7-9b40-296233c069c5', NULL, 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:33:23.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-29T15:33:23.000' AS DateTime))
INSERT [dbo].[sys_role_user] ([Id], [RoleId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'0efed5b5-7eee-48fd-ad21-de8d80e5468d', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'6f415677-cc02-4efa-bfbd-fa7c1c2c561d', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:20.067' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-12-03T16:26:29.363' AS DateTime))
INSERT [dbo].[sys_role_user] ([Id], [RoleId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'8a72cc3c-a89a-4c30-9438-e96eeab1e6d1', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'45aeff89-2867-43e7-9b40-296233c069c5', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T12:51:19.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T12:52:19.000' AS DateTime))
INSERT [dbo].[sys_role_user] ([Id], [RoleId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'759d8f21-fecd-445d-9d3f-eef61c197f11', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'930cc8e2-f1ae-404d-9258-6167a3acbcbc', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T12:51:19.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T12:52:19.000' AS DateTime))
INSERT [dbo].[sys_role_user] ([Id], [RoleId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'4b32b83d-51ca-4a29-a0ea-f05a56fa544b', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'930cc8e2-f1ae-404d-9258-6167a3acbcbc', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:03:18.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:09:40.000' AS DateTime))
INSERT [dbo].[sys_role_user] ([Id], [RoleId], [UserId], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'11d34b17-f2c2-4a68-ba3a-fa211231e85b', N'46a7abd1-ed2a-4617-bd38-f03b256efe84', N'45aeff89-2867-43e7-9b40-296233c069c5', NULL, 1, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:09:40.000' AS DateTime), N'00000000-0000-0000-0000-000000000000', CAST(N'2021-04-11T16:09:53.000' AS DateTime))
GO
INSERT [dbo].[sys_schedule_job] ([Id], [JobName], [JobDescription], [NameSpace], [JobImplement], [CronExpression], [StartTime], [EndTime], [JobStatus], [GroupId], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'10f976a2-f9ef-4629-8f43-9afce17c2a9d', N'邮件发送任务', N'定时发送邮件作业', N'RuYiAdmin.Net.Service.QuartzJobs', N'EmailJob', N'0/5 * * * * ?', CAST(N'2021-09-10T10:19:12.000' AS DateTime), CAST(N'9999-12-31T00:00:00.000' AS DateTime), 1, 1, 2, N'定时发送邮件作业', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-10T10:16:47.000' AS DateTime), N'0374f393-ed3b-40d5-9e7c-65b560b67f6a', CAST(N'2022-03-17T10:20:48.533' AS DateTime))
INSERT [dbo].[sys_schedule_job] ([Id], [JobName], [JobDescription], [NameSpace], [JobImplement], [CronExpression], [StartTime], [EndTime], [JobStatus], [GroupId], [SerialNumber], [Remark], [IsDel], [Creator], [CreateTime], [Modifier], [ModifyTime]) VALUES (N'8df4699c-b4d4-468f-9037-df49cd5a62dd', N'定时短信任务', N'定时发送短信作业', N'RuYiAdmin.Net.Service.QuartzJobs', N'MessageJob', N'0/5 * * * * ?', CAST(N'2021-09-10T09:46:28.000' AS DateTime), CAST(N'9999-12-31T00:00:00.000' AS DateTime), 1, 1, 1, N'定时短信任务作业', 0, N'00000000-0000-0000-0000-000000000000', CAST(N'2021-09-10T09:33:43.000' AS DateTime), N'f7d72a4b-b2bc-4bda-ad34-181432a974c0', CAST(N'2022-03-18T10:21:55.887' AS DateTime))
GO
ALTER TABLE [dbo].[biz_account]  WITH CHECK ADD  CONSTRAINT [FK_BIZ_ACCO_REFERENCE_BIZ_MODU] FOREIGN KEY([ModuleId])
REFERENCES [dbo].[biz_module] ([Id])
GO
ALTER TABLE [dbo].[biz_account] CHECK CONSTRAINT [FK_BIZ_ACCO_REFERENCE_BIZ_MODU]
GO
ALTER TABLE [dbo].[biz_user_module]  WITH CHECK ADD  CONSTRAINT [FK_BIZ_USER_REFERENCE_BIZ_MODU] FOREIGN KEY([ModuleId])
REFERENCES [dbo].[biz_module] ([Id])
GO
ALTER TABLE [dbo].[biz_user_module] CHECK CONSTRAINT [FK_BIZ_USER_REFERENCE_BIZ_MODU]
GO
ALTER TABLE [dbo].[biz_user_module]  WITH CHECK ADD  CONSTRAINT [FK_BIZ_USER_REFERENCE_BIZ_USER] FOREIGN KEY([UserId])
REFERENCES [dbo].[biz_user] ([Id])
GO
ALTER TABLE [dbo].[biz_user_module] CHECK CONSTRAINT [FK_BIZ_USER_REFERENCE_BIZ_USER]
GO
ALTER TABLE [dbo].[sys_import_config_detail]  WITH CHECK ADD  CONSTRAINT [FK_SYS_IMPO_REFERENCE_SYS_IMPO] FOREIGN KEY([ParentId])
REFERENCES [dbo].[sys_import_config] ([Id])
GO
ALTER TABLE [dbo].[sys_import_config_detail] CHECK CONSTRAINT [FK_SYS_IMPO_REFERENCE_SYS_IMPO]
GO
ALTER TABLE [dbo].[sys_menu_language]  WITH CHECK ADD  CONSTRAINT [FK_SYS_MENU_REFERENCE_SYS_LANG] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[sys_language] ([Id])
GO
ALTER TABLE [dbo].[sys_menu_language] CHECK CONSTRAINT [FK_SYS_MENU_REFERENCE_SYS_LANG]
GO
ALTER TABLE [dbo].[sys_menu_language]  WITH CHECK ADD  CONSTRAINT [FK_SYS_MENU_REFERENCE_SYS_MENU] FOREIGN KEY([MenuId])
REFERENCES [dbo].[sys_menu] ([Id])
GO
ALTER TABLE [dbo].[sys_menu_language] CHECK CONSTRAINT [FK_SYS_MENU_REFERENCE_SYS_MENU]
GO
ALTER TABLE [dbo].[sys_org_user]  WITH CHECK ADD  CONSTRAINT [FK_SYS_ORG__REFERENCE_SYS_ORGA] FOREIGN KEY([OrgId])
REFERENCES [dbo].[sys_organization] ([Id])
GO
ALTER TABLE [dbo].[sys_org_user] CHECK CONSTRAINT [FK_SYS_ORG__REFERENCE_SYS_ORGA]
GO
ALTER TABLE [dbo].[sys_org_user]  WITH CHECK ADD  CONSTRAINT [FK_SYS_ORG__REFERENCE_SYS_USER] FOREIGN KEY([UserId])
REFERENCES [dbo].[sys_user] ([Id])
GO
ALTER TABLE [dbo].[sys_org_user] CHECK CONSTRAINT [FK_SYS_ORG__REFERENCE_SYS_USER]
GO
ALTER TABLE [dbo].[sys_role_menu]  WITH CHECK ADD  CONSTRAINT [FK_SYS_ROLE_REFERENCE_SYS_MENU] FOREIGN KEY([MenuId])
REFERENCES [dbo].[sys_menu] ([Id])
GO
ALTER TABLE [dbo].[sys_role_menu] CHECK CONSTRAINT [FK_SYS_ROLE_REFERENCE_SYS_MENU]
GO
ALTER TABLE [dbo].[sys_role_menu]  WITH CHECK ADD  CONSTRAINT [FK_SYS_ROLE_REFERENCE_SYS_ROLE] FOREIGN KEY([RoleId])
REFERENCES [dbo].[sys_role] ([Id])
GO
ALTER TABLE [dbo].[sys_role_menu] CHECK CONSTRAINT [FK_SYS_ROLE_REFERENCE_SYS_ROLE]
GO
ALTER TABLE [dbo].[sys_role_org]  WITH CHECK ADD  CONSTRAINT [FK_SYS_ROLE_ORG_SYS_ROLE] FOREIGN KEY([RoleId])
REFERENCES [dbo].[sys_role] ([Id])
GO
ALTER TABLE [dbo].[sys_role_org] CHECK CONSTRAINT [FK_SYS_ROLE_ORG_SYS_ROLE]
GO
ALTER TABLE [dbo].[sys_role_org]  WITH CHECK ADD  CONSTRAINT [FK_SYS_ROLE_REFERENCE_SYS_ORGA] FOREIGN KEY([OrgId])
REFERENCES [dbo].[sys_organization] ([Id])
GO
ALTER TABLE [dbo].[sys_role_org] CHECK CONSTRAINT [FK_SYS_ROLE_REFERENCE_SYS_ORGA]
GO
ALTER TABLE [dbo].[sys_role_user]  WITH CHECK ADD  CONSTRAINT [FK_SYS_ROLE_REFERENCE_SYS_USER] FOREIGN KEY([UserId])
REFERENCES [dbo].[sys_user] ([Id])
GO
ALTER TABLE [dbo].[sys_role_user] CHECK CONSTRAINT [FK_SYS_ROLE_REFERENCE_SYS_USER]
GO
ALTER TABLE [dbo].[sys_role_user]  WITH CHECK ADD  CONSTRAINT [FK_SYS_ROLE_USER_SYS_ROLE] FOREIGN KEY([RoleId])
REFERENCES [dbo].[sys_role] ([Id])
GO
ALTER TABLE [dbo].[sys_role_user] CHECK CONSTRAINT [FK_SYS_ROLE_USER_SYS_ROLE]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_account', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模块编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_account', @level2type=N'COLUMN',@level2name=N'ModuleId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户登录账号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_account', @level2type=N'COLUMN',@level2name=N'UserLogonName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_account', @level2type=N'COLUMN',@level2name=N'UserDisplayName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户密码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_account', @level2type=N'COLUMN',@level2name=N'UserPassword'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'座机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_account', @level2type=N'COLUMN',@level2name=N'Telephone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_account', @level2type=N'COLUMN',@level2name=N'MobilePhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_account', @level2type=N'COLUMN',@level2name=N'Email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否启用，0：禁用，1：启用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_account', @level2type=N'COLUMN',@level2name=N'IsEnabled'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_account', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_account', @level2type=N'COLUMN',@level2name=N'IsDel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_account', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_account', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_account', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_account', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模块API访问账号表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模块编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_module', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模块名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_module', @level2type=N'COLUMN',@level2name=N'ModuleName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模块简称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_module', @level2type=N'COLUMN',@level2name=N'ModuleShortName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模块英文简称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_module', @level2type=N'COLUMN',@level2name=N'ModuleShortNameEN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'采用HTTP协议，HTTP或者HTTPS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_module', @level2type=N'COLUMN',@level2name=N'ModuleProtocol'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模块地址：ip或者域名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_module', @level2type=N'COLUMN',@level2name=N'ModuleAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模块端口' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_module', @level2type=N'COLUMN',@level2name=N'ModulePort'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模块logo图片位置' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_module', @level2type=N'COLUMN',@level2name=N'ModuleLogoAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模块单点登录地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_module', @level2type=N'COLUMN',@level2name=N'ModuleSsoAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模块待办地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_module', @level2type=N'COLUMN',@level2name=N'ModuleTodoAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_module', @level2type=N'COLUMN',@level2name=N'SerialNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_module', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_module', @level2type=N'COLUMN',@level2name=N'IsDel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_module', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_module', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_module', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_module', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模块表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_module'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_user', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户登录账号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_user', @level2type=N'COLUMN',@level2name=N'UserLogonName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_user', @level2type=N'COLUMN',@level2name=N'UserDisplayName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户密码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_user', @level2type=N'COLUMN',@level2name=N'UserPassword'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'座机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_user', @level2type=N'COLUMN',@level2name=N'Telephone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_user', @level2type=N'COLUMN',@level2name=N'MobilePhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_user', @level2type=N'COLUMN',@level2name=N'Email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否启用，0：禁用，1：启用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_user', @level2type=N'COLUMN',@level2name=N'IsEnabled'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_user', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_user', @level2type=N'COLUMN',@level2name=N'IsDel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_user', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_user', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_user', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_user', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模块用户表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_user'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_user_module', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_user_module', @level2type=N'COLUMN',@level2name=N'UserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模块编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_user_module', @level2type=N'COLUMN',@level2name=N'ModuleId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户所在模块登录账号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_user_module', @level2type=N'COLUMN',@level2name=N'UserModuleLogonName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户所在模块登录密码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_user_module', @level2type=N'COLUMN',@level2name=N'UserModulePassword'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_user_module', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_user_module', @level2type=N'COLUMN',@level2name=N'IsDel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_user_module', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_user_module', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_user_module', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_user_module', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'模块与用户关系表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'biz_user_module'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_code_table', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_code_table', @level2type=N'COLUMN',@level2name=N'CodeName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_code_table', @level2type=N'COLUMN',@level2name=N'Code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_code_table', @level2type=N'COLUMN',@level2name=N'Value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'父键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_code_table', @level2type=N'COLUMN',@level2name=N'ParentId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_code_table', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_code_table', @level2type=N'COLUMN',@level2name=N'SerialNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_code_table', @level2type=N'COLUMN',@level2name=N'IsDel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_code_table', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_code_table', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_code_table', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_code_table', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'字典表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_code_table'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'配置名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config', @level2type=N'COLUMN',@level2name=N'ConfigName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'起始行' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config', @level2type=N'COLUMN',@level2name=N'StartRow'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'起始列' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config', @level2type=N'COLUMN',@level2name=N'StartColumn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'工作簿索引列表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config', @level2type=N'COLUMN',@level2name=N'WorkSheetIndexes'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config', @level2type=N'COLUMN',@level2name=N'IsDel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'导入配置主表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config_detail', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'父键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config_detail', @level2type=N'COLUMN',@level2name=N'ParentId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'数据类型，0：小数，1：整数，2：文本t，3：日期，4：时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config_detail', @level2type=N'COLUMN',@level2name=N'DataType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'所在列' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config_detail', @level2type=N'COLUMN',@level2name=N'Cells'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否必填项，0：否，1：是' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config_detail', @level2type=N'COLUMN',@level2name=N'Required'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最大值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config_detail', @level2type=N'COLUMN',@level2name=N'MaxValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最小值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config_detail', @level2type=N'COLUMN',@level2name=N'MinValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'小数位上限' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config_detail', @level2type=N'COLUMN',@level2name=N'DecimalLimit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'枚举列表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config_detail', @level2type=N'COLUMN',@level2name=N'TextEnum'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'扩展字段' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config_detail', @level2type=N'COLUMN',@level2name=N'Extend1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'扩展字段' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config_detail', @level2type=N'COLUMN',@level2name=N'Extend2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'扩展字段' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config_detail', @level2type=N'COLUMN',@level2name=N'Extend3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config_detail', @level2type=N'COLUMN',@level2name=N'SerialNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config_detail', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config_detail', @level2type=N'COLUMN',@level2name=N'IsDel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config_detail', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config_detail', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config_detail', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config_detail', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'导入配置子表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_import_config_detail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_language', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_language', @level2type=N'COLUMN',@level2name=N'LanguageName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_language', @level2type=N'COLUMN',@level2name=N'OrderNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_language', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_language', @level2type=N'COLUMN',@level2name=N'IsDel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_language', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_language', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_language', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_language', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'系统语言' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_language'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'UserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'UserName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'机构编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'OrgId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'机构名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'OrgName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用系统' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'System'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'浏览器' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'Browser'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IP地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'IP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'操作类型，0：查询列表，1：查询记录，2：新增，3：编辑，4：逻辑删除，5：物理删除，6：上传，7：下载，8：导入，9：导出，10：菜单授权，11：用户授权，12：打印，13：登录，14：登出，15：强制登出，16：更新密码，17：添加计划任务，18：暂停计划任务，19：启动计划任务' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'OperationType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'请求路径' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'RequestUrl'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'请求参数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'Params'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'返回结果' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'Result'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'旧值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'OldVaue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'新值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'NewValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'IsDel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'审计日志表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_log'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'根路径' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu', @level2type=N'COLUMN',@level2name=N'Path'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu', @level2type=N'COLUMN',@level2name=N'MenuName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'菜单路径' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu', @level2type=N'COLUMN',@level2name=N'MenuUrl'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'父键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu', @level2type=N'COLUMN',@level2name=N'ParentId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu', @level2type=N'COLUMN',@level2name=N'SerialNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'菜单类型，0：菜单，1：按钮，视图：2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu', @level2type=N'COLUMN',@level2name=N'MenuType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'图标' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu', @level2type=N'COLUMN',@level2name=N'Icon'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu', @level2type=N'COLUMN',@level2name=N'Code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu', @level2type=N'COLUMN',@level2name=N'IsDel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'菜单表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu_language', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'菜单编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu_language', @level2type=N'COLUMN',@level2name=N'MenuId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'语言编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu_language', @level2type=N'COLUMN',@level2name=N'LanguageId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'菜单名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu_language', @level2type=N'COLUMN',@level2name=N'MenuName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu_language', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu_language', @level2type=N'COLUMN',@level2name=N'IsDel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu_language', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu_language', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu_language', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu_language', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'菜单多语表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_menu_language'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_org_user', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'机构编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_org_user', @level2type=N'COLUMN',@level2name=N'OrgId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_org_user', @level2type=N'COLUMN',@level2name=N'UserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_org_user', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_org_user', @level2type=N'COLUMN',@level2name=N'IsDel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_org_user', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_org_user', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_org_user', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_org_user', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'机构用户关系表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_org_user'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_organization', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'机构名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_organization', @level2type=N'COLUMN',@level2name=N'OrgName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'父键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_organization', @level2type=N'COLUMN',@level2name=N'ParentId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主管人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_organization', @level2type=N'COLUMN',@level2name=N'Leader'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_organization', @level2type=N'COLUMN',@level2name=N'SerialNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_organization', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_organization', @level2type=N'COLUMN',@level2name=N'IsDel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_organization', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_organization', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_organization', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_organization', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'机构表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_organization'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role', @level2type=N'COLUMN',@level2name=N'RoleName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role', @level2type=N'COLUMN',@level2name=N'SerialNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role', @level2type=N'COLUMN',@level2name=N'IsDel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_menu', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_menu', @level2type=N'COLUMN',@level2name=N'RoleId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'菜单编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_menu', @level2type=N'COLUMN',@level2name=N'MenuId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_menu', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_menu', @level2type=N'COLUMN',@level2name=N'IsDel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_menu', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_menu', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_menu', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_menu', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色菜单关系表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_menu'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_org', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_org', @level2type=N'COLUMN',@level2name=N'RoleId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'机构编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_org', @level2type=N'COLUMN',@level2name=N'OrgId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'类型：0，自有；1继承' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_org', @level2type=N'COLUMN',@level2name=N'OwnerType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_org', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_org', @level2type=N'COLUMN',@level2name=N'IsDel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_org', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_org', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_org', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_org', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色机构关系表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_org'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_user', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_user', @level2type=N'COLUMN',@level2name=N'RoleId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_user', @level2type=N'COLUMN',@level2name=N'UserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_user', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_user', @level2type=N'COLUMN',@level2name=N'IsDel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_user', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_user', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_user', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_user', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'角色用户关系表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_role_user'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_schedule_job', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'任务名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_schedule_job', @level2type=N'COLUMN',@level2name=N'JobName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'任务描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_schedule_job', @level2type=N'COLUMN',@level2name=N'JobDescription'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'命名空间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_schedule_job', @level2type=N'COLUMN',@level2name=N'NameSpace'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实现类' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_schedule_job', @level2type=N'COLUMN',@level2name=N'JobImplement'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cron表达式' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_schedule_job', @level2type=N'COLUMN',@level2name=N'CronExpression'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'开始时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_schedule_job', @level2type=N'COLUMN',@level2name=N'StartTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'结束时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_schedule_job', @level2type=N'COLUMN',@level2name=N'EndTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'任务状态，0：已启用，1：运行中，2：执行中，3：执行完成，4：任务计划中，5：已停止' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_schedule_job', @level2type=N'COLUMN',@level2name=N'JobStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'集群编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_schedule_job', @level2type=N'COLUMN',@level2name=N'GroupId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_schedule_job', @level2type=N'COLUMN',@level2name=N'SerialNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_schedule_job', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_schedule_job', @level2type=N'COLUMN',@level2name=N'IsDel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_schedule_job', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_schedule_job', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_schedule_job', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_schedule_job', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'计划任务表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_schedule_job'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'登录名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'LogonName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'DisplayName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'登陆密码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'Password'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'座机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'Telephone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'手机' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'MobilePhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'Email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否是超级管理员' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'IsSupperAdmin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'SerialNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否启用，0：禁用，1：启用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'IsEnabled'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标志位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'IsDel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user', @level2type=N'COLUMN',@level2name=N'ModifyTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sys_user'
GO
USE [master]
GO
ALTER DATABASE [RuYiAdmin] SET  READ_WRITE 
GO
