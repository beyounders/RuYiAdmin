﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RuYiAdmin.Net.Common.Global;
using RuYiAdmin.Net.Common.Utility;
using RuYiAdmin.Net.Entity.BusinessEnum;
using RuYiAdmin.Net.WebApi.AppCode.ActionFilters;
using RuYiAdmin.Net.WebApi.AppCode.AuthorizationFilter;
using System;
using System.Net;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.WebApi.Controllers.SystemManagement
{
    [Authorize]
    [ActionAuthorization]
    [ApiController]
    [Route(GlobalContext.RouteTemplate)]
    public class ServerHardwareMonitorController : ControllerBase
    {
        #region 获取服务器信息

        /// <summary>
        /// 获取服务器信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Log(OperationType.QueryEntity)]
        [Permission("query:hardware:info")]
        public async Task<IActionResult> Get()
        {
            return await Task.Run(() =>
            {
                var actionResult = new RuYiAdmin.Net.Entity.CoreEntity.ActionResult();
                actionResult.HttpStatusCode = HttpStatusCode.OK;
                actionResult.Message = new String("OK");
                var work = SmartThreadPoolUtil.Instance.QueueWorkItem(HardwareMonitorUtil.StartMonitoring);
                actionResult.Object = work.Result;
                return Ok(actionResult);
            });
        }

        #endregion
    }
}
