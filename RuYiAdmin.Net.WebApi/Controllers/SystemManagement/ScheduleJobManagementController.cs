﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RuYiAdmin.Net.Common.Global;
using RuYiAdmin.Net.Entity.BusinessDTO.SystemManagement;
using RuYiAdmin.Net.Entity.BusinessEntity.SystemManagement;
using RuYiAdmin.Net.Entity.BusinessEnum;
using RuYiAdmin.Net.Entity.CoreEntity;
using RuYiAdmin.Net.Service.BusinessService.MQ;
using RuYiAdmin.Net.Service.BusinessService.Redis;
using RuYiAdmin.Net.Service.BusinessService.SystemManagement.ScheduleJob;
using RuYiAdmin.Net.WebApi.AppCode.ActionFilters;
using RuYiAdmin.Net.WebApi.AppCode.FrameworkBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RuYiAdmin.Net.WebApi.Controllers.SystemManagement
{
    /// <summary>
    /// 计划任务控制器
    /// </summary>
    public class ScheduleJobManagementController : RuYiAdminBaseController<SysScheduleJob>
    {
        #region 属性及构造函数

        /// <summary>
        /// 计划任务接口实例
        /// </summary>
        private readonly IScheduleJobService scheduleJobService;

        /// <summary>
        /// Redis服务接口实例
        /// </summary>
        private readonly IRedisService redisService;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="scheduleJobService"></param>
        /// <param name="redisService"></param>
        public ScheduleJobManagementController(IScheduleJobService scheduleJobService,
                                               IRedisService redisService) : base(scheduleJobService)
        {
            this.scheduleJobService = scheduleJobService;
            this.redisService = redisService;
        }

        #endregion

        #region 查询任务列表

        /// <summary>
        /// 查询任务列表
        /// </summary>
        /// <param name="queryCondition">查询条件</param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [Log(OperationType.QueryList)]
        [Permission("job:query:list")]
        public async Task<IActionResult> Post(QueryCondition queryCondition)
        {
            var actionResult = await this.scheduleJobService.SqlQueryAsync(queryCondition, "sqls:sql:query_sysschedulejob");
            return Ok(actionResult);
        }

        #endregion

        #region 查询任务信息

        /// <summary>
        /// 查询任务信息
        /// </summary>
        /// <param name="jobId">任务编号</param>
        /// <returns>ActionResult</returns>
        [HttpGet("{jobId}")]
        [Log(OperationType.QueryEntity)]
        [Permission("job:query:list")]
        public async Task<IActionResult> GetById(Guid jobId)
        {
            var actionResult = await this.scheduleJobService.GetByIdAsync(jobId);
            return Ok(actionResult);
        }

        #endregion

        #region 新增计划任务

        /// <summary>
        /// 新增计划任务
        /// </summary>
        /// <param name="scheduleJob">计划任务对象</param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [Log(OperationType.AddEntity)]
        [Permission("job:add:entity")]
        public async Task<IActionResult> Add([FromBody] SysScheduleJob scheduleJob)
        {
            scheduleJob.JobStatus = JobStatus.Planning;

            //支持集群作业
            if (GlobalContext.QuartzConfig.SupportGroup)
            {
                //保存为本节点定时任务
                scheduleJob.GroupId = GlobalContext.QuartzConfig.GroupId;
            }

            var actionResult = await this.scheduleJobService.AddAsync(scheduleJob);
            return Ok(actionResult);
        }

        #endregion

        #region 编辑计划任务

        /// <summary>
        /// 编辑计划任务
        /// </summary>
        /// <param name="scheduleJob">计划任务对象</param>
        /// <returns>ActionResult</returns>
        [HttpPut]
        [Log(OperationType.EditEntity)]
        [Permission("job:edit:entity")]
        public async Task<IActionResult> Put([FromBody] SysScheduleJob scheduleJob)
        {
            var task = await this.scheduleJobService.GetByIdAsync(scheduleJob.Id);
            var job = task.Object as SysScheduleJob;

            if (job.JobStatus == JobStatus.Running)
            {
                return BadRequest("job is running,please stop it first");
            }

            var actionResult = await this.scheduleJobService.UpdateAsync(scheduleJob);
            return Ok(actionResult);
        }

        #endregion

        #region 删除计划任务

        /// <summary>
        /// 删除计划任务
        /// </summary>
        /// <param name="jobId">任务编号</param>
        /// <returns>ActionResult</returns>
        [HttpDelete("{jobId}")]
        [Permission("job:del:entity")]
        [Log(OperationType.DeleteEntity)]
        public async Task<IActionResult> DeleteAsync(Guid jobId)
        {
            var job = this.scheduleJobService.GetById(jobId).Object as SysScheduleJob;

            if (!GlobalContext.QuartzConfig.SupportGroup)
            {
                //单机版，不支持集群
                await this.scheduleJobService.DeleteScheduleJobAsync(jobId);

                var actionResult = new Entity.CoreEntity.ActionResult();
                actionResult.HttpStatusCode = HttpStatusCode.OK;
                actionResult.Message = "OK";

                return Ok(actionResult);
            }
            else if (GlobalContext.QuartzConfig.SupportGroup && job.GroupId != null && job.GroupId == GlobalContext.QuartzConfig.GroupId)
            {
                //支持集群、且为本节点任务
                await this.scheduleJobService.DeleteScheduleJobAsync(jobId);

                var actionResult = new Entity.CoreEntity.ActionResult();
                actionResult.HttpStatusCode = HttpStatusCode.OK;
                actionResult.Message = "OK";

                return Ok(actionResult);
            }
            else
            {
                var msg = JsonConvert.SerializeObject(new QuartzJobDTO()
                {
                    JobId = job.Id,
                    GroupId = job.GroupId,
                    Action = "Delete"
                });

                //支持集群、且不为本节点任务
                this.redisService.PublishMessage(GlobalContext.QuartzConfig.ChanelName, msg);

                return Ok("OK");
            }
        }

        #endregion

        #region 启动计划任务

        /// <summary>
        /// 启动计划任务
        /// </summary>
        /// <param name="jobId">任务编号</param>
        /// <returns>ActionResult</returns>
        [HttpGet("{jobId}")]
        [Log(OperationType.StartScheduleJob)]
        [Permission("schedule:job:add")]
        public async Task<IActionResult> StartScheduleJobAsync(Guid jobId)
        {
            var job = this.scheduleJobService.GetById(jobId).Object as SysScheduleJob;

            if (!GlobalContext.QuartzConfig.SupportGroup)
            {
                //单机版，不支持集群
                await this.scheduleJobService.StartScheduleJobAsync(jobId);
            }
            else if (GlobalContext.QuartzConfig.SupportGroup && job.GroupId != null && job.GroupId == GlobalContext.QuartzConfig.GroupId)
            {
                //支持集群、且为本节点任务
                await this.scheduleJobService.StartScheduleJobAsync(jobId);
            }
            else
            {
                var msg = JsonConvert.SerializeObject(new QuartzJobDTO()
                {
                    JobId = job.Id,
                    GroupId = job.GroupId,
                    Action = "Start"
                });

                //支持集群、且不为本节点任务
                this.redisService.PublishMessage(GlobalContext.QuartzConfig.ChanelName, msg);
            }

            return Ok("OK");
        }

        #endregion

        #region 暂停计划任务

        /// <summary>
        /// 暂停计划任务
        /// </summary>
        /// <param name="jobId">任务编号</param>
        /// <returns>ActionResult</returns>
        [HttpGet("{jobId}")]
        [Log(OperationType.PauseScheduleJob)]
        [Permission("schedule:job:pause")]
        public async Task<IActionResult> PauseScheduleJob(Guid jobId)
        {
            var job = this.scheduleJobService.GetById(jobId).Object as SysScheduleJob;

            if (!GlobalContext.QuartzConfig.SupportGroup)
            {
                //单机版，不支持集群
                await this.scheduleJobService.PauseScheduleJobAsync(jobId);
            }
            else if (GlobalContext.QuartzConfig.SupportGroup && job.GroupId != null && job.GroupId == GlobalContext.QuartzConfig.GroupId)
            {
                //支持集群、且为本节点任务
                await this.scheduleJobService.PauseScheduleJobAsync(jobId);
            }
            else
            {
                var msg = JsonConvert.SerializeObject(new QuartzJobDTO()
                {
                    JobId = job.Id,
                    GroupId = job.GroupId,
                    Action = "Pause"
                });

                //支持集群、且不为本节点任务
                this.redisService.PublishMessage(GlobalContext.QuartzConfig.ChanelName, msg);
            }

            return Ok("OK");
        }

        #endregion

        #region 恢复计划任务

        /// <summary>
        /// 恢复计划任务
        /// </summary>
        /// <param name="jobId">任务编号</param>
        /// <returns>ActionResult</returns>
        [HttpGet("{jobId}")]
        [Log(OperationType.ResumeScheduleJob)]
        [Permission("schedule:job:resume")]
        public async Task<IActionResult> ResumeScheduleJob(Guid jobId)
        {
            var job = this.scheduleJobService.GetById(jobId).Object as SysScheduleJob;

            if (!GlobalContext.QuartzConfig.SupportGroup)
            {
                //单机版，不支持集群
                await this.scheduleJobService.ResumeScheduleJobAsync(jobId);
            }
            else if (GlobalContext.QuartzConfig.SupportGroup && job.GroupId != null && job.GroupId == GlobalContext.QuartzConfig.GroupId)
            {
                //支持集群、且为本节点任务
                await this.scheduleJobService.ResumeScheduleJobAsync(jobId);
            }
            else
            {
                var msg = JsonConvert.SerializeObject(new QuartzJobDTO()
                {
                    JobId = job.Id,
                    GroupId = job.GroupId,
                    Action = "Resume"
                });

                //支持集群、且不为本节点任务
                this.redisService.PublishMessage(GlobalContext.QuartzConfig.ChanelName, msg);
            }

            return Ok("OK");
        }

        #endregion
    }
}
