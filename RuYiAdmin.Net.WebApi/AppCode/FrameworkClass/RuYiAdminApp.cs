﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using RuYiAdmin.Net.Common.Global;
using RuYiAdmin.Net.Repository.Base;
using RuYiAdmin.Net.Service.BusinessService.SystemManagement.CodeTable;
using RuYiAdmin.Net.Service.BusinessService.SystemManagement.Language;
using RuYiAdmin.Net.Service.BusinessService.SystemManagement.Menu;
using RuYiAdmin.Net.Service.BusinessService.SystemManagement.MenuLanguage;
using RuYiAdmin.Net.Service.BusinessService.SystemManagement.Organization;
using RuYiAdmin.Net.Service.BusinessService.SystemManagement.Role;
using RuYiAdmin.Net.Service.BusinessService.SystemManagement.RoleMenu;
using RuYiAdmin.Net.Service.BusinessService.SystemManagement.RoleOrg;
using RuYiAdmin.Net.Service.BusinessService.SystemManagement.RoleUser;
using RuYiAdmin.Net.Service.BusinessService.SystemManagement.ScheduleJob;
using RuYiAdmin.Net.Service.BusinessService.SystemManagement.User;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.WebApi.AppCode.FrameworkClass
{
    public class RuYiAdminApp
    {
        #region 启动业务作业

        /// <summary>
        /// 启动业务作业
        /// </summary>
        /// <returns></returns>
        public static async Task StartScheduleJobAsync(IApplicationBuilder app)
        {
            await app.ApplicationServices.GetService<IScheduleJobService>().StartScheduleJobAsync();
        }

        #endregion

        #region 加载系统缓存

        /// <summary>
        /// 加载系统缓存
        /// </summary>
        /// <returns></returns>
        public static async Task LoadSystemCache(IApplicationBuilder app)
        {
            //加载机构缓存
            await app.ApplicationServices.GetService<IOrganizationService>().LoadSystemOrgCache();

            //加载用户缓存
            await app.ApplicationServices.GetService<IUserService>().LoadSystemUserCache();

            //加载菜单缓存
            await app.ApplicationServices.GetService<IMenuService>().LoadSystemMenuCache();

            //加载菜单与多语缓存
            await app.ApplicationServices.GetService<IMenuLanguageService>().LoadSystemMenuLanguageCache();

            //加载角色缓存
            await app.ApplicationServices.GetService<IRoleService>().LoadSystemRoleCache();

            //加载角色与菜单缓存
            await app.ApplicationServices.GetService<IRoleMenuService>().LoadSystemRoleMenuCache();

            //加载角色与机构缓存
            await app.ApplicationServices.GetService<IRoleOrgService>().LoadSystemRoleOrgCache();

            //加载角色与用户缓存
            await app.ApplicationServices.GetService<IRoleUserService>().LoadSystemRoleUserCache();

            //加载数据字典缓存
            await app.ApplicationServices.GetService<ICodeTableService>().LoadSystemCodeTableCache();

            //加载多语缓存
            await app.ApplicationServices.GetService<ILanguageService>().LoadSystemLanguageCache();
        }

        #endregion

        #region 清理系统缓存

        /// <summary>
        /// 清理系统缓存
        /// </summary>
        /// <returns></returns>
        public static async Task ClearSystemCache(IApplicationBuilder app)
        {
            //清理机构缓存
            await app.ApplicationServices.GetService<IOrganizationService>().ClearSystemOrgCache();

            //清理用户缓存
            await app.ApplicationServices.GetService<IUserService>().ClearSystemUserCache();

            //清理菜单缓存
            await app.ApplicationServices.GetService<IMenuService>().ClearSystemMenuCache();

            //清理菜单与多语缓存
            await app.ApplicationServices.GetService<IMenuLanguageService>().ClearSystemMenuLanguageCache();

            //清理角色缓存
            await app.ApplicationServices.GetService<IRoleService>().ClearSystemRoleCache();

            //清理角色与菜单缓存
            await app.ApplicationServices.GetService<IRoleMenuService>().ClearSystemRoleMenuCache();

            //清理角色与机构缓存
            await app.ApplicationServices.GetService<IRoleOrgService>().ClearSystemRoleOrgCache();

            //清理角色与用户缓存
            await app.ApplicationServices.GetService<IRoleUserService>().ClearSystemRoleUserCache();

            //清理数据字典缓存
            await app.ApplicationServices.GetService<ICodeTableService>().ClearSystemCodeTableCache();

            //清理多语缓存
            await app.ApplicationServices.GetService<ILanguageService>().ClearSystemLanguageCache();
        }

        #endregion

        #region 自动构建数据库

        /// <summary>
        /// 自动构建数据库
        /// </summary>
        /// <returns></returns>
        public static async Task AutomaticallyBuildDatabase()
        {
            await Task.Run(async () =>
            {
                if (GlobalContext.DBConfig.AutomaticallyBuildDatabase)
                {
                    try
                    {
                        RuYiAdminDbContext.RuYiDbContext.DbMaintenance.GetTableInfoList();
                        Console.WriteLine("Database is existed");
                    }
                    catch
                    {
                        Console.WriteLine("Database is not existed");

                        //Sql脚本路径
                        var sqlScriptPath = Path.Join(Directory.GetCurrentDirectory(), "/", GlobalContext.DBConfig.SqlScriptPath);

                        //读取脚本内容
                        var content = File.ReadAllText(sqlScriptPath);

                        //创建数据库
                        RuYiAdminDbContext.RuYiDbContext.DbMaintenance.CreateDatabase();
                        Console.WriteLine("Database created");

                        //构建表结构
                        await RuYiAdminDbContext.RuYiDbContext.Ado.ExecuteCommandAsync(content);
                        Console.WriteLine("Tables created");
                    }

                }
            });
        }

        #endregion
    }
}
