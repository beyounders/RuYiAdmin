﻿using Microsoft.Extensions.Logging;
using Quartz;
using RuYiAdmin.Net.Common.Global;
using RuYiAdmin.Net.Common.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.WebApi.AppCode.FrameworkQuartz
{
    [DisallowConcurrentExecution]
    public class RuYiAdminFrameworkJob : IJob
    {
        private readonly ILogger<RuYiAdminFrameworkJob> _logger;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="logger"></param>
        public RuYiAdminFrameworkJob(ILogger<RuYiAdminFrameworkJob> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// 执行框架作业
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public Task Execute(IJobExecutionContext context)
        {
            String time = DateTime.Now.ToString("HH:mm");
            if (time.Equals("04:00"))
            {
                //清理临时文件、释放服务器存储空间
                FileUtil.ClearDirectory(GlobalContext.DirectoryConfig.GetTempPath());
            }

            _logger.LogInformation("RuYiAdmin Framework Job Executed!");

            return Task.CompletedTask;
        }
    }
}